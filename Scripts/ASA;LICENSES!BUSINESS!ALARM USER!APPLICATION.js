//ASA: Licenses/Business/Alarm User/Application

//call script ** ALU01 ** script/functions based on requirements
AddAlarmUserRenFee();

//call script ** ID ** script/functions based on requirements
SetFileDate();

/*******************************************************
| Script/Function: SetFileDate() - (ID)
| Created by: Nicolaj Bunting
| Created on: 18May18
| Usage: On submit set the file date to ASI "Date Placed into Operation"
| Modified by: ()
*********************************************************/
function SetFileDate() {
    Avo_LogDebug("ID SetFileDate()", 1);

    var inOperationDate = getAppSpecific("Date Placed into Operation", capId);
    startDate = Avo_GetDateFromAccelaDateString(inOperationDate);
    Avo_LogDebug("Operation Date(" + aa.util.formatDate(startDate, "MM/dd/yyyy") + ")", 2);

    // Set the record opened date
    var capModel = aa.cap.getCap(capId).getOutput().capModel;
    capModel.setFileDate(startDate);
    var result = aa.cap.editCapByPK(capModel);
    if (result.getSuccess() == false) {
        Avo_LogDebug("Failed to set Date Business Setup in System", 1);
        return;
    }

    Avo_LogDebug('Set file date to "Date Placed into Operation" ' + aa.util.formatDate(startDate, "MM/dd/yyyy"), 1);
}

/*******************************************************
| Script/Function: AddAlarmUserRenFee() - (ALU01)
| Created by: Nicolaj Bunting
| Created on: 19Apr18
| Usage: On submit If ASI "Date Placed into Operation" < 365 days from today Then add and invoice fee "ALUREN" from schedule "LIC_ALU_APP" for each year in arrears
| Modified by: ()
*********************************************************/
function AddAlarmUserRenFee() {
    logDebug("ALU01 AddAlarmUserRenFee()");

    var yearFromNow = Avo_GetToday();
    yearFromNow.setDate(yearFromNow.getDate() + 365);
    Avo_LogDebug("Next Year(" + aa.util.formatDate(yearFromNow, "MM/dd/yyyy") + ")", 2);    //debug

    var operation = getAppSpecific("Date Placed into Operation", capId);
    var opDate = Avo_GetDateFromAccelaDateString(operation);
    Avo_LogDebug("Op Date(" + aa.util.formatDate(opDate, "MM/dd/yyyy") + ")", 2);    //debug

    if (opDate.getTime() >= yearFromNow.getTime()) {
        return;
    }

    var today = Avo_GetToday();
    var daysInArrears = (today.getTime() - opDate.getTime()) / (86400000);
    Avo_LogDebug("Days(" + daysInArrears + ")", 2);    //debug

    var quantity = Math.floor(daysInArrears / 365);
    if (quantity > 4) {
        quantity = 4;
    }

    Avo_LogDebug("Quantity(" + quantity + ")", 2);  //debug
    if (quantity <= 0) {
        return;
    }

    addFee("ALUREN", "LIC_ALU_APP", "FINAL", quantity, "Y", capId);
    Avo_LogDebug("Added fee ALUAPP from schedule LIC_ALU_APP with quantity of " + quantity.toString(), 1);
}