//WTUA: Licenses/Business/*/Amendment

//call script ** ID71 ** script/functions based on requirements
if (wfTask == "Issue License") {
    if (wfStatus == "Closed: Issued") {
        Avo_IssueAmendment();
    }
}

/*******************************************************
| Script/Function: Avo_IssueAmendment() - (ID71)
| Created by: Nicolaj Bunting
| Created on: 8Mar18
| Usage: If task "Issue License" has status "Closed: Issued" then if primary or only address is different from parent license 
| primary address then remove primary status from parent license primary address, copy primary or only address from amendment 
| to parent license and set as primary, copy parcel, contacts, activities, comments, inspections, ASI, ASIT, 
| documents to license and update license app name
| Modified by: Nic Bunting (18Apr18)
| Modified by: Nic Bunting (2Oct18)
| Modified by: Nic Bunting (14Jun19)
*********************************************************/
function Avo_IssueAmendment() {
    Avo_LogDebug("ID71 Avo_IssueAmendment()", 1);

    // Check valid record type
    var isAuctionHouse = appMatch("*/*/Auction House/*", capId);
    var isBusRegService = appMatch("*/*/Bus Reg Service/*", capId);
    var isBusRegMerchant = appMatch("*/*/Bus Reg Merchant/*", capId);
    var isEscortBureau = appMatch("*/*/Escort Bureau/*", capId);
    var isLiqPermit = appMatch("*/*/Liquor/*", capId);
    var isSecHandPawn = appMatch("*/*/Secondhand/*", capId);
    var isSolidWaste = appMatch("*/*/Solid Waste/*", capId);
    var isValetParking = appMatch("*/*/Valet Parking/*", capId);
    var isMassageFacility = appMatch("*/*/Massage Facility/*", capId);
    var isStreetVendor = appMatch("*/*/Neighborhood Street Vendor/*", capId);
    if (!isAuctionHouse && !isBusRegService && !isBusRegMerchant && !isEscortBureau && !isLiqPermit
        && !isSecHandPawn && !isSolidWaste && !isValetParking && !isMassageFacility && !isStreetVendor) {
        Avo_LogDebug("Invalid record type", 1);
        return;
    }

    // Get parent license
    var parents = aa.cap.getProjectByChildCapID(capId, "", "").getOutput();
    if (!parents) {
        Avo_LogDebug("No parent licenses", 1);
        return;
    }

    var licCapId = null;
    for (var a in parents) {
        var parentCapId = parents[a].projectID;

        if (appMatch("Licenses/Business/*/License", parentCapId) == false) {
            continue;
        }

        licCapId = parentCapId;
        break;
    }

    if (!licCapId) {
        Avo_LogDebug("No parent licenses", 1);
        return;
    }

    var amendAltId = aa.cap.getCap(capId).getOutput().getCapModel().altID;
    var licAltId = aa.cap.getCap(licCapId).getOutput().getCapModel().altID;
    Avo_LogDebug("Parent License(" + licAltId + ")", 2);    //debug

    // Check if addresses match
    var result = aa.address.getAddressByCapId(capId);
    if (result.getSuccess() == true) {
        var copyAddress = true;

        // Get primary or only address
        var allAddresses = result.getOutput();
        var amendAddress = allAddresses[0];
        for (i in allAddresses) {
            var address = allAddresses[i];
            if (address.primaryFlag != "Y") {
                continue;
            }

            amendAddress = address;
            break;
        }

        var licAddress = null;

        result = aa.address.getAddressByCapId(licCapId);
        if (result.getSuccess() != true) {
            Avo_LogDebug("No address on parent license " + licAltId, 1);
        } else {
            var allAddresses = result.getOutput();
            for (i in allAddresses) {
                var address = allAddresses[i];
                if (address.primaryFlag != "Y") {
                    continue;
                }

                licAddress = address;
                copyAddress = false;
                break;
            }

            if (licAddress) {
                if (amendAddress.houseNumberStart != licAddress.houseNumberStart) {
                    Avo_LogDebug("Address numbers Amend(" + amendAddress.houseNumberStart + "), Lic(" + licAddress.houseNumberStart + ")", 2);
                    copyAddress = true;
                }
                if (amendAddress.streetDirection != licAddress.streetDirection) {
                    Avo_LogDebug("Street direction Amend(" + amendAddress.streetDirection + "), Lic(" + licAddress.streetDirection + ")", 2);
                    copyAddress = true;
                }
                if (amendAddress.streetName != licAddress.streetName) {
                    Avo_LogDebug("Street name Amend(" + amendAddress.streetName + "), Lic(" + licAddress.streetName + ")", 2);
                    copyAddress = true;
                }
                if (amendAddress.streetSuffix != licAddress.streetSuffix) {
                    Avo_LogDebug("Street type Amend(" + amendAddress.streetSuffix + "), Lic(" + licAddress.streetSuffix + ")", 2);
                    copyAddress = true;
                }
                if (amendAddress.unitStart != licAddress.unitStart) {
                    Avo_LogDebug("Unit # Amend(" + amendAddress.unitStart + "), Lic(" + licAddress.unitStart + ")", 2);
                    copyAddress = true;
                }

                if (copyAddress == true) {
                    // Set current address on license to secondary
                    licAddress.primaryFlag = "N";
                    result = aa.address.editAddress(licAddress);
                    if (result.getSuccess() != true) {
                        Avo_LogDebug("Failed to set license address as secondary. " + result.errorMessage, 1);
                        copyAddress = false;
                    }
                }
            }
        }

        if (copyAddress == true) {
            // Copy primary address to license
            amendAddress.setCapID(licCapId);
            amendAddress.setPrimaryFlag("Y");
            result = aa.address.createAddressWithAPOAttribute(licCapId, amendAddress);
            if (result.getSuccess() == true) {
                Avo_LogDebug("Copied address " + amendAddress.displayAddress + " from amendment " + amendAltId + " to parent license " + licAltId, 1);
            } else {
                Avo_LogDebug("Failed to copy address " + amendAddress.displayAddress + " from amendment " + amendAltId + " to parent license " + licAltId + ". " + result.errorMessage, 1);
            }
        }
    }

    // Copy parcel
    copyParcels(capId, licCapId);

    // Remove all contacts from license
    var contacts = getContactArray(licCapId);
    for (var a in contacts) {
        // Remove contact
        var contactId = contacts[a].contactSeqNumber;
        var result = aa.people.removeCapContact(licCapId, contactId);
        if (result.getSuccess()) {
            Avo_LogDebug("Contact " + contactId + " removed from license " + licAltId, 1);
        } else {
            Avo_LogDebug("Failed to remove contact " + contactId + " from license " + licAltId + ". " + result.errorMessage, 1);
        }
    }

    // Copy contacts
    copyContacts(capId, licCapId);

    // Copy comments
    var fromCapScriptModel = aa.cap.getCapScriptModel(aa.cap.getCap(capId).getOutput().getCapModel()).getOutput();
    var toCapScriptModel = aa.cap.getCapScriptModel(aa.cap.getCap(licCapId).getOutput().getCapModel()).getOutput();

    result = aa.cap.copyComments(fromCapScriptModel, toCapScriptModel);
    if (result.getSuccess() == false) {
        Avo_LogDebug("Failed to copy comments from amendment " + amendAltId + " to parent license " + licAltId, 1);
    } else {
        Avo_LogDebug("Copied comments from amendment " + amendAltId + " to parent license " + licAltId, 1);
    }

    // Copy inspections
    result = aa.inspection.getInspections(capId);
    if (result.getSuccess() == true) {
        var allInspections = result.getOutput();
        for (var i in allInspections) {
            var inspection = allInspections[i];
            var inspModel = inspection.inspection;

            result = aa.inspection.copyInspectionWithGuideSheet(capId, licCapId, inspModel);
            if (result.getSuccess() == false) {
                Avo_LogDebug("Failed to copy " + inspModel.inspectionType + " inspection from amendment " + amendAltId + " to parent license " + licAltId, 1);
            }
        }

        Avo_LogDebug("Copied all inspections from amendment " + amendAltId + " to parent license " + licAltId, 1);
    } else {
        Avo_LogDebug("No inspections found. " + result.getErrorMessage(), 1);
    }

    // Copy ASI
    copyAppSpecific(licCapId);

    // Drop all tables
    var allTables = aa.appSpecificTableScript.getAppSpecificGroupTableNames(licCapId).getOutput();
    for (var i in allTables) {
        var tableName = allTables[i];
        removeASITable(tableName, licCapId);
        Avo_LogDebug('Cleared table "' + tableName + '" on license ' + licAltId, 1);
    }

    // Copy ASIT
    copyASITables(capId, licCapId);

    // Copy Docs
    Avo_CopyDocuments(capId, licCapId);
}