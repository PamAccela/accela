//WTUB: Licenses/*/*/*

if (wfTask == "License Issuance" && wfStatus == "Issued" && balanceDue > 0) {
//if (((wfTask == "License Issuance" && wfStatus == "Issued") || (wfTask == "Permit Application" && wfStatus == "Closed: Permit Issued")) && balanceDue > 0) {
    showMessage = true;
    cancel = true;
    comment("Cannot issue this license with a balance greater than zero");
}

var marijuanaRecords = appMatch("Licenses/Business/Marijuana Medical Center/Application") ||
appMatch("Licenses/Business/Marijuana Med Infu Prod Mfg/Application") ||
appMatch("Licenses/Business/Marijuana Med Opt Prem Culti/Application"); var marijuanaRetailRecords =
appMatch("Licenses/Business/Marijuana Ret Infu Prod Mfg/Application") ||
appMatch("Licenses/Business/Marijuana Ret Opt Prem Culti/Application") ||
appMatch("Licenses/Business/Marijuana Retail Store/Application") ||
appMatch("Licenses/Business/Marijuana Ret Testing Facility/Application");

if (marijuanaRecords && wfTask == "Application Intake" && wfStatus == "Complete" && balanceDue > 0) {
    showMessage = true;
    cancel = true;
    comment("The balance must be paid in full at the Cashier to move forward");
}

if (marijuanaRetailRecords) {
    include("WTUB Marijuana Retail App");
}

//call script ** ID4 ** script/functions based on requirements
var app = appMatch("Licenses/*/*/Application", capId);
var amend = appMatch("Licenses/*/*/Amendment", capId);
var renewal = appMatch("Licenses/*/*/Renewal", capId);

if (app == true || amend == true) {
    if (wfTask == "Issue License") {
        if (wfStatus == "Closed: Issued") {
            Avo_CheckAllFeesPaid();
        }
    }

    if (wfTask == "Permit Application") {
        if (wfStatus == "Closed: Permit Issued") {
            Avo_CheckAllFeesPaid();
        }
    }
}

if (renewal == true) {
    if (wfTask == "Renewal") {
        if (wfStatus == "Closed: Paid") {
            Avo_CheckAllFeesPaid();
        }
    }
    if (wfTask == "Issue License") {
        if (wfStatus == "Closed: Issued") {
            Avo_CheckAllFeesPaid();
        }
    }
}

/*******************************************************
| Script/Function: Avo_CheckAllFeesPaid() - (ID4)
| Created by: Nicolaj Bunting
| Created on: 12Dec17
| Usage: For application and amendment records if task "Issue License" has status "Closed: Issued", or renewal if task "Renewal" has status "Closed: Paid" or 
| task "Issue License" has status "Closed: Issued" then if any fees are unpaid and invoiced or any related records have unpaid, invoiced fees then cancel 
| and display message "Unpaid fees on the following records have been found:<Record Ids>Please ensure all fees are paid prior to proceeding."
| Modified by: ()
*********************************************************/
function Avo_CheckAllFeesPaid() {
    logDebug("ID4 Avo_CheckAllFeesPaid()");

    var message = "Unpaid fees on the following records have been found:";
    var messageSuffix = "Please ensure all fees are paid prior to proceeding.";
    var cancelUpdate = false;

    // Check record for unpaid, invoiced fees
    var unpaid = Avo_HasUnpaidFeesWithStatus(capId, "INVOICED");
    if (unpaid == true) {
        cancelUpdate = true;
        var altId = aa.cap.getCap(capId).getOutput().getCapModel().altID;
        message += "<br/>" + altId;
    }

    // Get parent license
    var licCapId = null;
    var allParentRecords = aa.cap.getProjectByChildCapID(capId, "", "").getOutput();
    for (i in allParentRecords) {
        var parentCapId = allParentRecords[i].projectID;

        var result = aa.cap.getCap(parentCapId);
        if (result.getSuccess() != true) {
            continue;
        }

        licCapId = parentCapId;
        break;
    }

    if (licCapId) {
        var licAltId = aa.cap.getCap(licCapId).getOutput().getCapModel().altID;
        logDebug("Parent License CapId(" + licAltId + ")"); //debug

        // Check related records for unpaid, invoiced fees
        unpaid = Avo_HasUnpaidFeesWithStatus(licCapId, "INVOICED");
        if (unpaid == true) {
            cancelUpdate = true;
            
            message += "<br/>" + licAltId;
        }

        var children = getChildren("Licenses/*/*/*", licCapId, capId);
        for (i in children) {
            var childCapId = children[i];

            unpaid = Avo_HasUnpaidFeesWithStatus(childCapId, "INVOICED");
            if (unpaid == false) {
                continue;
            }

            cancelUpdate = true;
            var childAltId = aa.cap.getCap(childCapId).getOutput().getCapModel().altID;
            message += "<br/>" + childAltId;
        }
    } else {
        logDebug("No parent license");
    }

    if(cancelUpdate == false) {
        return;
    }

    cancel = true;
    showMessage = true;
    comment(message + "<br/>" + messageSuffix);
}