//ASA: Licenses/Business/Solicitor For Profit/Application

//call script ** SFP01 ** script/functions based on requirements
Avo_AddSFPLicFee();

/*******************************************************
| Script/Function: Avo_AddSFPLicFee() - (SFP01)
| Created by: Nicolaj Bunting
| Created on: 11Jan18
| Usage: For each row in ASIT "DATES OF SOLICITATION" set field "Number of Days" to difference between field "Solicitation Start Date"
| and "Solicitation End Date" and set to read only, add and invoice fee "SFPLIC" from schedule "LIC_SFP_APP" with quantity of total number 
| of days of all rows
| Modified by: Nic Bunting (28May19)
*********************************************************/
function Avo_AddSFPLicFee() {
    logDebug("SFP01 Avo_AddSFPLicFee()");

    var tableName = "DATES OF SOLICITATION";
    var table = loadASITable(tableName);
    if (!table) {
        logDebug(tableName + " does not exist on this record type");
        return;
    }
    if (table.length == 0) {
        Avo_LogDebug('"' + tableName + '" table has no rows', 1);
        return;
    }

    var startDateField = "Solicitation Start Date";
    var endDateField = "Solicitation End Date";
    var daysField = "Number of Days";
    var allFields = [startDateField, endDateField, daysField];

    var totalDays = 0;

    var tableArray = new Array();
    for (var i in table) {
        var rowToAdd = new Object();

        // Copy row to array
        for (var j = 0; j < allFields.length; j++) {
            var columnName = allFields[j];
            var field = table[i][columnName];
            var entry = field.fieldValue;
            var readOnly = field.readOnly;

            rowToAdd[columnName] = new asiTableValObj(columnName, entry, readOnly);
        }

        Avo_LogDebug("End Date(" + rowToAdd[endDateField].fieldValue + ")", 2);  //debug
        Avo_LogDebug("Start Date(" + rowToAdd[startDateField].fieldValue + ")", 2);  //debug

        var startDate = Avo_GetDateFromAccelaDateString(rowToAdd[startDateField].fieldValue);
        var endDate = Avo_GetDateFromAccelaDateString(rowToAdd[endDateField].fieldValue);

        var days = dateDiff(startDate, endDate) + 1;
        Avo_LogDebug("Days(" + days + ")", 2);  //debug

        totalDays += days;

        rowToAdd[daysField] = new asiTableValObj(daysField, days.toString(), "Y");	// "Y" - field is read only
        Avo_LogDebug('Set "' + daysField + '" field to ' + days, 1);

        tableArray.push(rowToAdd);
    }

    addFee("SFPLIC", "LIC_SFP_APP", "FINAL", totalDays, "Y", capId);
    Avo_LogDebug("Added fee SFPLIC with quantity of " + totalDays, 1);

    // Clear current table and replace with newly updated table
    try {
        removeASITable(tableName.toUpperCase(), capId);
        addASITable(tableName.toUpperCase(), tableArray, capId);
        Avo_LogDebug('Updated "' + tableName + '" table', 1);
    }
    catch (err) {
        Avo_LogDebug('Failed to update "' + tableName + '" table. ' + err.message, 1);
    }
}