//ASA: Licenses/Business/After Hours Establishment/Application

//call script ** AHE01 ** script/functions based on requirements
Avo_AfterHoursIssFee();

/*******************************************************
| Script/Function: Avo_AfterHoursIssFee() - (AHE01)
| Created by: Nicolaj Bunting
| Created on: 9Jan18
| Usage: Get month of ASI "Business Start Date" then add and invoice fee "AHELIC" from schedule "LIC_AHE_APP" with quantity depending on the month
| Modified by: ()
*********************************************************/
function Avo_AfterHoursIssFee() {
    logDebug("AHE01 Avo_AfterHoursIssFee()");

    var busStart = getAppSpecific("Business Start Date", capId);
    if (!busStart) {
        logDebug("Business start date not set");
        return;
    }

    var quantity = Avo_GetQuarterPercentage(busStart);
    logDebug("Quantity(" + quantity + ")"); //debug

    addFee("AHELIC", "LIC_AHE_APP", "FINAL", quantity, "Y", capId);
    logDebug("Added fee AHELIC from schedule LIC_AHE_APP with quantity of " + quantity.toString());
}