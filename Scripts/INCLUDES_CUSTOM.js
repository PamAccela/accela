/*------------------------------------------------------------------------------------------------------/
| Program : INCLUDES_CUSTOM.js
| Event   : N/A
|
| Usage   : Custom Script Include.  Insert custom EMSE Function below and they will be 
	    available to all master scripts
|
| Notes   : createRefLicProf - override to default the state if one is not provided
|
|         : createRefContactsFromCapContactsAndLink - testing new ability to link public users to new ref contacts
/------------------------------------------------------------------------------------------------------*/
function runReportTest(aaReportName) {
    x = "test param"
    currentUserID = "ADMIN";
    setCode = "X";
    var bReport = false;
    var reportName = aaReportName;
    report = aa.reportManager.getReportModelByName(reportName);
    report = report.getOutput();
    var permit = aa.reportManager.hasPermission(reportName, currentUserID);
    if (permit.getOutput().booleanValue()) {
        var parameters = aa.util.newHashMap();
        parameters.put("BatchNumber", setCode);
        //report.setReportParameters(parameters);
        var msg = aa.reportManager.runReport(parameters, report);
        aa.env.setValue("ScriptReturnCode", "0");
        aa.env.setValue("ScriptReturnMessage", msg.getOutput());
    }
}

function createRefLicProf(rlpId, rlpType, pContactType) {
    //Creates/updates a reference licensed prof from a Contact
    //06SSP-00074, modified for 06SSP-00238
    var updating = false;
    var capContResult = aa.people.getCapContactByCapID(capId);
    if (capContResult.getSuccess())
    { conArr = capContResult.getOutput(); }
    else
    {
        logDebug("**ERROR: getting cap contact: " + capAddResult.getErrorMessage());
        return false;
    }

    if (!conArr.length) {
        logDebug("**WARNING: No contact available");
        return false;
    }


    var newLic = getRefLicenseProf(rlpId)

    if (newLic) {
        updating = true;
        logDebug("Updating existing Ref Lic Prof : " + rlpId);
    }
    else
        var newLic = aa.licenseScript.createLicenseScriptModel();

    //get contact record
    if (pContactType == null)
        var cont = conArr[0]; //if no contact type specified, use first contact
    else {
        var contFound = false;
        for (yy in conArr) {
            if (pContactType.equals(conArr[yy].getCapContactModel().getPeople().getContactType())) {
                cont = conArr[yy];
                contFound = true;
                break;
            }
        }
        if (!contFound) {
            logDebug("**WARNING: No Contact found of type: " + pContactType);
            return false;
        }
    }

    peop = cont.getPeople();
    addr = peop.getCompactAddress();

    newLic.setContactFirstName(cont.getFirstName());
    //newLic.setContactMiddleName(cont.getMiddleName());  //method not available
    newLic.setContactLastName(cont.getLastName());
    newLic.setBusinessName(peop.getBusinessName());
    newLic.setAddress1(addr.getAddressLine1());
    newLic.setAddress2(addr.getAddressLine2());
    newLic.setAddress3(addr.getAddressLine3());
    newLic.setCity(addr.getCity());
    newLic.setState(addr.getState());
    newLic.setZip(addr.getZip());
    newLic.setPhone1(peop.getPhone1());
    newLic.setPhone2(peop.getPhone2());
    newLic.setEMailAddress(peop.getEmail());
    newLic.setFax(peop.getFax());

    newLic.setAgencyCode(aa.getServiceProviderCode());
    newLic.setAuditDate(sysDate);
    newLic.setAuditID(currentUserID);
    newLic.setAuditStatus("A");

    if (AInfo["Insurance Co"]) newLic.setInsuranceCo(AInfo["Insurance Co"]);
    if (AInfo["Insurance Amount"]) newLic.setInsuranceAmount(parseFloat(AInfo["Insurance Amount"]));
    if (AInfo["Insurance Exp Date"]) newLic.setInsuranceExpDate(aa.date.parseDate(AInfo["Insurance Exp Date"]));
    if (AInfo["Policy #"]) newLic.setPolicy(AInfo["Policy #"]);

    if (AInfo["Business License #"]) newLic.setBusinessLicense(AInfo["Business License #"]);
    if (AInfo["Business License Exp Date"]) newLic.setBusinessLicExpDate(aa.date.parseDate(AInfo["Business License Exp Date"]));

    newLic.setLicenseType(rlpType);

    if (addr.getState() != null)
        newLic.setLicState(addr.getState());
    else
        newLic.setLicState("AK"); //default the state if none was provided

    newLic.setStateLicense(rlpId);

    if (updating)
        myResult = aa.licenseScript.editRefLicenseProf(newLic);
    else
        myResult = aa.licenseScript.createRefLicenseProf(newLic);

    if (myResult.getSuccess()) {
        logDebug("Successfully added/updated License No. " + rlpId + ", Type: " + rlpType);
        logMessage("Successfully added/updated License No. " + rlpId + ", Type: " + rlpType);
        return true;
    }
    else {
        logDebug("**ERROR: can't create ref lic prof: " + myResult.getErrorMessage());
        logMessage("**ERROR: can't create ref lic prof: " + myResult.getErrorMessage());
        return false;
    }
}

function createRefContactsFromCapContactsAndLink(pCapId, contactTypeArray, ignoreAttributeArray, replaceCapContact, overwriteRefContact, refContactExists) {

    // contactTypeArray is either null (all), or an array or contact types to process
    //
    // ignoreAttributeArray is either null (none), or an array of attributes to ignore when creating a REF contact
    //
    // replaceCapContact not implemented yet
    //
    // overwriteRefContact -- if true, will refresh linked ref contact with CAP contact data
    //
    // refContactExists is a function for REF contact comparisons.
    //
    // Version 2.0 Update:   This function will now check for the presence of a standard choice "REF_CONTACT_CREATION_RULES". 
    // This setting will determine if the reference contact will be created, as well as the contact type that the reference contact will 
    // be created with.  If this setting is configured, the contactTypeArray parameter will be ignored.   The "Default" in this standard
    // choice determines the default action of all contact types.   Other types can be configured separately.   
    // Each contact type can be set to "I" (create ref as individual), "O" (create ref as organization), 
    // "F" (follow the indiv/org flag on the cap contact), "D" (Do not create a ref contact), and "U" (create ref using transaction contact type).

    var standardChoiceForBusinessRules = "REF_CONTACT_CREATION_RULES";


    var ingoreArray = new Array();
    if (arguments.length > 1) ignoreArray = arguments[1];

    var defaultContactFlag = lookup(standardChoiceForBusinessRules, "Default");

    var c = aa.people.getCapContactByCapID(pCapId).getOutput()
    var cCopy = aa.people.getCapContactByCapID(pCapId).getOutput()  // must have two working datasets

    for (var i in c) {
        var ruleForRefContactType = "U"; // default behavior is create the ref contact using transaction contact type
        var con = c[i];

        var p = con.getPeople();

        var contactFlagForType = lookup(standardChoiceForBusinessRules, p.getContactType());

        if (!defaultContactFlag && !contactFlagForType) // standard choice not used for rules, check the array passed
        {
            if (contactTypeArray && !exists(p.getContactType(), contactTypeArray))
                continue;  // not in the contact type list.  Move along.
        }

        if (!contactFlagForType && defaultContactFlag) // explicit contact type not used, use the default
        {
            ruleForRefContactType = defaultContactFlag;
        }

        if (contactFlagForType) // explicit contact type is indicated
        {
            ruleForRefContactType = contactFlagForType;
        }

        if (ruleForRefContactType.equals("D"))
            continue;

        var refContactType = "";

        switch (ruleForRefContactType) {
            case "U":
                refContactType = p.getContactType();
                break;
            case "I":
                refContactType = "Individual";
                break;
            case "O":
                refContactType = "Organization";
                break;
            case "F":
                if (p.getContactTypeFlag() && p.getContactTypeFlag().equals("organization"))
                    refContactType = "Organization";
                else
                    refContactType = "Individual";
                break;
        }

        var refContactNum = con.getCapContactModel().getRefContactNumber();

        if (refContactNum)  // This is a reference contact.   Let's refresh or overwrite as requested in parms.
        {
            if (overwriteRefContact) {
                p.setContactSeqNumber(refContactNum);  // set the ref seq# to refresh
                p.setContactType(refContactType);

                var a = p.getAttributes();

                if (a) {
                    var ai = a.iterator();
                    while (ai.hasNext()) {
                        var xx = ai.next();
                        xx.setContactNo(refContactNum);
                    }
                }

                var r = aa.people.editPeopleWithAttribute(p, p.getAttributes());

                if (!r.getSuccess())
                    logDebug("WARNING: couldn't refresh reference people : " + r.getErrorMessage());
                else
                    logDebug("Successfully refreshed ref contact #" + refContactNum + " with CAP contact data");
            }

            if (replaceCapContact) {
                // To Be Implemented later.   Is there a use case?
            }

        }
        else  // user entered the contact freehand.   Let's create or link to ref contact.
        {
            var ccmSeq = p.getContactSeqNumber();

            var existingContact = refContactExists(p);  // Call the custom function to see if the REF contact exists

            var p = cCopy[i].getPeople();  // get a fresh version, had to mangle the first for the search

            if (existingContact)  // we found a match with our custom function.  Use this one.
            {
                refPeopleId = existingContact;
            }
            else  // did not find a match, let's create one
            {

                var a = p.getAttributes();

                if (a) {
                    //
                    // Clear unwanted attributes
                    var ai = a.iterator();
                    while (ai.hasNext()) {
                        var xx = ai.next();
                        if (ignoreAttributeArray && exists(xx.getAttributeName().toUpperCase(), ignoreAttributeArray))
                            ai.remove();
                    }
                }

                p.setContactType(refContactType);
                var r = aa.people.createPeopleWithAttribute(p, a);

                if (!r.getSuccess())
                { logDebug("WARNING: couldn't create reference people : " + r.getErrorMessage()); continue; }

                //
                // createPeople is nice and updates the sequence number to the ref seq
                //

                var p = cCopy[i].getPeople();
                var refPeopleId = p.getContactSeqNumber();

                logDebug("Successfully created reference contact #" + refPeopleId);

                // Need to link to an existing public user.

                var getUserResult = aa.publicUser.getPublicUserByEmail(con.getEmail())
                if (getUserResult.getSuccess() && getUserResult.getOutput()) {
                    var userModel = getUserResult.getOutput();
                    logDebug("createRefContactsFromCapContactsAndLink: Found an existing public user: " + userModel.getUserID());

                    if (refPeopleId) {
                        logDebug("createRefContactsFromCapContactsAndLink: Linking this public user with new reference contact : " + refPeopleId);
                        aa.licenseScript.associateContactWithPublicUser(userModel.getUserSeqNum(), refPeopleId);
                    }
                }
            }

            //
            // now that we have the reference Id, we can link back to reference
            //

            var ccm = aa.people.getCapContactByPK(pCapId, ccmSeq).getOutput().getCapContactModel();

            ccm.setRefContactNumber(refPeopleId);
            r = aa.people.editCapContact(ccm);

            if (!r.getSuccess())
            { logDebug("WARNING: error updating cap contact model : " + r.getErrorMessage()); }
            else
            { logDebug("Successfully linked ref contact " + refPeopleId + " to cap contact " + ccmSeq); }


        }  // end if user hand entered contact 
    }  // end for each CAP contact
} // end function

function reversePayment() { logDebug("hello") }

function addToASITable(tableName, tableValues) // optional capId
{
    //  tableName is the name of the ASI table
    //  tableValues is an associative array of values.  All elements must be either a string or asiTableVal object
    itemCap = capId
    if (arguments.length > 2)
        itemCap = arguments[2]; // use cap ID specified in args

    var tssmResult = aa.appSpecificTableScript.getAppSpecificTableModel(itemCap, tableName)

    if (!tssmResult.getSuccess())
    { logDebug("**WARNING: error retrieving app specific table " + tableName + " " + tssmResult.getErrorMessage()); return false }

    var tssm = tssmResult.getOutput();
    var tsm = tssm.getAppSpecificTableModel();
    var fld = tsm.getTableField();
    var col = tsm.getColumns();
    var fld_readonly = tsm.getReadonlyField(); //get ReadOnly property
    var coli = col.iterator();

    while (coli.hasNext()) {
        colname = coli.next();

        if (!tableValues[colname.getColumnName()]) {
            logDebug("addToASITable: null or undefined value supplied for column " + colname.getColumnName() + ", setting to empty string");
            tableValues[colname.getColumnName()] = "";
        }

        if (typeof (tableValues[colname.getColumnName()].fieldValue) != "undefined") {
            fld.add(tableValues[colname.getColumnName()].fieldValue);
            fld_readonly.add(tableValues[colname.getColumnName()].readOnly);
        }
        else // we are passed a string
        {
            fld.add(tableValues[colname.getColumnName()]);
            fld_readonly.add(null);
        }
    }

    tsm.setTableField(fld);
    tsm.setReadonlyField(fld_readonly); // set readonly field

    addResult = aa.appSpecificTableScript.editAppSpecificTableInfos(tsm, itemCap, currentUserID);
    if (!addResult.getSuccess())
    { logDebug("**WARNING: error adding record to ASI Table:  " + tableName + " " + addResult.getErrorMessage()); return false }
    else
        logDebug("Successfully added record to ASI Table: " + tableName);
}

function addASITable(tableName, tableValueArray) // optional capId
{
    //  tableName is the name of the ASI table
    //  tableValueArray is an array of associative array values.  All elements MUST be either a string or asiTableVal object
    var itemCap = capId
    if (arguments.length > 2)
        itemCap = arguments[2]; // use cap ID specified in args

    var tssmResult = aa.appSpecificTableScript.getAppSpecificTableModel(itemCap, tableName)

    if (!tssmResult.getSuccess()) {
        logDebug("**WARNING: error retrieving app specific table " + tableName + " " + tssmResult.getErrorMessage());
        return false
    }

    var tssm = tssmResult.getOutput();
    var tsm = tssm.getAppSpecificTableModel();
    var fld = tsm.getTableField();
    var fld_readonly = tsm.getReadonlyField(); // get Readonly field

    for (thisrow in tableValueArray) {

        var col = tsm.getColumns()
        var coli = col.iterator();
        while (coli.hasNext()) {
            var colname = coli.next();

            if (!tableValueArray[thisrow][colname.getColumnName()]) {
                logDebug("addToASITable: null or undefined value supplied for column " + colname.getColumnName() + ", setting to empty string");
                tableValueArray[thisrow][colname.getColumnName()] = "";
            }

            if (typeof (tableValueArray[thisrow][colname.getColumnName()].fieldValue) != "undefined") // we are passed an asiTablVal Obj
            {
                fld.add(tableValueArray[thisrow][colname.getColumnName()].fieldValue);
                fld_readonly.add(tableValueArray[thisrow][colname.getColumnName()].readOnly);
                //fld_readonly.add(null);
            } else // we are passed a string
            {
                fld.add(tableValueArray[thisrow][colname.getColumnName()]);
                fld_readonly.add(null);
            }
        }

        tsm.setTableField(fld);

        tsm.setReadonlyField(fld_readonly);

    }

    var addResult = aa.appSpecificTableScript.editAppSpecificTableInfos(tsm, itemCap, currentUserID);

    if (!addResult.getSuccess()) {
        logDebug("**WARNING: error adding record to ASI Table:  " + tableName + " " + addResult.getErrorMessage());
        return false
    } else
        logDebug("Successfully added record to ASI Table: " + tableName);

}

function getLatestScheduledDate() {
    var inspResultObj = aa.inspection.getInspections(capId);
    if (inspResultObj.getSuccess()) {
        inspList = inspResultObj.getOutput();
        var array = new Array();
        var j = 0;
        for (i in inspList) {
            if (inspList[i].getInspectionStatus().equals("Scheduled")) {
                array[j++] = aa.util.parseDate(inspList[i].getInspection().getScheduledDate());
            }
        }

        var latestScheduledDate = array[0];
        for (k = 0; k < array.length; k++) {
            temp = array[k];
            logDebug("----------array.k---------->" + array[k]);
            if (temp.after(latestScheduledDate)) {
                latestScheduledDate = temp;
            }
        }
        return latestScheduledDate;
    }
    return false;
}

function cntAssocGarageSales(strnum, strname, city, state, zip, cfname, clname) {

    /***

	Searches for Garage-Yard Sale License records 
	- Created in the current year 
	- Matches address parameters provided
	- Matches the contact first and last name provided
	- Returns the count of records

	***/

    // Create a cap model for search
    var searchCapModel = aa.cap.getCapModel().getOutput();

    // Set cap model for search. Set search criteria for record type DCA/*/*/*
    var searchCapModelType = searchCapModel.getCapType();
    searchCapModelType.setGroup("Licenses");
    searchCapModelType.setType("Garage-Yard Sale");
    searchCapModelType.setSubType("License");
    searchCapModelType.setCategory("NA");
    searchCapModel.setCapType(searchCapModelType);

    searchAddressModel = searchCapModel.getAddressModel();
    searchAddressModel.setStreetName(strname);

    gisObject = new com.accela.aa.xml.model.gis.GISObjects;
    qf = new com.accela.aa.util.QueryFormat;

    var toDate = aa.date.getCurrentDate();
    var fromDate = aa.date.parseDate("01/01/" + toDate.getYear());

    var recordCnt = 0;
    message = "The applicant has reached the Garage-Sale License limit of 3 per calendar year.<br>"

    capList = aa.cap.getCapListByCollection(searchCapModel, searchAddressModel, "", fromDate, toDate, qf, gisObject).getOutput();
    for (x in capList) {
        resultCap = capList[x];
        resultCapId = resultCap.getCapID();
        altId = resultCapId.getCustomID();
        //aa.print("Record ID: " + altId);
        resultCapIdScript = aa.cap.createCapIDScriptModel(resultCapId.getID1(), resultCapId.getID2(), resultCapId.getID3());
        contact = aa.cap.getCapPrimaryContact(resultCapIdScript).getOutput();

        contactFname = contact.getFirstName();
        contactLname = contact.getLastName();

        if (contactFname == cfname && contactLname == clname) {
            recordCnt++;
            message = message + recordCnt + ": " + altId + " - " + contactFname + " " + contactLname + " @ " + strnum + " " + strname + "<br>";
        }
    }

    return recordCnt;

}

function copyContactsWithAddress(pFromCapId, pToCapId) {
    // Copies all contacts from pFromCapId to pToCapId and includes Contact Address objects
    //
    if (pToCapId == null)
        var vToCapId = capId;
    else
        var vToCapId = pToCapId;

    var capContactResult = aa.people.getCapContactByCapID(pFromCapId);
    var copied = 0;
    if (capContactResult.getSuccess()) {
        var Contacts = capContactResult.getOutput();
        for (yy in Contacts) {
            var newContact = Contacts[yy].getCapContactModel();

            var newPeople = newContact.getPeople();
            // aa.print("Seq " + newPeople.getContactSeqNumber());

            var addressList = aa.address.getContactAddressListByCapContact(newContact).getOutput();
            newContact.setCapID(vToCapId);
            aa.people.createCapContact(newContact);
            newerPeople = newContact.getPeople();
            // contact address copying
            if (addressList) {
                for (add in addressList) {
                    var transactionAddress = false;
                    contactAddressModel = addressList[add].getContactAddressModel();

                    logDebug("contactAddressModel.getEntityType():" + contactAddressModel.getEntityType());

                    if (contactAddressModel.getEntityType() == "CAP_CONTACT") {
                        transactionAddress = true;
                        contactAddressModel.setEntityID(parseInt(newerPeople.getContactSeqNumber()));
                    }
                    // Commit if transaction contact address
                    if (transactionAddress) {
                        var newPK = new com.accela.orm.model.address.ContactAddressPKModel();
                        contactAddressModel.setContactAddressPK(newPK);
                        aa.address.createCapContactAddress(vToCapId, contactAddressModel);
                    }
                        // Commit if reference contact address
                    else {
                        // build model
                        var Xref = aa.address.createXRefContactAddressModel().getOutput();
                        Xref.setContactAddressModel(contactAddressModel);
                        Xref.setAddressID(addressList[add].getAddressID());
                        Xref.setEntityID(parseInt(newerPeople.getContactSeqNumber()));
                        Xref.setEntityType(contactAddressModel.getEntityType());
                        Xref.setCapID(vToCapId);
                        // commit address
                        commitAddress = aa.address.createXRefContactAddress(Xref.getXRefContactAddressModel());
                        if (commitAddress.getSuccess()) {
                            commitAddress.getOutput();
                            logDebug("Copied contact address");
                        }
                    }
                }
            }
            // end if
            copied++;
            logDebug("Copied contact from " + pFromCapId.getCustomID() + " to " + vToCapId.getCustomID());
        }
    }
    else {
        logMessage("**ERROR: Failed to get contacts: " + capContactResult.getErrorMessage());
        return false;
    }
    return copied;
}

function changeCapContactTypes(origType, newType) {
    // Renames all contacts of type origType to contact type of newType and includes Contact Address objects
    //
    var vCapId = capId;
    if (arguments.length == 3)
        vCapId = arguments[2];

    var capContactResult = aa.people.getCapContactByCapID(vCapId);
    var renamed = 0;
    if (capContactResult.getSuccess()) {
        var Contacts = capContactResult.getOutput();
        for (yy in Contacts) {
            var contact = Contacts[yy].getCapContactModel();

            var people = contact.getPeople();
            var contactType = people.getContactType();
            aa.print("Contact Type " + contactType);

            if (contactType == origType) {

                var contactNbr = people.getContactSeqNumber();
                var editContact = aa.people.getCapContactByPK(vCapId, contactNbr).getOutput();
                editContact.getCapContactModel().setContactType(newType)

                aa.print("Set to: " + people.getContactType());
                renamed++;

                var updContactResult = aa.people.editCapContact(editContact.getCapContactModel());
                logDebug("contact " + updContactResult);
                logDebug("contact.getSuccess() " + updContactResult.getSuccess());
                logDebug("contact.getOutput() " + updContactResult.getOutput());
                updContactResult.getOutput();
                logDebug("Renamed contact from " + origType + " to " + newType);
            }
        }
    }
    else {
        logMessage("**ERROR: Failed to get contacts: " + capContactResult.getErrorMessage());
        return false;
    }
    return renamed;
}

function checkWorkflowTaskAndStatus(capId, workflowTask, taskStatus) {
    var workflowResult = aa.workflow.getTasks(capId);
    if (workflowResult.getSuccess())
        wfObj = workflowResult.getOutput();
    else {
        aa.print("**ERROR: Failed to get workflow object: " + wfObj);
        return false;
    }

    for (i in wfObj) {
        fTask = wfObj[i];
        var status = fTask.getDisposition();
        var taskDesc = fTask.getTaskDescription();

        if (status != null && taskDesc != null && taskDesc.equals(workflowTask) && status.equals(taskStatus))
            return true;
    }

    return false;
}

function associatedRefContactWithRefLicProf(capIdStr, refLicProfSeq, servProvCode, auditID) {
    var contact = getLicenseHolderByLicenseNumber(capIdStr);
    if (contact && contact.getRefContactNumber()) {
        linkRefContactWithRefLicProf(parseInt(contact.getRefContactNumber()), refLicProfSeq, servProvCode, auditID)
    }
    else {
        logMessage("**ERROR:cannot find license holder of license");
    }
}

function linkRefContactWithRefLicProf(refContactSeq, refLicProfSeq, servProvCode, auditID) {

    if (refContactSeq && refLicProfSeq && servProvCode && auditID) {
        var xRefContactEntity = aa.people.getXRefContactEntityModel().getOutput();
        xRefContactEntity.setServiceProviderCode(servProvCode);
        xRefContactEntity.setContactSeqNumber(refContactSeq);
        xRefContactEntity.setEntityType("PROFESSIONAL");
        xRefContactEntity.setEntityID1(refLicProfSeq);
        var auditModel = xRefContactEntity.getAuditModel();
        auditModel.setAuditDate(new Date());
        auditModel.setAuditID(auditID);
        auditModel.setAuditStatus("A")
        xRefContactEntity.setAuditModel(auditModel);
        var xRefContactEntityBusiness = aa.proxyInvoker.newInstance("com.accela.aa.aamain.people.XRefContactEntityBusiness").getOutput();
        var existedModel = xRefContactEntityBusiness.getXRefContactEntityByUIX(xRefContactEntity);
        if (existedModel.getContactSeqNumber()) {
            //aa.print("The professional license have already linked to contact.");
            logMessage("License professional link to reference contact successfully.");
        }
        else {
            var XRefContactEntityCreatedResult = xRefContactEntityBusiness.createXRefContactEntity(xRefContactEntity);
            if (XRefContactEntityCreatedResult) {
                //aa.print("License professional link to reference contact successfully.");
                logMessage("License professional link to reference contact successfully.");
            }
            else {
                //aa.print("**ERROR:License professional failed to link to reference contact.  Reason: " +  XRefContactEntityCreatedResult.getErrorMessage());
                logMessage("**ERROR:License professional failed to link to reference contact.  Reason: " + XRefContactEntityCreatedResult.getErrorMessage());
            }
        }
    }
    else {
        //aa.print("**ERROR:Some Parameters are empty");
        logMessage("**ERROR:Some Parameters are empty");
    }

}

function getConatctAddreeByID(contactID, vAddressType) {
    var conArr = new Array();
    var capContResult = aa.people.getCapContactByContactID(contactID);

    if (capContResult.getSuccess()) {
        conArr = capContResult.getOutput();
        for (contact in conArr) {
            cont = conArr[contact];

            return getContactAddressByContact(cont.getCapContactModel(), vAddressType);
        }
    }
}

function getContactAddressByContact(contactModel, vAddressType) {
    var xrefContactAddressBusiness = aa.proxyInvoker.newInstance("com.accela.aa.aamain.address.XRefContactAddressBusiness").getOutput();
    var contactAddressArray = xrefContactAddressBusiness.getContactAddressListByCapContact(contactModel);
    for (i = 0; i < contactAddressArray.size() ; i++) {
        var contactAddress = contactAddressArray.get(i);
        if (vAddressType.equals(contactAddress.getAddressType())) {
            return contactAddress;
        }
    }
}

function copyContactAddressToLicProf(contactAddress, licProf) {
    if (contactAddress && licProf) {
        licProf.setAddress1(contactAddress.getAddressLine1());
        licProf.setAddress2(contactAddress.getAddressLine2());
        licProf.setAddress3(contactAddress.getAddressLine3());
        licProf.setCity(contactAddress.getCity());
        licProf.setState(contactAddress.getState());
        licProf.setZip(contactAddress.getZip());
        licProf.getLicenseModel().setCountryCode(contactAddress.getCountryCode());
    }
}


function associatedLicensedProfessionalWithPublicUser(licnumber, publicUserID) {
    var mylicense = aa.licenseScript.getRefLicenseProfBySeqNbr(aa.getServiceProviderCode(), licnumber);
    var puser = aa.publicUser.getPublicUserByPUser(publicUserID);
    if (puser.getSuccess())
        aa.licenseScript.associateLpWithPublicUser(puser.getOutput(), mylicense.getOutput());
}

function taskCloseAllAdjustBranchtaskExcept(e, t) {
    var n = new Array;
    var r = false;
    if (arguments.length > 2) {
        for (var i = 2; i < arguments.length; i++)
            n.push(arguments[i])
    } else
        r = true;
    var s = aa.workflow.getTasks(capId);
    if (s.getSuccess())
        var o = s.getOutput();
    else {
        logMessage("**ERROR: Failed to get workflow object: " + s.getErrorMessage());
        return false
    }
    var u;
    var a;
    var f;
    var l = aa.date.getCurrentDate();
    var c = " ";
    var h;
    for (i in o) {
        u = o[i];
        h = u.getTaskDescription();
        a = u.getStepNumber();
        if (r) {
            aa.workflow.handleDisposition(capId, a, e, l, c, t, systemUserObj, "B");
            logMessage("Closing Workflow Task " + h + " with status " + e);
            logDebug("Closing Workflow Task " + h + " with status " + e)
        } else {
            if (!exists(h, n)) {
                aa.workflow.handleDisposition(capId, a, e, l, c, t, systemUserObj, "B");
                logMessage("Closing Workflow Task " + h + " with status " + e);
                logDebug("Closing Workflow Task " + h + " with status " + e)
            }
        }
    }
}

function getLicenseHolderByLicenseNumber(capIdStr) {
    var capContactResult = aa.people.getCapContactByCapID(capIdStr);
    if (capContactResult.getSuccess()) {
        var Contacts = capContactResult.getOutput();
        for (yy in Contacts) {
            var contact = Contacts[yy].getCapContactModel();
            var contactType = contact.getContactType();
            if (contactType.toUpperCase().equals("LICENSE HOLDER") && contact.getRefContactNumber()) {
                return contact;
            }
        }
    }
}

function taskCloseAllExcept(pStatus, pComment) {
    // Closes all tasks in CAP with specified status and comment
    // Optional task names to exclude
    // 06SSP-00152
    //
    var taskArray = new Array();
    var closeAll = false;
    if (arguments.length > 2) //Check for task names to exclude
    {
        for (var i = 2; i < arguments.length; i++)
            taskArray.push(arguments[i]);
    }
    else
        closeAll = true;

    var workflowResult = aa.workflow.getTasks(capId);
    if (workflowResult.getSuccess())
        var wfObj = workflowResult.getOutput();
    else {
        logMessage("**ERROR: Failed to get workflow object: " + workflowResult.getErrorMessage());
        return false;
    }

    var fTask;
    var stepnumber;
    var processID;
    var dispositionDate = aa.date.getCurrentDate();
    var wfnote = " ";
    var wftask;

    for (i in wfObj) {
        fTask = wfObj[i];
        wftask = fTask.getTaskDescription();
        stepnumber = fTask.getStepNumber();
        //processID = fTask.getProcessID();
        if (closeAll) {
            aa.workflow.handleDisposition(capId, stepnumber, pStatus, dispositionDate, wfnote, pComment, systemUserObj, "Y");
            logMessage("Closing Workflow Task " + wftask + " with status " + pStatus);
            logDebug("Closing Workflow Task " + wftask + " with status " + pStatus);
        }
        else {
            if (!exists(wftask, taskArray)) {
                aa.workflow.handleDisposition(capId, stepnumber, pStatus, dispositionDate, wfnote, pComment, systemUserObj, "Y");
                logMessage("Closing Workflow Task " + wftask + " with status " + pStatus);
                logDebug("Closing Workflow Task " + wftask + " with status " + pStatus);
            }
        }
    }
}

function getInspResultByInspType(capId, inspType) {
    var inspResultObj = aa.inspection.getInspections(capId);
    if (inspResultObj.getSuccess()) {
        var inspList = inspResultObj.getOutput();
        for (xx in inspList)
            if (String(inspType).equals(inspList[xx].getInspectionType()))
                return inspList[xx].getInspectionStatus();
    }
    return "Not Found";
}

function getNextAvailableExpDate(fixedDates, permitIssuedDate) {
    if (fixedDates != undefined && fixedDates != null && fixedDates != "null") {
        var issuedDate = new Date(permitIssuedDate);
        //remove empty elements
        var fixedDates = fixedDates.split(";").filter(function (e) { return e });
        var permitIssuedDate = issuedDate.getMonth() + 1
                            + "/" + issuedDate.getDate();

        fixedDates.push(permitIssuedDate);
        //sort dates
        fixedDates.sort(function (a, b) {
            var aMonth = a.split("/")[0];
            var bMonth = b.split("/")[0];
            var aDay = a.split("/")[1];
            var bDay = b.split("/")[1];
            if (aMonth != bMonth)
                return aMonth - bMonth;
            else
                return aDay - bDay;
        });
        for (i in fixedDates) {
            aa.print(fixedDates[i]);
        }

        var lastIndex = fixedDates.lastIndexOf(permitIssuedDate) + 1;
        if (lastIndex < fixedDates.length) {
            return fixedDates[lastIndex] + "/" + issuedDate.getFullYear();
        } else if (lastIndex = fixedDates.length) {
            return fixedDates[0] + "/" + (issuedDate.getFullYear() + 1);
        }
    }

    return null;
}

function dateAddMonths(pDate, pMonths) {
    // Adds specified # of months (pMonths) to pDate and returns new date as string in format MM/DD/YYYY
    // If pDate is null, uses current date
    // pMonths can be positive (to add) or negative (to subtract) integer
    // If pDate is on the last day of the month, the new date will also be end of month.
    // If pDate is not the last day of the month, the new date will have the same day of month, unless such a day doesn't exist in the month, in which case the new date will be on the last day of the month
    //
    if (!pDate)
        baseDate = new Date();
    else
        baseDate = new Date(pDate);

    //change pMonths to integer (any decimal values are removed)
    if (parseInt(pMonths) >= 0)
        var vMonths = Math.floor(parseInt(pMonths));
    else
        var vMonths = Math.ceil(parseInt(pMonths));

    var baseMM = baseDate.getMonth() + 1;
    var baseDD = baseDate.getDate();
    var baseYY = baseDate.getFullYear();

    //Determine if pDate is last day of month
    var monthEnd = false;
    if ((baseMM == 1 || baseMM == 3 || baseMM == 5 || baseMM == 7 || baseMM == 8 || baseMM == 10 || baseMM == 12) && baseDD == 31 ||
        (baseMM == 4 || baseMM == 6 || baseMM == 9 || baseMM == 11) && baseDD == 30 ||
        baseMM == 2) {
        if (baseMM != 2)
            monthEnd = true;
        else {
            if (baseYY % 4 == 0 && baseDD == 29) //leap year
                monthEnd = true;
            if (baseYY % 4 != 0 && baseDD == 28)
                monthEnd = true;
        }
    }

    var totMonths = baseMM + vMonths;
    if (totMonths > 0) {
        //calc new year
        var addYears = (totMonths == 12 ? 0 : Math.floor((totMonths - 1) / 12));
        var newYY = baseYY + addYears;
        //calc new month
        var newMM = (totMonths % 12 == 0 ? 12 : totMonths % 12);
    } else {
        //calc new year
        var addYears = (Math.ceil(totMonths / 12) - 1);
        var newYY = baseYY + addYears;
        //calc new month
        var newMM = totMonths % 12 + 12;
    }
    //calc new date (dd)
    if (newMM == 1 || newMM == 3 || newMM == 5 || newMM == 7 || newMM == 8 || newMM == 10 || newMM == 12) {
        if (monthEnd)
            var newDD = 31;
        else
            var newDD = baseDD;
    } else if (newMM == 4 || newMM == 6 || newMM == 9 || newMM == 11) {
        if (monthEnd)
            var newDD = 30;
        else
            var newDD = (baseDD > 30 ? 30 : baseDD);
    } else if (newYY % 4 == 0) //Feb of leap year
    {
        if (monthEnd)
            var newDD = 29;
        else
            var newDD = (baseDD > 29 ? 29 : baseDD);
    } else //Feb of non-leap year
    {
        if (monthEnd)
            var newDD = 28;
        else
            var newDD = (baseDD > 28 ? 28 : baseDD);
    }
    var newDate = "" + newMM + "/" + newDD + "/" + newYY;
    logDebug("New Date is " + newDate);
    return newDate;
}

function scheduleInspectionBasedOnRisk(permitID, permitIssueDate, risk) {

    var inspRes = aa.person.getUser(currentUserID);
    if (inspRes.getSuccess())
        var inspectorObj = inspRes.getOutput();
    var DaysAhead = 0;

    if (risk == 1) {
        DaysAhead = Math.round((new Date(dateAddMonths(permitIssueDate, 12)) - new Date()) / (1000 * 60 * 60 * 24) + 1);
    } else if (risk == 2) {
        DaysAhead = Math.round((new Date(dateAddMonths(permitIssueDate, 6)) - new Date()) / (1000 * 60 * 60 * 24) + 1);
    } else if (risk == 3) {
        DaysAhead = Math.round((new Date(dateAddMonths(permitIssueDate, 4)) - new Date()) / (1000 * 60 * 60 * 24) + 1);
    } else if (risk == 4) {
        DaysAhead = Math.round((new Date(dateAddMonths(permitIssueDate, 3)) - new Date()) / (1000 * 60 * 60 * 24) + 1);
    }

    var schedRes = aa.inspection.scheduleInspection(permitID, inspectorObj, aa.date.parseDate(dateAdd(null, DaysAhead)), null, "Routine Inspection", "Scheduled via Script");
    if (schedRes.getSuccess())
        logDebug("Successfully scheduled inspection : Routine Inspection for " + dateAdd(null, DaysAhead));
    else
        logDebug("**ERROR: adding  inspection (Routine Inspection): " + schedRes.getErrorMessage());
}

//CTRCA SCRIPTS
function updateCapAldID() {
    logDebug("updateCapAldID()");

    if (!parentCapId) {
        parentProject = aa.cap.getProjectByChildCapID(capId, "Renewal", "").getOutput();
        pojectCapId = parentProject[0].getProjectID();
        parentCapId = aa.cap.getCapID(pojectCapId.getID1(), pojectCapId.getID2(), pojectCapId.getID3()).getOutput();
    }

    var parentAltId = aa.cap.getCap(parentCapId).getOutput().getCapModel().altID;
    Avo_LogDebug("parentCapId(" + parentAltId + ")", 2);    //debug

    //var newAltId = getNewAltID(parentCapId);
    var altId = aa.cap.getCap(capId).getOutput().getCapModel().altID;
    var aldIdArray = altId.split("-");
    var newAltId = aldIdArray[0] + "-" + parentAltId + "-" + aldIdArray[2];
    Avo_LogDebug("updated AltId(" + newAltId + ")", 2);    //debug
    if (!newAltId) {
        return;
    }

    var result = aa.cap.updateCapAltID(capId, newAltId).getOutput();
    if (result) {
        Avo_LogDebug("Update Alt ID Succesfully.", 1);
    }
    else {
        Avo_LogDebug("Update Alt ID Failed.", 1);
    }
}

function getNewAltID(parentLicenseCAPID) {
    var parentAltId = aa.cap.getCap(parentLicenseCAPID).getOutput().getCapModel().altID;
    logDebug("getNewAltID(" + parentAltId + ")");

    var capAldId = capId.getCustomID();
    Avo_LogDebug("capId(" + capAldId + ")", 2);    //debug

    var childList = aa.cap.getProjectByMasterID(parentLicenseCAPID, "Renewal", "Complete").getOutput();

    var year = new Date().getFullYear().toString();
    var count = 1;
    if (childList) {
        Avo_LogDebug("Children(" + childList.length + ")", 2);    //debug
        for (x in childList) {
            renewcap = childList[x].getCapID();
            var cap = aa.cap.getCapID(renewcap.getID1(), renewcap.getID2(), renewcap.getID3()).getOutput();

            //var tmpString1 = cap.getCustomID().substring(0,8);
            var tmpString1 = cap.getCustomID();
            Avo_LogDebug("Index(" + tmpString1.indexOf(year) + ")", 2);    //debug

            if (tmpString1.indexOf(year) > -1) {
                count++;
            }
        }
    }

    if (count < 10) {
        count = "0" + count;
    }

    Avo_LogDebug("Count(" + count + ")", 2);    //debug

    var capAldIdArray = capAldId.split("-");
    //var parentAltIdArray = parentAltId.split("-");
    //capAldIdArray[2] = parentAltIdArray[1];

    return capAldIdArray[0] + "-" + parentAltId + "-" + capAldIdArray[2] + "-" + count;
}

function deleteAllFee() {
    var feeList = aa.finance.getFeeItemByCapID(capId).getOutput();

    if (feeList || feeList != "") {
        for (x in feeList) {
            try {
                aa.finance.removeFeeItem(capId, feeList[x].getFeeSeqNbr());

            } catch (e) {
                aa.logDebug("Message:" + e);
            }
        }
    }
}

function licEditExpInfoforRenewal(pExpStatus, pExpDate) {
    //Edits expiration status and/or date
    //Needs licenseObject function
    //06SSP-00238
    //
    if (arguments.length > 2) {
        tmpCapId = arguments[2]; // subprocess
        var lic = new licenseObject(null, tmpCapId);

    }
    else {
        var lic = new licenseObject(null);

    }
    if (pExpStatus != null) {
        lic.setStatus(pExpStatus);
    }

    if (pExpDate != null) {
        lic.setExpiration(pExpDate);
    }
}

function isReadyRenew(capid) {
    if (capid == null || aa.util.instanceOfString(capid)) {
        return false;
    }
    var result = aa.expiration.isExpiredLicenses(capid);
    if (result.getSuccess()) {
        return true;
    }
    else {
        logDebug("ERROR: Failed to get expiration with CAP(" + capid + "): " + result.getErrorMessage());
    }
    return false;
}

/*******************************************************
| Script/Function: Avo_CopyDocuments()
| Usage: Copy all documents from pFromCapId to pToCapId
| Modified by: ()
*********************************************************/
function Avo_CopyDocuments(pFromCapId, pToCapId) {
    var fromAltId = aa.cap.getCap(pFromCapId).getOutput().getCapModel().altID;
    var toAltId = aa.cap.getCap(pToCapId).getOutput().getCapModel().altID;
    Avo_LogDebug("Avo_CopyDocuments(" + fromAltId + ", " + toAltId + ")", 1);

    //Copies all attachments (documents) from pFromCapId to pToCapId
    var vFromCapId = pFromCapId;
    var vToCapId = pToCapId;
    var categoryArray = new Array();

    // third optional parameter is comma delimited list of categories to copy.
    if (arguments.length > 2) {
        categoryList = arguments[2];
        categoryArray = categoryList.split(",");
    }

    var capDocResult = aa.document.getDocumentListByEntity(capId, "CAP");
    if (!capDocResult.getSuccess()) {
        return;
    }
    if (capDocResult.getOutput().size() == 0) {
        return;
    }

    for (docInx = 0; docInx < capDocResult.getOutput().size() ; docInx++) {
        var documentObject = capDocResult.getOutput().get(docInx);
        currDocCat = "" + documentObject.getDocCategory();
        if (categoryArray.length == 0 || exists(currDocCat, categoryArray)) {
            // download the document content
            var useDefaultUserPassword = true;
            //If useDefaultUserPassword = true, there is no need to set user name & password, but if useDefaultUserPassword = false, we need define EDMS user name & password.
            var EMDSUsername = null;
            var EMDSPassword = null;
            var downloadResult = aa.document.downloadFile2Disk(documentObject, documentObject.getModuleName(), EMDSUsername, EMDSPassword, useDefaultUserPassword);
            if (downloadResult.getSuccess()) {
                var path = downloadResult.getOutput();
                Avo_LogDebug("path(" + path + ")", 2);
            }
            var tmpEntId = vToCapId.getID1() + "-" + vToCapId.getID2() + "-" + vToCapId.getID3();
            documentObject.setDocumentNo(null);
            documentObject.setCapID(vToCapId)
            documentObject.setEntityID(tmpEntId);

            // Open and process file
            try {
                // put together the document content - use java.io.FileInputStream
                var newContentModel = aa.document.newDocumentContentModel().getOutput();
                inputstream = new java.io.FileInputStream(path);
                newContentModel.setDocInputStream(inputstream);
                documentObject.setDocumentContent(newContentModel);
                var newDocResult = aa.document.createDocument(documentObject);
                if (newDocResult.getSuccess()) {
                    newDocResult.getOutput();
                    Avo_LogDebug("Successfully copied document: " + documentObject.getFileName(), 1);
                }
                else {
                    Avo_LogDebug("Failed to copy document: " + documentObject.getFileName(), 1);
                    Avo_LogDebug(newDocResult.getErrorMessage(), 1);
                }
            }
            catch (err) {
                Avo_LogDebug("Error copying document: " + err.message, 1);
                return false;
            }
        }
    } // end for loop

    var pFromAltId = aa.cap.getCap(pFromCapId).getOutput().getCapModel().altID;
    var pToAltId = aa.cap.getCap(pToCapId).getOutput().getCapModel().altID;
    Avo_LogDebug("Copied all documents from " + pFromAltId + " to " + pToAltId, 1);
}

/*******************************************************
| Script/Function: Avo_GetDateFromAccelaDateString()
| Usage: Convert an Accela formatted date string into a js date object
| Modified by: ()
*********************************************************/
function Avo_GetDateFromAccelaDateString(dateStr) {
    Avo_LogDebug("Avo_GetDateFromAccelaDateString(" + dateStr + ")", 1);

    if (!dateStr) {
        Avo_LogDebug("date string is empty or null", 1);
        return null;
    }

    var dateStr = String(dateStr).trim();
    var dateStr = dateStr.split(" ")[0];

    var year, month, day;

    if (dateStr.indexOf("/") != -1) {
        var dateArr = dateStr.split("/");
        year = dateArr[2];
        month = dateArr[0];
        day = dateArr[1];
    } else if (dateStr.indexOf("-") != -1) {
        var dateArr = dateStr.split("-");
        year = dateArr[0];
        month = dateArr[1];
        day = dateArr[2];
    } else {
        year = dateStr.substring(4, dateStr.length);
        month = dateStr.substring(0, 2);
        day = dateStr.substring(2, 4);
    }

    if (!year) {
        return;
    }
    if (!month) {
        return;
    }
    if (!day) {
        return;
    }

    //Avo_LogDebug("Year(" + year + ")", 2); //debug
    //Avo_LogDebug("Month(" + month + ")", 2);   //debug
    //Avo_LogDebug("Day(" + day + ")", 2);   //debug

    var date = new Date(year, month - 1, day);
    return date;
}

/*******************************************************
| Script/Function: Avo_HasUnpaidFeesWithStatus(recordId, feeStatus)
| Created by: Nicolaj Bunting
| Created on: 14Dec17
| Usage: If record has any fees with status fee status and a balance > 0 then return true else return false
| Modified by: ()
*********************************************************/
function Avo_HasUnpaidFeesWithStatus(recordId, feeStatus) {
    var altId = aa.cap.getCap(recordId).getOutput().getCapModel().altID;
    Avo_LogDebug("Avo_HasUnpaidFeesWithStatus(" + altId + ", " + feeStatus + ")", 1);

    feeStatus = String(feeStatus).toUpperCase();

    var feeIds = new Array();

    var capDetailsResult = aa.cap.getCapDetail(recordId);
    if (capDetailsResult.getSuccess()) {
        capDetails = capDetailsResult.getOutput();
        var balanceDue = capDetails.getBalance();
        Avo_LogDebug("Balance($" + balanceDue + ")", 2);   //debug

        if (balanceDue <= 0) {
            return false;
        }
    }

    var feeItems = aa.fee.getFeeItems(recordId).getOutput();
    for (i in feeItems) {
        var feeItem = feeItems[i];
        var matchingStatus = feeItem.feeitemStatus == feeStatus;
        if (matchingStatus == false) {
            continue;
        }

        feeIds[feeItems[i].feeSeqNbr] = feeItems[i].fee;
    }

    var paymentItems = aa.finance.getPaymentFeeItems(recordId, null).getOutput();
    for (i in paymentItems) {
        var feeIdToCheck = paymentItems[i].feeSeqNbr;
        for (b in feeIds) {
            var feeId = b;
            if (feeId != feeIdToCheck) {
                continue;
            }

            Avo_LogDebug("Fee code(" + feeId + ")", 2); //debug

            var amount = feeIds[b];
            Avo_LogDebug("Fee ($" + amount + ")", 2);   //debug

            var paid = paymentItems[i].feeAllocation;
            Avo_LogDebug("Paid ($" + paid + ")", 2);   //debug

            feeIds[feeId] = (amount - paid);
        }
    }

    for (i in feeIds) {
        var feeId = i;
        var amount = feeIds[i];

        if (amount <= 0) {
            continue;
        }

        return true;
    }

    return false;
}

/*******************************************************
| Script/Function: Avo_GetToday()
| Created by: Nicolaj Bunting
| Usage: get a javascript date object for today's date with time set to midnight
| Modified by: ()
*********************************************************/
function Avo_GetToday() {
    Avo_LogDebug("Avo_GetToday()", 1);

    var today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);
    return today;
}

/*******************************************************
| Script/Function: Avo_GetQuarterPercentage()
| Created by: Nicolaj Bunting
| Usage: get the prorated fee percentage based on the quarter of dateStr
| Modified by: ()
*********************************************************/
function Avo_GetQuarterPercentage(dateStr) {
    dateStr = String(dateStr);
    Avo_LogDebug("Avo_GetQuarterPercentage(" + dateStr + ")", 1);

    var date = Avo_GetDateFromAccelaDateString(dateStr);
    var month = date.getMonth() + 1;
    Avo_LogDebug("Month(" + month + ")", 2);   //debug

    var quantity = 1;

    switch (month) {
        case 1:
        case 2:
        case 3:
            break;

        case 4:
        case 5:
        case 6:
            quantity = 0.75;
            break;

        case 7:
        case 8:
        case 9:
            quantity = 0.5;
            break;

        case 10:
        case 11:
        case 12:
            quantity = 0.25;
            break;

        default:
            break;
    }

    return quantity;
}

/*******************************************************
| Script/Function: Avo_UpperContactFields()
| Created by: Nicolaj Bunting
| Created on: 02Feb18
| Usage: Convert all text fields in contact to uppercase
| Modified by: ()
*********************************************************/
function Avo_UpperContactFields(capContactModel) {
    Avo_LogDebug("Avo_UpperContactFields(" + capContactModel + ")", 1);

    //Avo_LogDebug(Serialize(capContactModel, false), 2);

    if (capContactModel.addressLine1) {
        capContactModel.addressLine1 = String(capContactModel.addressLine1).toUpperCase();
    }
    if (capContactModel.addressLine2) {
        capContactModel.addressLine2 = String(capContactModel.addressLine2).toUpperCase();
    }
    if (capContactModel.addressLine3) {
        capContactModel.addressLine3 = String(capContactModel.addressLine3).toUpperCase();
    }
    //capContactModel.relation = String(capContactModel.relation).toUpperCase();
    //capContactModel.contactName = String(capContactModel.contactName).toUpperCase();
    if (capContactModel.namesuffix) {
        capContactModel.namesuffix = String(capContactModel.namesuffix).toUpperCase();
    }
    if (capContactModel.businessName) {
        capContactModel.businessName = String(capContactModel.businessName).toUpperCase();
    }
    if (capContactModel.tradeName) {
        capContactModel.tradeName = String(capContactModel.tradeName).toUpperCase();
    }
    if (capContactModel.salutation) {
        capContactModel.salutation = String(capContactModel.salutation).toUpperCase();
    }
    if (capContactModel.firstName) {
        capContactModel.firstName = String(capContactModel.firstName).toUpperCase();
    }
    if (capContactModel.middleName) {
        capContactModel.middleName = String(capContactModel.middleName).toUpperCase();
    }
    if (capContactModel.lastName) {
        capContactModel.lastName = String(capContactModel.lastName).toUpperCase();
    }
    if (capContactModel.fullName) {
        capContactModel.fullName = String(capContactModel.fullName).toUpperCase();
    }
    if (capContactModel.city) {
        capContactModel.city = String(capContactModel.city).toUpperCase();
    }
    if (capContactModel.state) {
        capContactModel.state = String(capContactModel.state).toUpperCase();
    }

    var result;

    var peopleModel = capContactModel.people;
    if (peopleModel) {
        //    //peopleModel.contactName = String(peopleModel.contactName).toUpperCase();
        //    peopleModel.salutation = String(peopleModel.salutation).toUpperCase();
        //    peopleModel.namesuffix = String(peopleModel.namesuffix).toUpperCase();
        //    peopleModel.firstName = String(peopleModel.firstName).toUpperCase();
        //    //peopleModel.dispFirstName = String(peopleModel.dispFirstName).toUpperCase();
        //    //peopleModel.resFirstName = String(peopleModel.resFirstName).toUpperCase();
        //    peopleModel.middleName = String(peopleModel.middleName).toUpperCase();
        //    //peopleModel.dispMiddleName = String(peopleModel.dispMiddleName).toUpperCase();
        //    //peopleModel.resMiddleName = String(peopleModel.resMiddleName).toUpperCase();
        //    peopleModel.lastName = String(peopleModel.lastName).toUpperCase();
        //    //peopleModel.dispLastName = String(peopleModel.dispLastName).toUpperCase();
        //    //peopleModel.resLastName = String(peopleModel.resLastName).toUpperCase();
        //    peopleModel.businessName = String(peopleModel.businessName).toUpperCase();
        //    peopleModel.tradeName = String(peopleModel.tradeName).toUpperCase();
        //    peopleModel.gender = String(peopleModel.gender).toUpperCase();
        //    peopleModel.contactType = String(peopleModel.contactType).toUpperCase();
        //    peopleModel.relation = String(peopleModel.relation).toUpperCase();
        //    peopleModel.country = String(peopleModel.country).toUpperCase();
        //    peopleModel.email = String(peopleModel.email).toUpperCase();
        //    peopleModel.contactTypeFlag = String(peopleModel.contactTypeFlag).toUpperCase();

        var allAddresses = peopleModel.contactAddressList.toArray();
        for (i in allAddresses) {
            var contactAddressModel = allAddresses[i];

            contactAddressModel.addressLine1 = String(contactAddressModel.addressLine1).toUpperCase();
            contactAddressModel.city = String(contactAddressModel.city).toUpperCase();
            contactAddressModel.state = String(contactAddressModel.state).toUpperCase();
            contactAddressModel.countryCode = String(contactAddressModel.countryCode).toUpperCase();

            var result = aa.address.editContactAddress(contactAddressModel);
            if (result.getSuccess()) {
                Avo_LogDebug("Set all fields in contact address model to uppercase", 1);
            } else {
                Avo_LogDebug("Failed to update contact address model. " + result.getErrorMessage(), 1);
            }
        }

        //result = aa.people.editPeople(peopleModel);
        //if (result.getSuccess()) {
        //    Avo_LogDebug("Set all fields in people model to uppercase", 1);
        //} else {
        //    Avo_LogDebug(result.getErrorMessage(), 1);
        //}

        //capContactModel.people = peopleModel;
    }

    //Avo_LogDebug(Serialize(people, false), 2);

    result = aa.people.editCapContact(capContactModel);
    if (result.getSuccess()) {
        Avo_LogDebug("Set all fields in cap contact model to uppercase", 1);
    } else {
        Avo_LogDebug("Failed to update cap contact model. " + result.getErrorMessage(), 1);
    }

    var contactAttrArray = aa.env.getValue("ContactAttribute")
    //Avo_LogDebug(Serialize(contactAttrArray, false), 2);
}

/*******************************************************
| Script/Function: Avo_LogDebug(debug)
| Created by: Nicolaj Bunting
| Created on: 05Feb18
| Usage: Display debug output based on importance and current debug level
| Modified by: ()
*********************************************************/
function Avo_LogDebug(debug, importance) {
    //logDebug("Avo_LogDebug(" + debug + ", " + importance.toString() + ")");

    if (br == null || typeof (br) == "undefined") {
        br = "<br/>";
    }

    if (isNaN(importance) == true) {
        var message = "Importance must be an integer";
        if (useLogDebug == true) {
            logDebug(message);
        } else {
            aa.print(message + br);
        }
        return;
    }

    if (importance < 0) {
        var message = "Importance can't be negative";
        if (useLogDebug == true) {
            logDebug(message);
        } else {
            aa.print(message + br);
        }
    }

    if (importance > debugLevel) {
        return;
    }

    if (useLogDebug == true) {
        logDebug(debug);
    } else {
        aa.print(debug + br);
    }
}

/*******************************************************
| Script/Function: Avo_UpperAddressFields()
| Created by: Nicolaj Bunting
| Created on: 02Feb18
| Usage: Convert all text fields in address to uppercase
| Modified by: ()
*********************************************************/
function Avo_UpperAddressFields(addressModel) {
    Avo_LogDebug("Avo_UpperAddressFields(" + addressModel + ")", 1);

    // Get actual addressModel
    var addressId = addressModel.addressId;
    Avo_LogDebug("Address Id(" + addressId + ")", 2);  //debug

    var addressScriptModel = aa.address.getAddressByAddressId(addressId, aa.util.newQueryFormat()).getOutput()[0];
    addressModel = addressScriptModel.addressModel;

    //Avo_LogDebug(Serialize(addressModel, false), 2);

    if (addressModel.unitStart) {
        addressModel.unitStart = String(addressModel.unitStart).toUpperCase();
    }
    if (addressModel.streetDirection) {
        addressModel.streetDirection = String(addressModel.streetDirection).toUpperCase();
    }
    if (addressModel.fullAddress) {
        addressModel.fullAddress = String(addressModel.fullAddress).toUpperCase();
    }
    if (addressModel.streetName) {
        addressModel.streetName = String(addressModel.streetName).toUpperCase();
    }
    if (addressModel.city) {
        addressModel.city = String(addressModel.city).toUpperCase();
    }
    if (addressModel.displayAddress) {
        addressModel.displayAddress = String(addressModel.displayAddress).toUpperCase();
    }
    if (addressModel.state) {
        addressModel.state = String(addressModel.state).toUpperCase();
    }

    //Avo_LogDebug(Serialize(addressModel, false), 2);

    var result = aa.address.editAddress(addressModel);
    if (result.getSuccess()) {
        Avo_LogDebug("Set all fields in address model to uppercase", 1);
    } else {
        Avo_LogDebug(result.getErrorType() + ": " + result.getErrorMessage(), 1);
    }

    //aa.env.setValue("AddressModel", addressModel);
}

/*******************************************************
| Script/Function: Avo_SetAppName([recordId]) (ID64)
| Created by: Nicolaj Bunting
| Created on: 20Feb18
| Usage: Set the app name to Primary/Organization/Only Contact's Legal Business Name, DBA Name, or First Name Last Name and capitolize
| Modified by: Nic Bunting (9Mar18)
| Modified by: Nic Bunting (14May18)
| Modified by: Nic Bunting (21Jun18)
| Modified by: Nic Bunting (1Aug18)
| Modified by: Nic Bunting (14Jun19)
*********************************************************/
function Avo_SetAppName(recordId) {
    if (arguments.length == 0 || !arguments[0]) {
        recordId = capId;
    }

    var altId = aa.cap.getCap(recordId).getOutput().getCapModel().altID;
    Avo_LogDebug("ID64 Avo_SetAppName(" + altId + ")", 1);

    var appName = null;

    //var contactTypeToFind = "Applicant";
    //var allContacts = getContactArray(recordId);
    //for (a in allContacts) {
    //    var contactType = String(allContacts[a].contactType);
    //    if (contactType != contactTypeToFind) {
    //        continue;
    //    }

    //    var contact = allContacts[a];

    //    // DBA Name
    //    if (contact.peopleModel.tradeName) {
    //        appName = contact.peopleModel.tradeName;
    //        break;
    //    }

    //    // Legal Business Name
    //    if (contact.businessName) {
    //        appName = contact.businessName;
    //        break;
    //    }

    //    // First Name, Last Name
    //    appName = contact.firstName + " " + contact.lastName;
    //    break;
    //}

    var allContacts = aa.people.getCapContactByCapID(recordId).getOutput();
    Avo_LogDebug("Contacts(" + allContacts.length + ")", 2);    //debug

    //for (a in allContacts) {
    //    var contact = allContacts[a];
    //    var contactId = contact.contactSeqNumber;
    //    Avo_LogDebug("Contact Id(" + contactId + ")", 2);   //debug

    //    if (allContacts.length > 1) {
    //        var capContactModel = contact.capContactModel;
    //        var flag = capContactModel.primaryFlag;
    //        Avo_LogDebug("Cap Contact Model Flag(" + flag + ")", 2);	//debug

    //        var peopleModel = contact.people;
    //        if (!flag) {
    //            flag = peopleModel.flag;
    //            Avo_LogDebug("People Model Flag(" + flag + ")", 2);	//debug
    //        }

    //        if (!flag) {
    //            flag = capContactModel.people.flag;
    //            Avo_LogDebug("Cap Contact Model People Flag(" + flag + ")", 2);	//debug
    //        }

    //        if (flag && flag != "Y") {
    //            continue;
    //        }
    //    }

    //    var peopleModel = contact.people;

    //    var businessName = peopleModel.businessName;
    //    Avo_LogDebug("Business Name(" + businessName + ")", 2); //debug

    //    var dbaName = peopleModel.tradeName;
    //    Avo_LogDebug("DBA Name(" + dbaName + ")", 2);  //debug

    //    var fullName = peopleModel.firstName + " " + peopleModel.lastName;
    //    Avo_LogDebug("Full name(" + fullName + ")", 2); //debug

    //    // Legal Business Name
    //    if (businessName) {
    //        appName = businessName;
    //        break;
    //    }

    //    // DBA Name
    //    if (dbaName) {
    //        appName = dbaName;
    //        break;
    //    }

    //    // First Name, Last Name
    //    appName = fullName;
    //    break;
    //}

    var highestLevel = null;

    var allContacts = aa.people.getCapContactByCapID(capId).getOutput();
    // Avo_LogDebug("Contacts(" + allContacts.length + ")", 2);    //debug
    for (var i in allContacts) {
        var contact = allContacts[i];

        var peopleModel = contact.people;

        var contactId = peopleModel.contactSeqNumber;
        Avo_LogDebug("Contact Id(" + contactId + ")", 2);   //debug

        // Check if Org
        var contactTypeFlag = peopleModel.contactTypeFlag;
        Avo_LogDebug("Ind/Org(" + contactTypeFlag + ")", 2);	//debug

        var isOrg = contactTypeFlag == "organization";
        if (highestLevel == "organization" && isOrg != true) {
            continue;
        }

        var businessName = peopleModel.businessName;
        Avo_LogDebug("Business Name(" + businessName + ")", 2); //debug

        var dbaName = peopleModel.tradeName;
        Avo_LogDebug("DBA Name(" + dbaName + ")", 2);  //debug

        var fullName = peopleModel.firstName + " " + peopleModel.lastName;
        Avo_LogDebug("Full name(" + fullName + ")", 2); //debug

        // Legal Business Name
        if (businessName) {
            appName = businessName;
        }
            // DBA Name
        else if (dbaName) {
            appName = dbaName;
        } else {
            // First Name, Last Name
            appName = fullName;
        }

        highestLevel = contactTypeFlag;

        if (allContacts.length == 1) {
            break;
        }

        // Check if Primary
        var capContactModel = contact.capContactModel;
        var flag = capContactModel.primaryFlag;
        Avo_LogDebug("Primary Flag(" + flag + ")", 2);	//debug

        if (flag != "Y") {
            continue;
        }

        break;
    }

    if (!appName) {
        Avo_LogDebug("Could not find a valid app name", 1);
        if (debugLevel >= 2) {
            aa.sendMail("tlicense@scottsdaleaz.gov", "PI_Test@avocette.com", "", "Avo_SetAppName", debug); //debug
        }
        return;
    }

    appName = String(appName).toUpperCase();

    var capModel = aa.cap.getCap(recordId).getOutput().getCapModel();
    capModel.setSpecialText(appName);

    var result = aa.cap.editCapByPK(capModel);
    if (!result.getSuccess()) {
        Avo_LogDebug("Failed to update app name", 1);
    } else {
        Avo_LogDebug("Updated app name on record " + altId + ' to "' + appName + '"', 1);
    }

    if (debugLevel >= 2) {
        aa.sendMail("tlicense@scottsdaleaz.gov", "PI_Test@avocette.com", "", "Avo_SetAppName", debug); //debug
    }
}

/*******************************************************
| Script/Function: Avo_ShouldLicFeeInvoice(stateSeriesPrefix) ()
| Created by: Nicolaj Bunting
| Created on: 15Mar18
| Usage: Based on the state series prefix determine if the fee should be invoiced
| Modified by: Nic Bunting (17Apr18)
*********************************************************/
function Avo_ShouldLicFeeInvoice(stateSeriesPrefix) {
    Avo_LogDebug("Avo_ShouldLicFeeInvoice(" + stateSeriesPrefix + ")", 1);

    var invoiceFee = "Y";

    switch (stateSeriesPrefix) {
        case "02":
        case "05":
        case "17":
        case "19":
        case "20":
        case "21":
            invoiceFee = "N";
            break;
    }

    return invoiceFee;
}

/*******************************************************
| Script/Function: Avo_BusRegServicePenFees(capId) - (BRS02)
| Created by: Nicolaj Bunting
| Created on: 10Jan18
| Usage: If ASI "Business Start Date" is before today then add and invoice fee "BRSPEN" from schedule "LIC_BRS_APP"
| If ASI "Business Start Date" is in a previous year Then add and invoice fee "BRSREN" from schedule "LIC_BRS_APP" with quantity of every January 1 that has passed since business start date to a max of 4
| and add and invoice fee "BRSRPEN" from schedule "LIC_BRS_APP" with quantity of every February 1 that has passed since business start date to a max of 4
| Modified by: Nic Bunting (21Feb18)
| Modified by: Nic Bunting (17Apr18)
*********************************************************/
function Avo_BusRegServicePenFees(capId) {
    var altId = aa.cap.getCap(capId).getOutput().getCapModel().altID;
    Avo_LogDebug("BRS02 Avo_BusRegServicePenFees(" + altId + ")", 1);

    var busStart = getAppSpecific("Business Start Date", capId);
    if (!busStart) {
        Avo_LogDebug("Business start date not set", 1);
        return;
    }

    var busStartDate = Avo_GetDateFromAccelaDateString(busStart);
    var today = Avo_GetToday();
    Avo_LogDebug("Today(" + aa.util.formatDate(today, "MM/dd/yyyy") + ")", 2); //debug

    if (busStartDate.getTime() < today.getTime()) {
        addFee("BRSPEN", "LIC_BRS_APP", "FINAL", 1, "Y", capId);
        Avo_LogDebug("Added fee BRSPEN from schedule LIC_BRS_APP", 1);
    }

    var startYear = busStartDate.getFullYear();
    Avo_LogDebug("Start Year(" + startYear + ")", 2);  //debug

    var year = today.getFullYear();
    Avo_LogDebug("Year(" + year + ")", 2); //debug

    if (startYear >= year) {
        return;
    }

    var jan1 = new Date(startYear + 1, 1 - 1, 1);
    if (today.getTime() <= jan1.getTime()) {
        return;
    }

    var renQuantity = year - startYear;
    Avo_LogDebug("Ren Qty(" + renQuantity + ")", 2);  //debug

    jan1.setFullYear(year);
    if (today.getTime() < jan1.getTime()) {
        renQuantity--;
    }

    if (renQuantity > 4) {
        renQuantity = 4;
    }

    addFee("BRSREN", "LIC_BRS_APP", "FINAL", renQuantity, "Y", capId);
    Avo_LogDebug("Added fee BRSREN from schedule LIC_BRS_APP with quantity of " + renQuantity.toString(), 1);

    var feb1 = new Date(startYear + 1, 2 - 1, 1);
    if (today.getTime() <= feb1.getTime()) {
        return;
    }

    var rPenQuantity = year - startYear;
    Avo_LogDebug("R Pen Qty(" + rPenQuantity + ")", 2);    //debug

    feb1.setFullYear(year);
    if (today.getTime() < feb1.getTime()) {
        rPenQuantity--;
    }

    if (rPenQuantity > 4) {
        rPenQuantity = 4;
    }

    addFee("BRSRPEN", "LIC_BRS_APP", "FINAL", rPenQuantity, "Y", capId);
    Avo_LogDebug("Added fee BRSRPEN from schedule LIC_BRS_APP with quantity of " + rPenQuantity.toString(), 1);
}

/*******************************************************
| Script/Function: Avo_AddLiqRenPenFees(capId) - (LIQ08)
| Created by: Nicolaj Bunting
| Created on: 11Jan18
| Usage: For all "Licenses/Business/Liquor/Renewal" records if balance > 0 
| then add and invoice fee "LIQRPEN" from schedule "LIC_LIQ_LIC" with quantity of 10% of renewal fee amount
| Modified by: ()
*********************************************************/
function Avo_AddLiqRenPenFees(capId) {
    var altId = aa.cap.getCap(capId).getOutput().getCapModel().altID;
    Avo_LogDebug("LIQ08 Avo_AddLiqRenPenFees(" + altId + ")", 1);

    var result = aa.cap.getCapDetail(capId);
    if (!result.getSuccess()) {
        Avo_LogDebug("Unable to load record details for " + altId);
        return;
    }

    var capDetails = result.getOutput();
    var balanceDue = capDetails.getBalance();
    Avo_LogDebug("Balance($" + balanceDue + ")", 2);  //debug

    if (balanceDue <= 0) {
        return;
    }

    var stateSeries = getAppSpecific("State Series #", capId);
    Avo_LogDebug("#(" + stateSeries + ")", 2); //debug

    var prefix = String(stateSeries).substring(0, 2);
    if (isNaN(prefix) == true) {
        prefix = "0" + prefix.substring(0, 1);
    }

    var renFeeCode = prefix + "-R";

    // Get permit fee assessment date and amount
    var amount = 0;
    var count = 0;

    var result = aa.fee.getFeeItems(capId);
    if (result.getSuccess() != true) {
        Avo_LogDebug("No fee items could be found on record " + altId, 1);
        return;
    }

    var allFeeItems = result.getOutput();
    for (i in allFeeItems) {
        var feeItem = allFeeItems[i];
        if (feeItem.feeCod != renFeeCode) {
            continue;
        }

        count++;

        amount += feeAmount(renFeeCode);
        Avo_LogDebug("Amount($" + amount + ")", 2);	//debug
    }

    Avo_LogDebug("Count(" + count + ")", 2);  //debug

    // Check that fee hasn't already been added
    var quantity = feeQty("LIQPEN");
    Avo_LogDebug("Qty(" + quantity + ")", 2);	//debug
    if (quantity > 0) {
        return;
    }

    quantity = 0.1 * amount;

    addFee("LIQRPEN", "LIC_LIQ_LIC", "FINAL", quantity, "Y", capId);
    Avo_LogDebug("Added fee LIQRPEN to record " + altId + " with quantity " + String(quantity), 1);
}

/*******************************************************
| Script/Function: Avo_AddBRSRenPenFees(capId) - (BRS03)
| Created by: Nicolaj Bunting
| Created on: 10Jan18
| Usage: For all "Licenses/Business/Bus Reg Service/Renewal" records if no payment has been recorded on the renewal record by Feb 1 of the calendar year 
| then assess and invoice fee "BRSRPEN" from schedule "LIC_BRS_REN"
| Modified by: ()
*********************************************************/
function Avo_AddBRSRenPenFees(capId) {
    var altId = aa.cap.getCap(capId).getOutput().getCapModel().altID;
    Avo_LogDebug("BRS03 Avo_AddBRSRenPenFees(" + altId + ")", 1);

    var result = aa.cap.getCapDetail(capId);
    if (!result.getSuccess()) {
        Avo_LogDebug("Unable to load record details for " + altId, 1);
        return;
    }

    var capDetails = result.getOutput();
    var balanceDue = capDetails.getBalance();
    Avo_LogDebug("Balance($" + balanceDue + ")", 2);  //debug

    if (balanceDue <= 0) {
        return;
    }

    var feeCode = "BRSRPEN";

    // Check that fee hasn't already been added
    var quantity = feeQty(feeCode);
    Avo_LogDebug("Qty(" + quantity + ")", 2);	//debug
    if (quantity > 0) {
        return;
    }

    addFee(feeCode, "LIC_BRS_REN", "FINAL", 1, "Y", capId);
    Avo_LogDebug("Added fee " + feeCode + " to record " + altId, 1);
}

/*******************************************************
| Script/Function: Avo_CreditFee(feeId, [recordId])
| Created by: Nicolaj Bunting
| Created on: 2May18
| Usage: Voids and credits invoiced fee with feeSeqNum feeId
| Modified by: ()
*********************************************************/
function Avo_CreditFee(feeId, recordId) {
    if (arguments.length < 2 || !arguments[1]) {
        recordId = capId;
    }
    var altId = aa.cap.getCap(recordId).getOutput().getCapModel().altID;
    if (!feeId) {
        Avo_LogDebug("Invalid fee sequence number", 1);
        return false;
    }
    Avo_LogDebug("Avo_CreditFee(" + String(feeId) + ", " + altId + ")", 1);

    var result = aa.fee.getFeeItemByPK(capId, feeId);
    if (result.getSuccess() != true) {
        Avo_LogDebug("Failed to get fee. " + result.errorMessage, 1);
        return false;
    }

    var feeItem = result.getOutput();

    var feeIdToCheck = feeItem.feeSeqNbr;
    Avo_LogDebug("FeeId(" + feeIdToCheck + ")", 2);	//debug

    if (feeIdToCheck != feeId) {
        return false;
    }

    var status = feeItem.feeitemStatus;
    Avo_LogDebug("Status(" + status + ")", 2);	//debug

    if (status != "INVOICED") {
        return false;
    }

    var feeItemModel = feeItem.f4FeeItem;
    if (!feeItemModel) {
        Avo_LogDebug("Failed to get feeItemModel", 1);
        return false;
    }

    feeItemModel.feeitemStatus = "CREDITED";

    var result = aa.finance.editFeeItem(feeItemModel);
    if (result.getSuccess() != true) {
        Avo_LogDebug("Failed to credit fee. " + result.errorMessage, 1);
        return false;
    }

    Avo_LogDebug("Credited fee " + feeId + " on record " + altId, 1);
    return true;
}

/*******************************************************
| Script/Function: Avo_AddFeeOnDateWithComment(fcode, fsched, fperiod, fqty, finvoice, fDate, fComment, [fCapId])
| Created by: Nicolaj Bunting
| Created on: 2May18
| Usage: Adds fee with code fcode from schedule fsched and period fperiod with quantity fqty and apply date of fDate to record fCapId with comment fComment and invoices if finvoice is "Y"
| Modified by: Nic Bunting (17May18)
*********************************************************/
function Avo_AddFeeOnDateWithComment(fcode, fsched, fperiod, fqty, finvoice, fDate, fComment, fCapId) {
    if (!fCapId || arguments.length < 6) {
        fCapId = capId;
    }
    var applyDate = fDate;
    if (typeof (fDate) == "string") {
        applyDate = new Date(fDate);
        Avo_LogDebug("Date String(" + fDate + ")", 2);  //debug
    }
    var altId = aa.cap.getCap(fCapId).getOutput().getCapModel().altID;
    Avo_LogDebug("Avo_AddFeeOnDateWithComment(" + fcode + ", " + fsched + ", " + fperiod + ", " + String(fqty) + ", " + finvoice + ", " + aa.util.formatDate(applyDate, "MM/dd/yyyy") + fComment + ", " + altId + ")", 1);

    var result = aa.finance.createFeeItem(fCapId, fsched, fcode, fperiod, fqty);
    if (result.getSuccess() == false) {
        Avo_LogDebug("Failed to add fee " + fcode + " from schedule " + fsched + ". " + result.errorMessage, 1);
        return;
    }

    var feeSeq = result.getOutput();
    Avo_LogDebug("Fee Id(" + feeSeq + ")", 2);    //debug

    Avo_LogDebug("Added fee " + fcode + " from schedule " + fsched + " with quantity " + String(fqty), 1);

    if (fComment) {
        var feeItem = aa.finance.getFeeItemByPK(fCapId, feeSeq).getOutput().getF4FeeItem();
        feeItem.setFeeNotes(fComment);
        feeItem.setApplyDate(applyDate);

        result = aa.finance.editFeeItem(feeItem);
        if (result.getSuccess() == false) {
            Avo_LogDebug("Failed to add comment to fee " + feeSeq + ". " + result.errorMessage);
        } else {
            Avo_LogDebug("Added comment to fee " + feeSeq, 2);    //debug
        }
    }

    if (String(finvoice).toUpperCase() == "Y") {
        var allFees = new Array();
        var allPeriods = new Array();

        allFees.push(feeSeq);
        allPeriods.push(fperiod);

        result = aa.finance.createInvoice(fCapId, allFees, allPeriods);
        if (result.getSuccess() == false) {
            Avo_LogDebug("Failed to invoice fee " + feeSeq + ". " + result.errorMessage);
        } else {
            Avo_LogDebug("Invoiced fee " + feeSeq, 2);    //debug
        }
    }

    return feeSeq;
}

/*******************************************************
| Script/Function: Avo_SortNumeric(a, b)
| Created by: Nicolaj Bunting
| Created on: 2May18
| Usage: Compares two values numerically descending, otherwise compares as strings 
| Modified by: ()
*********************************************************/
function Avo_CompareNumeric(a, b) {
    var intA = parseFloat(a);
    if (isNaN(intA) == true) {
        return String(a) < String(b);
    }

    var intB = parseFloat(b);
    if (isNan(intB) == true) {
        return String(a) < String(b);
    }

    return intA - intB;
}

/*******************************************************
| Script/Function: Avo_GenerateReport(reportName, module, parameters, [recordId], [username])
| Created by: Nicolaj Bunting
| Created on: 18Sep18
| Usage: Generates report with reportName
| Modified by: ()
*********************************************************/
function Avo_GenerateReport(reportName, module, parameters) {
    Avo_LogDebug("Avo_GenerateReport(" + reportName + ", " + module + ", " + parameters.toString() + ")", 1);

    // Get optional record id
    var recordId = null;
    if (arguments.length > 3 && arguments[3]) {
        recordId = arguments[3];
    }

    // Get optional user parameter
    var user = currentUserID;
    if (arguments.length > 4 && arguments[4]) {
        user = arguments[4];
    }

    var result = aa.reportManager.getReportInfoModelByName(reportName);
    if (result.getSuccess() != true) {
        Avo_LogDebug("Failed to get report info model. " + result.errorMessage, 1);
        return false;
    }

    var reportInfoModel = result.getOutput();
    reportInfoModel.setModule(module);
    reportInfoModel.setReportParameters(parameters);

    if (recordId) {
        var customId = aa.cap.getCap(recordId).getOutput().capModel.altID;
        reportInfoModel.setCapId(customId);
    }

    //var permit = aa.reportManager.hasPermission(reportName, user);
    //if (permit.getOutput().booleanValue() != true) {
    //    Avo_LogDebug("User " + user + " does not have permission to generate report " + reportName, 1);
    //    return false;
    //}

    result = aa.reportManager.getReportResult(reportInfoModel);
    if (result.getSuccess() != true) {
        Avo_LogDebug("System failed get report: " + result.getErrorType() + ":" + result.errorMessage, 1);
        return false;
    }

    var reportOutput = result.getOutput();
    result = aa.reportManager.storeReportToDisk(reportOutput);
    if (result.getSuccess() != true) {
        Avo_LogDebug("Failed to save report on disk. " + result.errorMessage, 1);
        return false;
    }

    var reportFile = result.getOutput();
    Avo_LogDebug("Filename(" + reportFile + ")", 2);	//debug

    return reportFile;
}

/*******************************************************
| Script/Function: Avo_GenerateReportAndAttachToRecord(itemCap, reportName, module, parameters)
| Usage: Generates the report reportName using parameters and attaches it to record itemCap if "save to EDMS" is enabled and "EDMS Object" is "Record" in report manager
| Modified by: ()
*********************************************************/
function Avo_GenerateReportAndAttachToRecord(itemCap, reportName, module, parameters) {
    var altId = aa.cap.getCap(itemCap).getOutput().getCapModel().altID;
    Avo_LogDebug("Avo_GenerateReportAndAttachToRecord(" + altId + ", " + reportName + ", " + module + ", " + parameters.toString() + ")", 1);
    //returns the report file which can be attached to an email.
    var user = currentUserID;   // Setting the User Name
    var result = aa.reportManager.getReportInfoModelByName(reportName);
    if (result.getSuccess() != true) {
        Avo_LogDebug('Failed to get report "' + reportName + '". ' + result.errorType + ":" + result.errorMessage, 1);
        return false;
    }

    var report = result.getOutput();

    report.setModule(module);
    // Note: Changed this to use the capId instead of the altId because the adhoc report will not attach to the record otherwise. 
    // Question: Will SSRS reports use the capId or the altId?
    report.setCapId(itemCap.getID1() + "-" + itemCap.getID2() + "-" + itemCap.getID3());
    report.getEDMSEntityIdModel().setAltId(itemCap.getCustomID());
    report.setReportParameters(parameters);

    //var permit = aa.reportManager.hasPermission(reportName, user);
    //if (!permit.getOutput().booleanValue()) {
    //    Avo_LogDebug("You have no permission.", 1);
    //    return false;
    //}

    var reportResult = aa.reportManager.getReportResult(report);
    if (!reportResult || reportResult.getSuccess() != true) {
        Avo_LogDebug("System failed get report: " + reportResult.getErrorType() + ":" + reportResult.errorMessage, 1);
        return false;
    }

    reportOutput = reportResult.getOutput();
    if (!reportOutput) {
        return false;
    }

    Avo_LogDebug("report ran: " + reportOutput.name, 1);
    return true;
}

/*******************************************************
| Script/Function: Avo_CopyAppSpecific(fromCapId, toCapId, [ignoreObj])
| Usage: Copy all ASI from fromCapId to toCapId, ignoring those fields listed in ignoreObj
| Modified by: ()
*********************************************************/
function Avo_CopyAppSpecific(fromCapId, toCapId, ignoreObj) {
    var fromAltId = aa.cap.getCap(fromCapId).getOutput().capModel.altID;
    var toAltId = aa.cap.getCap(toCapId).getOutput().capModel.altID;

    if (arguments.length < 3) {
        Avo_LogDebug("Avo_CopyAppSpecific(" + fromAltId + ", " + toAltId + ")", 2);	//debug

        ignoreObj = new Object();
    } else {
        Avo_LogDebug("Avo_CopyAppSpecific(" + fromAltId + ", " + toAltId + ", " + ignoreObj.toString() + ")", 2);	//debug
    }

    var result = aa.appSpecificInfo.getByCapID(fromCapId);
    if (result.getSuccess() != true) {
        Avo_LogDebug("Failed to get ASI. " + result.getErrorType() + ": " + result.errorMessage);
        return false;
    }

    var copied = 0;
    var allASI = result.getOutput();
    for (var i in allASI) {
        var asiModel = allASI[i];

        var fieldName = asiModel.checkboxDesc;
        if (fieldName in ignoreObj) {
            Avo_LogDebug("Ignoring " + fieldName, 1);
            continue;
        }

        var fieldValue = asiModel.checklistComment;

        var success = editAppSpecific(fieldName, fieldValue, toCapId);
        if (success == false) {
            continue;
        }

        copied++;
    }

    return copied;
}

function Serialize(obj) {
    if (!obj) {
        return;
    }

    var includeMethods = true;
    if (arguments.length > 1) {
        includeMethods = arguments[1];
    }

    level = 0;	//debug

    var json = "";
    //var json = "{";
    json = SerializeObject(json, obj, includeMethods);
    //json += "}";

    return json;

    function SerializeObject(json, obj, includeMethods) {
        if (!obj) {
            return json;
        }

        level++;

        json += "{\n";

        var first = true;
        for (k in obj) {
            if (!k) {
                continue;
            }
            var child = obj[k];
            if (!child) {
                continue;
            }

            if (typeof (child) == "function") {
                if (!includeMethods) {
                    continue;
                }
            }

            var prefix = ',\n';
            if (first) {
                prefix = '';
                first = false;
            }

            if (child.constructor == Array) {
                json += prefix + '"' + k + '":';

                json = SerializeObject(json, child, includeMethods);
                continue;
            }

            if (typeof (child) == "function") {
                if (!includeMethods) {
                    continue;
                }

                json += prefix + '"' + k + '":"' + k + '()"';
                continue;
            }

            json += prefix + '"' + k + '":"' + child + '"';
        }

        json += "\n}";
        level--;

        return json;
    }
}