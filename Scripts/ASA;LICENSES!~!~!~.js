// ASA: Licenses/*/*/*

showDebug = 3;

include("EMSE:SetContactRelationshipToContactType");
//include("ASIUA:Licenses/*/*/License");

// Get capId
//var capIdStrArr = [aa.env.getValue("PermitId1"), aa.env.getValue("PermitId2"), aa.env.getValue("PermitId3")];
//var cap = aa.cap.getCap(capIdStrArr[0], capIdStrArr[1], capIdStrArr[2]).getOutput();
//capId = cap.getCapID();

var allContacts = new Array();
if (publicUser == true) {
    allContacts = aa.env.getValue("ContactList").toArray();
} else {
    allContacts = aa.people.getCapContactByCapID(capId).getOutput();
}

for (i in allContacts) {
    var contact = allContacts[i];

    if (publicUser == false) {
        Avo_UpperContactFields(contact.capContactModel);
    } else {
        Avo_UpperContactFields(contact);
    }
}

var allAddresses = aa.address.getAddressByCapId(capId).getOutput();
for (i in allAddresses) {
    var addressModel = allAddresses[i];
    Avo_UpperAddressFields(addressModel);
}

//call script ** ID40 ** script/functions based on requirements
include("Avo_SetPrimaryContact");

//call script ** ID64 ** script/functions based on requirements
Avo_SetAppName(capId);

//call script ** ID52 ** script/functions based on requirements
Avo_SetDPSNum(capId);

//call script ** ID87 ** script/functions based on requirements
include("Avo_CopyAddressToContact");

//call script ** ID89 ** script/functions based on requirements
include("Avo_CopyOpenedDate");

/*******************************************************
| Script/Function: Avo_SetDPSNum() - (ID52)
| Created by: Nicolaj Bunting
| Created on: 18Apr18
| Usage: On submit For all documents If doc group is "LIC_DOCS" and type is "Records Check", "Records Check - RESUBMITTAL", 
| or "Records Check - DO NOT SUBMIT" then set field "DPS #" to YYYYMMDD
| Modified by: Nic Bunting (19Apr18)
*********************************************************/
function Avo_SetDPSNum() {
    logDebug("ID52 Avo_SetDPSNum()");

    var result = aa.document.getCapDocumentList(capId, currentUserID);
    if (result.getSuccess() != true) {
        Avo_LogDebug("Can't get documents on record", 1);
        return;
    }

    var allDocs = result.getOutput();
    if (allDocs.length == 0) {
        Avo_LogDebug("No documents on record", 1);
        return;
    }

    var today = new Date();

    var year = String(today.getFullYear());
    var month = today.getMonth() + 1;
    var day = today.getDate();

    var dpsNum = year + (month < 10 ? "0" + month : String(month)) + (day < 10 ? "0" + day : String(day));
    Avo_LogDebug("DPS #(" + dpsNum + ")", 2);   //debug

    for (i in allDocs) {
        var docModel = allDocs[i];

        var group = docModel.docGroup;
        Avo_LogDebug("Group(" + group + ")", 2);   //debug

        if (group != "LIC_DOCS") {
            continue;
        }

        var category = docModel.docCategory;
        Avo_LogDebug("Category(" + category + ")", 2);   //debug

        if (category != "Records Check" && category != "Records Check - RESUBMITTAL" && category != "Records Check - DO NOT SUBMIT") {
            continue;
        }

        var docId = docModel.documentNo;
        Avo_LogDebug("Id(" + docId + ")", 2);   //debug

        var doc = aa.document.getDocumentByPK(docId).getOutput();

        var forms = doc.template.templateForms.toArray();
        for (j in forms) {
            var template = forms[j];
            var groupName = template.groupName;
            var allSubGroups = template.subgroups.toArray();
            for (k in allSubGroups) {
                var subGroup = allSubGroups[k];
                var subGroupName = subGroup.displayName;

                var allFields = subGroup.fields.toArray();
                for (l in allFields) {
                    var field = allFields[l];
                    if (field.fieldName != "DPS #") {
                        continue;
                    }

                    // field.checklistComment = "tested";
                    field.defaultValue = dpsNum;
                    break;
                    break;
                    break;
                }
            }
        }

        var result = aa.document.updateDocument(doc);
        if (result.getSuccess() == true) {
            Avo_LogDebug("Updated DPS # in document", 1);
        } else {
            Avo_LogDebug("Failed to update DPS # in document. " + result.getErrorMessage(), 1);
        }
    }
}