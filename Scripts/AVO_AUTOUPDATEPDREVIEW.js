/*******************************************************
| Script/Function: Avo_AutoUpdatePDReview() - (ID14)
| Created by: Nicolaj Bunting
| Created on: 12Dec17
| Usage: If doc is assigned to user "PD" and workflow task "PD Review" is active Then close task "PD Review" with document 
| review status ["No Opposition", "Opposition"]
| Modified by: Nic Bunting (4Jul19)
*********************************************************/
(function () {
    logDebug("ID14 Avo_AutoUpdatePDReview()");

    var active = isTaskActive("PD Review");
    Avo_LogDebug("PD Review Active(" + active + ")", 2);	//debug

    if (active != true) {
        return;
    }

    var docRevModel = aa.env.getValue("DocumentReviewModel");

    var assigned = docRevModel.entityID1;
    Avo_LogDebug("Assigned(" + assigned + ")", 2);	//debug

    if (assigned != "PD") {
        return;
    }

    var reviewStatus = docRevModel.status;
    Avo_LogDebug("Status(" + reviewStatus + ")", 2);	//debug

    if (reviewStatus != "No Opposition" && reviewStatus != "Opposition") {
        return;
    }

    closeTask("PD Review", reviewStatus, "Updated by EMSE ID14", "");
    Avo_LogDebug('Closed task "PD Review" with status of "' + reviewStatus + '"', 1);
})();