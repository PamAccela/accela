//CTRCA: Licenses/Business/Liquor/Renewal

//call script ** LIQ07 ** script/functions based on requirements
Avo_AddLiqRenFees();

/*******************************************************
| Script/Function: Avo_AddLiqRenFees() - (LIQ07)
| Created by: Nicolaj Bunting
| Created on: 22Mar18
| Usage: For all "Licenses/Business/Liquor/Renewal" records
| get number prefix from ASI "State Series #" and add and invoice fee prefix + "-R" from schedule "LIC_LIQ_LIC"
| Modified by: ()
*********************************************************/
function Avo_AddLiqRenFees() {
    logDebug("LIQ07 Avo_AddLiqRenFees()");

    var invoiceFee = "Y";

    var stateSeries = getAppSpecific("State Series #", capId);
    Avo_LogDebug("#(" + stateSeries + ")", 2); //debug

    var prefix = String(stateSeries).substring(0, 2);
    if (isNaN(prefix) == true) {
        prefix = "0" + prefix.substring(0, 1);
    }

    var invoiceFee = Avo_ShouldLicFeeInvoice(prefix);
    Avo_LogDebug("Invoice fee(" + invoiceFee + ")", 2); //debug

    var feeCode = prefix + "-R";
    Avo_LogDebug("Code(" + feeCode + ")", 2);  //debug

    addFee(feeCode, "LIC_LIQ_LIC", "FINAL", 1, invoiceFee, capId);
}