/*******************************************************
| Script Title: Batch_AddRenLateFees (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 10Jan18
| Usage: For all "Licenses/Business/Escort Bureau/Renewal", "Licenses/Business/Escort Permit/Renewal", "Licenses/Business/Valet Parking/Renewal", 
| "Licenses/Business/Massage Facility/Renewal", "Licenses/Business/Neighbourhood Street Vendor/Renewal" records if balance > 0 
| and date is 59 days before expiration date (29 days for Valet Parking) then assess fee "EBHLAP" from schedule "LIC_EBH_REN", "ESPLAP" from schedule "LIC_ESP_REN",
| "PRKLAP" from schedule "LIC_PRK_REN", "MFCDL1" from schedule "LIC_MFC_REN", or  "NSVLAP" from schedule "LIC_NSV_REN" respectively
| Modified by: ()
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
//aa.env.setValue("param1","Yes");
//aa.env.setValue("param1","No");
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var param1 = aa.env.getValue("param1");
//var param2 = aa.env.getValue("param2");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
//eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    aa.print("Batch Job " + batchJobName + " Job ID is " + batchJobID);
}
else {
    aa.print("Batch job ID not found " + batchJobResult.getErrorMessage());
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    aa.print("Start of Job");

    if (!timeExpired) {
        try {
            AddRenLateFees();
        }
        catch (e) {
            aa.print("Error in process " + e.message);
        }
    }
    else {
        aa.print("End of Job: Elapsed Time : " + elapsed() + " Seconds");
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

function AddRenLateFees() {
    printLine("EBH01, ESP01, MFC01, NSV01, PRK01 AddRenLateFees()");

    var group = "Licenses";
    var type = "Business";
    var subType = "*";
    var category = "Renewal";
    var appsArray = aa.cap.getByAppType(group, type, subType = null, category).getOutput();
    if (appsArray.length == 0) {
        printLine('No records of type "' + group + "/" + type + "/" + subType + "/" + category + '" found');
        return;
    }

    var today = Avo_GetToday();

    for (a in appsArray) {
        capId = appsArray[a].getCapID();
        var altId = capId.getCustomID();

        var daysPrior = 59;
        var feeCode = "EBHLAP";
        var feeSchedule = "LIC_EBH_REN";

        var escortBureau = appMatch("Licenses/Business/Escort Bureau/Renewal", capId);

        var escortPermit = appMatch("Licenses/Business/Escort Permit/Renewal", capId);
        if (escortPermit == true) {
            feeCode = "ESPLAP";
            feeSchedule = "LIC_ESP_REN";
        }

        var valetParking = appMatch("Licenses/Business/Valet Parking/Renewal", capId);
        if (valetParking == true) {
            feeCode = "PRKLAP";
            feeSchedule = "LIC_PRK_REN";
            daysPrior = 29;
        }

        var massageFacility = appMatch("Licenses/Business/Massage Facility/Renewal", capId);
        if (massageFacility == true) {
            feeCode = "MFCDL1";
            feeSchedule = "LIC_MFC_REN";
        }

        var streetVendor = appMatch("Licenses/Business/Neighborhood Street Vendor/Renewal", capId);
        if (streetVendor == true) {
            feeCode = "NSVLAP";
            feeSchedule = "LIC_NSV_REN";
        }

        if ((escortBureau || escortPermit || valetParking || massageFacility || streetVendor) == false) {
            continue;
        }

        var result = aa.cap.getCapDetail(capId);
        if (!result.getSuccess()) {
            printLine("Unable to load record details for " + altId);
            continue;
        }

        var capDetails = result.getOutput();
        var balanceDue = capDetails.getBalance();
        printLine("Balance($" + balanceDue + ")");  //debug

        if (balanceDue <= 0) {
            continue;
        }

        // Get parent license
        var parentCapId = null;
        var allParentRecords = aa.cap.getProjectByChildCapID(capId, "", "").getOutput();
        for (i in allParentRecords) {
            var capIdToCheck = allParentRecords[i].projectID;

            var result = aa.cap.getCap(capIdToCheck);
            if (result.getSuccess() != true) {
                continue;
            }

            parentCapId = capIdToCheck;
            break;
        }

        if (!parentCapId) {
            printLine("Unable to retrieve parent license for renewal " + altId);
            continue;
        }

        var parentAltId = aa.cap.getCap(parentCapId).getOutput().getCapModel().altID;

        // Check expiration date
        var b1ExpResults = aa.expiration.getLicensesByCapID(parentCapId);
        if (b1ExpResults.getSuccess() == false) {
            printLine("Unable to load expiration data for renewal " + parentAltId);
            continue;
        }

        var b1Exp = b1ExpResults.getOutput();
        var b1ExpModel = b1Exp.getB1Expiration();
        if (!b1ExpModel) {
            printLine("Unable to load expiration data for renewal " + parentAltId);
            continue;
        }

        var expiration = b1ExpModel.getExpDate();
        var expDate = new Date(expiration.time);
        printLine("Exp Date(" + aa.util.formatDate(expDate, "MM/dd/yyyy") + ")");  //debug

        var priorDate = new Date(expDate.getFullYear(), expDate.getMonth(), expDate.getDate() - daysPrior);
        printLine("Prior Date(" + aa.util.formatDate(priorDate, "MM/dd/yyyy") + ")");  //debug

        if (priorDate.getTime() != today.getTime()) {
            continue;
        }

        // Check that fee hasn't already been added
        var quantity = feeQty(feeCode);
        printLine("Qty(" + quantity + ")");	//debug
        if (quantity > 0) {
            continue;
        }

        addFee(feeCode, feeSchedule, "FINAL", 1, "Y", capId);
        printLine("Added fee " + feeCode + " to record " + altId);
    }
}

function printLine(pStr) {
    aa.print(pStr + br);
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}