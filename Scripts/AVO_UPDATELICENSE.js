/*******************************************************
| Script/Function: Avo_UpdateLicense() - (ID5)
| Created by: Nicolaj Bunting
| Created on: 12Dec17
| Usage: If task "Renewal" has status "Closed: Paid" or task "Issue License" has status "Closed: Issued" then update parent license's issued date, expiration date, 
| set expiration status to "Active", and copy ASI, ASIT, contacts, documents, address, and parcel, and update app name
| Modified by: Nic Bunting (7Mar18)
| Modified by: Nic Bunting (18Apr18)
| Modified by: Nic Bunting (8Oct19)
| Modified by: Nic Bunting (15Oct19)
*********************************************************/
(function () {
    logDebug("ID5 Avo_UpdateLicense()");

    // Get parent license
    aa.runScript("WORKFLOWTASKUPDATEAFTER4RENEW");	// Use event specific Renew script

    var licCapId = null;
    var licAltId = null;
    var allParentRecords = aa.cap.getProjectByChildCapID(capId, "Renewal", "").getOutput();
    for (var i in allParentRecords) {
        var capIdToCheck = allParentRecords[i].projectID;

        var result = aa.cap.getCap(capIdToCheck);
        if (result.getSuccess() != true) {
            continue;
        }

        var capToCheck = result.getOutput();

        var altIdToCheck = capToCheck.capModel.altID;
        Avo_LogDebug("Parent(" + altIdToCheck + ")", 2);    //debug

        var capTypeModel = capToCheck.capType;
        var recordType = String(capTypeModel);
        Avo_LogDebug("Record Type(" + recordType + ")", 2); //debug

        var capGroup = capTypeModel.getGroup();
        if (capGroup != "Licenses") {
            continue;
        }

        var capCategory = capTypeModel.getCategory();
        if (capCategory != "License" && capCategory != "Permit") {
            continue;
        }

        licCapId = capIdToCheck;
        licAltId = altIdToCheck;
        break;
    }

    if (!licCapId) {
        Avo_LogDebug("Unable to retrieve parent license", 1);
        return;
    }

    Avo_LogDebug("Parent License(" + licAltId + ")", 2);    //debug

    // Get Subtype
    var cap = aa.cap.getCap(capId).getOutput();
    var capTypeModel = cap.getCapType();
    var capSubType = String(capTypeModel.getSubType());
    Avo_LogDebug("Subtype(" + capSubType + ")", 2);    //debug

    // Update expiration date and status
    var b1ExpResults = aa.expiration.getLicensesByCapID(licCapId);
    if (b1ExpResults.getSuccess()) {
        var b1Exp = b1ExpResults.getOutput();
        var b1ExpModel = b1Exp.getB1Expiration();
        if (b1ExpModel) {
            var licExp = b1ExpModel.getExpDate();
            var licExpDate = new Date(licExp.time);
            Avo_LogDebug("Exp Date(" + aa.util.formatDate(licExpDate, "MM/dd/yyyy") + ")", 2);

            var startDate = licExpDate;

            // Set start date
            switch (capSubType) {
                case "Charitable Solicitations":
                    var activityType = getAppSpecific("Activity Type", capId);
                    Avo_LogDebug("Activity Type(" + activityType + ")", 2); //debug
                    if (activityType != "Permanent Location") {
                        Avo_LogDebug("Renewal not permitted for Charitable Solicitation", 1);
                        return;
                    }
                case "Adult Service Provider":
                case "Auction House":
                case "Auctioneer":
                case "Escort Bureau":
                case "Escort":
                case "Magic Arts":
                case "Massage Facility":
                case "Neighborhood Street Vendor":
                case "Secondhand":
                case "Sexually Oriented Business Mgr":
                case "Sexually Oriented Business":
                case "Teen Dance Center":
                case "Teletrack Operator":
                case "Teletrack Establishment":
                case "Valet Parking":
                    Avo_LogDebug("Starting day after license expiration", 1);
                    startDate.setDate(startDate.getDate() + 1); // Next day
                    break;

                case "After Hours Establishment":
                case "Alarm User":
                case "Bus Reg Merchant":
                case "Bus Reg Service":
                case "Recycling":
                case "Liquor":
                case "Solid Waste":
                    Avo_LogDebug("Starting Jan 1st of next year", 1);
                    startDate = new Date(startDate.getFullYear() + 1, 0, 1);    // Jan 1st of next year
                    break;

                case "Close Out Sales":
                    var saleDate = getAppSpecific("Date Sale to Commence", capId);
                    startDate = Avo_GetDateFromAccelaDateString(saleDate);
                    break;
            }

            if (!startDate) {
                Avo_LogDebug("Business start date is invalid or could not be found", 1);
                return;
            }

            Avo_LogDebug("Start Date(" + aa.util.formatDate(startDate, "MM/dd/yyyy") + ")", 2);

            // Set the record opened date
            var licCapModel = aa.cap.getCap(licCapId).getOutput().capModel;
            licCapModel.setFileDate(startDate);
            var result = aa.cap.editCapByPK(licCapModel);
            if (result.getSuccess() == false) {
                Avo_LogDebug("Failed to set Date Business Setup in System", 1);
            }

            var updatedExpDate = startDate;

            // Set end date
            switch (capSubType) {
                case "Adult Service Provider":
                case "Sexually Oriented Business Mgr":
                case "Teletrack Operator":
                case "Teletrack Establishment":
                    Avo_LogDebug("Added 36 months", 1);
                    updatedExpDate.setDate(updatedExpDate.getDate() + 1094);
                    break;

                case "After Hours Establishment":
                case "Alarm User":
                case "Bus Reg Merchant":
                case "Bus Reg Service":
                case "Liquor":
                case "Recycling":
                case "Solid Waste":
                    Avo_LogDebug("Dec 31st of next year", 1);
                    updatedExpDate = new Date(updatedExpDate.getFullYear(), 12 - 1, 31);
                    break;

                case "Charitable Solicitations":
                    var activityType = getAppSpecific("Activity Type", capId);
                    if (activityType != "Permanent Location") {
                        break;
                    }
                case "Auction House":
                case "Auctioneer":
                case "Escort Bureau":
                case "Escort":
                case "Magic Arts":
                case "Massage Facility":
                case "Neighborhood Street Vendor":
                case "Secondhand":
                case "Sexually Oriented Business":
                case "Teen Dance Center":
                case "Valet Parking":
                    Avo_LogDebug("Added 12 months", 1);
                    updatedExpDate.setDate(updatedExpDate.getDate() + 364);
                    break;

                case "Close Out Sales":
                    var extEndDate = getAppSpecific("Extension End Date", capId);
                    updatedExpDate = Avo_GetDateFromAccelaDateString(extEndDate);
                    break;
            }

            //set the status
            b1ExpModel.setExpStatus("Active");

            //set the date
            b1ExpModel.setExpDate(updatedExpDate);

            //need to use editB1Expiration to make changes
            var b1EditResult = aa.expiration.editB1Expiration(b1ExpModel);
            if (b1EditResult.getSuccess() == true) {
                Avo_LogDebug("Updated expiration on record " + licAltId + " to " + aa.util.formatDate(updatedExpDate, "MM/dd/yyyy"), 1);
            }
        }
    }

    // Copy ASI
    //copyASIFields(capId, licCapId);
    Avo_CopyAppSpecific(capId, licCapId);

    // Copy ASIT
    var allTableNames = ["SOLICITATION DATES", "REPRESENTED BUSINESS", "BOND INFORMATION", "LIC_AUC_EMP", "INSPECTIONS", "PARKING LOTS", "STREET AND SIDEWALK CLOSURES", "VENDOR INFORMATION",
        "HOURS OF OPERATION", "KEY DATES", "OWNERSHIP INFORMATION", "VEHICLES", "PROFESSIONAL LICENSES", "CHARACTER REFERENCES", "EMPLOYMENT HISTORY", "TELETRACK OPERATOR INFORMATION"];
    for (var i in allTableNames) {
        var tableName = allTableNames[i];
        removeASITable(tableName, licCapId);
    }

    copyASITables(capId, licCapId);

    // Copy contacts
    var allContacts = getContactArray(licCapId);
    for (var i in allContacts) {
        // Remove contact
        var contactId = allContacts[i].contactSeqNumber;
        result = aa.people.removeCapContact(licCapId, contactId);
        if (result.getSuccess()) {
            Avo_LogDebug("Contact " + contactId + " removed from license " + licAltId, 1);
        } else {
            Avo_LogDebug("Failed to remove contact " + contactId + " from license " + licAltId, 1);
        }
    }

    copyContacts(capId, licCapId);

    // Copy documents
    result = aa.document.getCapDocumentList(licCapId, currentUserID);
    if (result.getSuccess() == true) {
        var allDocs = result.getOutput();
        for (var i in allDocs) {
            var docModel = allDocs[i];
            var docId = docModel.documentNo;

            docModel.recStatus = "I";
            docModel.entityID = null;

            result = aa.document.updateDocument(docModel);
            if (result.getSuccess()) {
                Avo_LogDebug("Document " + docId + " removed from license " + licAltId, 1);
            } else {
                Avo_LogDebug("Failed to remove document " + docId + " from license " + licAltId + ". " + result.getErrorMessage(), 1);
            }
        }
    }

    Avo_CopyDocuments(capId, licCapId);

    // Copy address
    result = aa.address.getAddressByCapId(licCapId);
    if (result.getSuccess()) {
        var allAddresses = result.getOutput();
        for (var i in allAddresses) {
            var address = allAddresses[i];
            var addressId = address.addressId;

            result = aa.address.removeAddress(licCapId, addressId);
            if (result.getSuccess()) {
                Avo_LogDebug("Address " + addressId + " removed from license " + licAltId, 1);
            } else {
                Avo_LogDebug("Failed to remove address " + addressId + " from license " + licAltId + ". " + result.getErrorMessage(), 1);
            }
        }
    }

    copyAddresses(capId, licCapId);

    // Copy parcel
    // Get list of parcels to be copied
    var renParcelNums = new Array();
    result = aa.parcel.getParcelDailyByCapID(capId, null);
    if (result.getSuccess()) {
        var allParcels = result.getOutput();
        for (var i in allParcels) {
            var parcelModel = allParcels[i];
            var parcelNum = parcelModel.parcelNumber;
            renParcelNums.push(parcelNum);
        }
    }

    result = aa.parcel.getParcelDailyByCapID(licCapId, null);
    if (result.getSuccess()) {
        var allParcels = result.getOutput();
        for (var i in allParcels) {
            var parcelModel = allParcels[i];
            var parcelNum = parcelModel.parcelNumber;

            // If parcel is to be copied, then reactivate it
            var parcelMatch = false;
            for (j in renParcelNums) {
                var parcelNumToCheck = renParcelNums[j];
                if (parcelNumToCheck != parcelNum) {
                    continue;
                }

                parcelMatch = true;
                parcelModel.setParcelStatus("A");
                parcelModel.setAuditStatus("A");

                var capIdStrArr = String(licCapId).split("-");
                var capIdModel = aa.cap.getCapIDModel(capIdStrArr[0], capIdStrArr[1], capIdStrArr[2]).getOutput();

                var capParcelModel = aa.parcel.getCapParcelModel().getOutput();
                capParcelModel.setParcelModel(parcelModel.parcelModel);
                capParcelModel.setCapIDModel(capIdModel);
                capParcelModel.setL1ParcelNo(parcelNum);
                capParcelModel.setParcelNo(parcelNum);

                result = aa.parcel.updateDailyParcelWithAPOAttribute(capParcelModel);
                if (result.getSuccess()) {
                    Avo_LogDebug("Parcel " + parcelNum + " readded to license " + licAltId, 1);
                } else {
                    Avo_LogDebug("Failed to readd parcel " + parcelNum + " to license " + licAltId + ". " + result.getErrorMessage(), 1);
                }
            }

            if (parcelMatch == true) {
                continue;
            }

            // Make Parcel inactive
            parcelModel.setPrimaryParcelFlag("N");
            parcelModel.setParcelStatus("I");
            parcelModel.setAuditStatus("I");

            var capIdStrArr = String(licCapId).split("-");
            var capIdModel = aa.cap.getCapIDModel(capIdStrArr[0], capIdStrArr[1], capIdStrArr[2]).getOutput();

            var capParcelModel = aa.parcel.getCapParcelModel().getOutput();
            capParcelModel.setParcelModel(parcelModel.parcelModel);
            capParcelModel.setCapIDModel(capIdModel);
            capParcelModel.setL1ParcelNo(parcelNum);
            capParcelModel.setParcelNo(parcelNum);

            result = aa.parcel.updateDailyParcelWithAPOAttribute(capParcelModel);
            if (result.getSuccess()) {
                Avo_LogDebug("Parcel " + parcelNum + " removed from license " + licAltId, 1);
            } else {
                Avo_LogDebug("Failed to remove parcel " + parcelNum + " from license " + licAltId + ". " + result.getErrorMessage(), 1);
            }
        }
    }

    copyParcels(capId, licCapId);

    //call script ** ID64 ** script/functions based on requirements
    Avo_SetAppName(licCapId);
})();