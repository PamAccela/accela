//ASA: Licenses/Business/Liquor/Application

//call script ** LIQ01 ** script/functions based on requirements
Avo_AddPermitAppFee();

//call script ** LIQ02 ** script/functions based on requirements
Avo_AddTransferFee();

//call script ** LIQ05 ** script/functions based on requirements
Avo_AddInterimFee();

/*******************************************************
| Script/Function: Avo_AddInterimFee() - (LIQ05)
| Created by: Nicolaj Bunting
| Created on: 10Jan18
| Usage: If ASI "Interim Permit" is checked then get number prefix from ASI "State Series #" 
| and add and invoice fee prefix from schedule "LIC_LIQ_LIC" with quantity of 0.25
| Modified by: Nic Bunting (15Mar18)
*********************************************************/
function Avo_AddInterimFee() {
    logDebug("LIQ05 Avo_AddInterimFee()");

    var interimPermit = getAppSpecific("Interim Permit", capId);
    if (interimPermit != "CHECKED") {
        return;
    }

    var stateSeries = getAppSpecific("State Series #", capId);
    logDebug("#(" + stateSeries + ")"); //debug

    var prefix = String(stateSeries).substring(0, 2);
    if (isNaN(prefix) == true) {
        prefix = "0" + prefix.substring(0, 1);
    }

    var invoiceFee = Avo_ShouldLicFeeInvoice(prefix);
    Avo_LogDebug("Invoice fee(" + invoiceFee + ")", 2); //debug

    var feeCode = prefix;
    logDebug("Code(" + feeCode + ")");  //debug

    addFee(feeCode, "LIC_LIQ_LIC", "FINAL", 0.25, invoiceFee, capId);
    if (invoiceFee == "Y") {
        Avo_LogDebug('Added and invoiced fee "' + feeCode + '" from schedule "LIC_LIQ_LIC"');
    } else {
        Avo_LogDebug('Added fee "' + feeCode + '" from schedule "LIC_LIQ_LIC"');
    }
}

/*******************************************************
| Script/Function: Avo_AddTransferFee() - (LIQ02)
| Created by: Nicolaj Bunting
| Created on: 10Jan18
| Usage: If ASI "State Series #" prefix isn't "02, "05", "17", "19", "20", "21"
| And ASI "Person to Person Transfer" and "Location to Location Transfer" is checked 
| then add and invoice fee "LIQTR" from schedule "LIC_LIQ_LIC"
| Modified by: Nic Bunting (17Apr18)
*********************************************************/
function Avo_AddTransferFee() {
    logDebug("LIQ02 Avo_AddTransferFee()");

    var stateSeries = getAppSpecific("State Series #", capId);
    Avo_LogDebug("#(" + stateSeries + ")", 2); //debug

    var prefix = String(stateSeries).substring(0, 2);
    if (isNaN(prefix) == true) {
        prefix = "0" + prefix.substring(0, 1);
    }

    Avo_LogDebug("Prefix(" + prefix + ")", 2);  //debug
    if (prefix == "02" || prefix == "05" || prefix == "17" || prefix == "19" || prefix == "20" || prefix == "21") {
        return;
    }

    var p2pTransfer = getAppSpecific("Person to Person Transfer", capId);
    var l2lTransfer = getAppSpecific("Location to Location Transfer", capId);
    if (p2pTransfer != "CHECKED") {
        if (l2lTransfer != "CHECKED") {
            return;
        }
    }

    addFee("LIQTR", "LIC_LIQ_LIC", "FINAL", 1, "Y", capId);
    Avo_LogDebug('Added and invoiced fee "LIQTR" from schedule "LIC_LIQ_LIC"');
}

/*******************************************************
| Script/Function: Avo_AddPermitAppFee() - (LIQ01)
| Created by: Nicolaj Bunting
| Created on: 10Jan18
| Usage: If ASI "Person to Person Transfer" and "Location to Location Transfer" is unchecked 
| then get number prefix from ASI "State Series #" and add and invoice fee prefix + "-A" from schedule "LIC_LIQ_LIC"
| Modified by: Nic Bunting (15Mar18)
*********************************************************/
function Avo_AddPermitAppFee() {
    logDebug("LIQ01 Avo_AddPermitAppFee()");

    var p2pTransfer = getAppSpecific("Person to Person Transfer", capId);
    if (p2pTransfer == "CHECKED") {
        return;
    }

    var l2lTransfer = getAppSpecific("Location to Location Transfer", capId);
    if (l2lTransfer == "CHECKED") {
        return;
    }

    var invoiceFee = "Y";

    var stateSeries = getAppSpecific("State Series #", capId);
    Avo_LogDebug("#(" + stateSeries + ")", 2); //debug

    var prefix = String(stateSeries).substring(0, 2);
    if (isNaN(prefix) == true) {
        prefix = "0" + prefix.substring(0, 1);
    }

    var invoiceFee = Avo_ShouldLicFeeInvoice(prefix);
    Avo_LogDebug("Invoice fee(" + invoiceFee + ")", 2); //debug

    var feeCode = prefix + "-A";
    Avo_LogDebug("Code(" + feeCode + ")", 2);  //debug

    addFee(feeCode, "LIC_LIQ_LIC", "FINAL", 1, invoiceFee, capId);
    if (invoiceFee == "Y") {
        Avo_LogDebug('Added and invoiced fee "' + feeCode + '" from schedule "LIC_LIQ_LIC"');
    } else {
        Avo_LogDebug('Added fee "' + feeCode + '" from schedule "LIC_LIQ_LIC"');
    }
}