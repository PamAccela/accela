/*******************************************************
| Script Title: Batch_ImportPaymentData (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 16May18
| Usage: Get payment data from SFTP adapter Then for each row make a payment, generate receipt, apply payment to all unpaid, invoiced fees, If record is app 
| And workflow task "Permit Application", "Issue License", "Issue Permit" are active Then If "Alarm User" Then close task with status of "Closed: Permit Issued" 
| Else "Closed: Issued", call function Avo_CreateLicense(), If record is renewal And balance due on renewal and parent license is $0 And "Renewal" task is active
| Then close task "Renewal" with status of "Closed: Paid" Then call function Avo_UpdateLicense(), upload file archiveFolder
| Modified by: Nic Bunting (8Aug19)
| Modified by: Nic Bunting (11Sep19)
| Modified by: Nic Bunting (17Sep19)
| Modified by: Nic Bunting (18Sep19)
| Modified by: Nic Bunting (25Sep19)
| Modified by: Nic Bunting (2Oct19)
| Modified by: Nic Bunting (8Oct19)
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
////aa.env.setValue("sftpUrl", "172.19.61.35");
////aa.env.setValue("sftpPort", 21);
//aa.env.setValue("sftpFolder", "test");    //testing
//aa.env.setValue("archiveFolder", "archive");    //testing
////aa.env.setValue("sftpUsername", "AccelaFTP");
////aa.env.setValue("sftpPassword", "txfr1234!");
//aa.env.setValue("filename", "impLockBox.csv");    //testing
////aa.env.setValue("filename", "impLockBox20190822.csv");    //testing
////aa.env.setValue("adapterUrl", "http://172.19.61.35/AccelaFTPService");
////aa.env.setValue("adapterUsername", "AccelaFTPUser");
////aa.env.setValue("adapterPassword", "x1transfer1234$");
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var url = aa.env.getValue("sftpUrl");
//var port = aa.env.getValue("sftpPort");
var sftpFolder = aa.env.getValue("sftpFolder");
var archiveFolder = aa.env.getValue("archiveFolder");
//var username = aa.env.getValue("sftpUsername");
//var password = aa.env.getValue("sftpPassword");
var filename = aa.env.getValue("filename");
//var adapterUrl = aa.env.getValue("adapterUrl");
//var adapterUsername = aa.env.getValue("adapterUsername");
//var adapterPassword = aa.env.getValue("adapterPassword");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

// Print debug using aa.print instead of aa.debug
useLogDebug = false;

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    Avo_LogDebug("Batch Job " + batchJobName + " Job ID is " + batchJobID, 1);
}
else {
    Avo_LogDebug("Batch job ID not found " + batchJobResult.getErrorMessage(), 1);
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    Avo_LogDebug("Start of Job", 1);

    if (!timeExpired) {
        try {
            importPaymentData(sftpFolder, archiveFolder, filename);
        }
        catch (e) {
            Avo_LogDebug("Error in process " + e.message, 1);
        }
    }
    else {
        Avo_LogDebug("End of Job: Elapsed Time : " + elapsed() + " Seconds", 1);
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

function importPaymentData(sftpFolder, archiveFolder, filename) {
    Avo_LogDebug("importPaymentData(" + sftpFolder + ", " + archiveFolder + ", " + filename + ")", 1);

    if (!filename || String(filename).length == 0) {
        var today = Avo_GetToday();
        filename = "impLockBox" + today.getFullYear() + zeroPad(today.getMonth() + 1, 2) + zeroPad(today.getDate(), 2) + "_00000.csv";
        Avo_LogDebug("Filename(" + filename + ")", 2);  //debug
    }

    var allPayments = getDataFromFTP(sftpFolder, archiveFolder, filename);
    if (!allPayments) {
        return;
    }

    Avo_LogDebug("Payments(" + allPayments.length + ")", 2);    //debug
    for (var i in allPayments) {
        //testing
        //if (i < 24) {
        //    continue;
        //}
        //if (i > 1) {
        //    break;
        //}
        // end testing

        var payment = allPayments[i];
        Avo_LogDebug(br + "Payment(" + payment + ")", 2);	//debug
        if (!payment) {
            continue;
        }

        var paymentArr = payment.split(",");

        var licNum = paymentArr[0];
        var altId = licNum;
        Avo_LogDebug("Record Id(" + altId + ")", 2);	//debug

        var result = aa.cap.getCapID(altId);
        if (result.getSuccess() != true) {
            Avo_LogDebug("Failed to find record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);

            // Check for possible app
            altId = getApp(licNum);
            Avo_LogDebug("Record Id(" + altId + ")", 2);	//debug
            if (altId.length == 0) {
                Avo_LogDebug("Failed to find a matching app record for " + licNum + ". " + result.errorType + ": " + result.errorMessage, 1);
                continue;
            }

            var result = aa.cap.getCapID(altId);
            if (result.getSuccess() != true) {
                Avo_LogDebug("Failed to find app record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);
                continue;
            }
        }

        var recordId = result.getOutput();
        var primaryRecordId = recordId;
        var primaryAltId = altId;

        var totalPayment = Number(paymentArr[1]);
        Avo_LogDebug("Payment($" + totalPayment + ")", 2);	//debug

        if (isNaN(totalPayment) == true) {
            Avo_LogDebug("Invalid payment for record " + altId, 1);
            continue;
        }

        if (totalPayment == 0) {
            continue;
        }

        var paymentInvoiceNum = paymentArr[4];
        if (paymentInvoiceNum.length > 0) {
            paymentInvoiceNum = parseInt(paymentInvoiceNum, 10);
        }
        Avo_LogDebug("Payment Invoice #(" + paymentInvoiceNum + ")", 2);	//debug

        var dateStr = paymentArr[2];
        var dateTime = aa.date.parseDate(dateStr);
        var method = paymentArr[3];

        var systemSource = paymentArr[5];

        // Get invoice #
        var invoiceId = null;
        var invoiceNum = null;

        if (String(paymentArr[4]).length == 0) {
            Avo_LogDebug('No invoice provided for record ' + altId, 1);
        }
        else if (isNaN(paymentInvoiceNum) == true) {
            Avo_LogDebug('Invalid invoice number ' + paymentArr[4] + ' for record ' + altId, 1);
        } else {
            result = aa.finance.getInvoiceByCapID(recordId, aa.util.newQueryFormat());
            if (result.getSuccess() != true) {
                Avo_LogDebug("Failed to get all invoices on record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);
            } else {
                var allInvoices = result.getOutput();
                for (var i in allInvoices) {
                    var invoiceModel = allInvoices[i].invoiceModel;

                    var invoiceNumToCheck = parseInt(invoiceModel.customizedNbr, 10);
                    Avo_LogDebug("Record Invoice Num(" + invoiceNumToCheck + ")", 2);	//debug

                    if (invoiceNumToCheck != paymentInvoiceNum) {
                        continue;
                    }

                    invoiceId = invoiceModel.invNbr;
                    Avo_LogDebug("Record Inv Id(" + invoiceId + ")", 2);  //debug

                    invoiceNum = invoiceNumToCheck;
                    Avo_LogDebug("Found matching invoice " + invoiceNum + " on record " + altId, 1);
                    break;
                }

                //if (!invoiceId) {
                //    Avo_LogDebug("Failed to find matching invoice number " + paymentInvoiceNum + " on record " + altId, 1);
                //}
                if (!invoiceNum) {
                    Avo_LogDebug("Failed to find matching invoice number " + paymentInvoiceNum + " on record " + altId, 1);
                }
            }
        }

        //if (String(paymentArr[4]).length == 0 || isNaN(paymentInvoiceNum) == true) {
        //    Avo_LogDebug('Invalid invoice number ' + paymentArr[4] + ' for record ' + altId, 1);
        //}

        //result = aa.finance.getInvoiceByCapID(capId, aa.util.newQueryFormat());
        //if (result.getSuccess() != true) {
        //    Avo_LogDebug("Failed to get all invoices on record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);
        //} else {
        //    var oldestDate = null;

        //    var allInvoices = result.getOutput();
        //    for (var i in allInvoices) {
        //        var invoiceModel = allInvoices[i].invoiceModel;

        //        if (String(paymentArr[4]).length == 0 || isNaN(paymentInvoiceNum) == true) {
        //            // Find oldest receivable
        //            var statusDate = new Date(invoiceModel.invStatusDate.time);
        //            Avo_LogDebug("Status Date(" + aa.util.formatDate(statusDate, "MM/dd/yyyy") + ")", 2);	//debug

        //            if (oldestDate && statusDate.getTime() > oldestDate.getTime()) {
        //                continue;
        //            }

        //            var invoiceNumToCheck = parseInt(invoiceModel.invNbr, 10);
        //            Avo_LogDebug("Invoice # to Check(" + invoiceNumToCheck + ")", 2);	//debug

        //            if (isNaN(invoiceNumToCheck) == true || (invoiceId && invoiceNumToCheck > invoiceId)) {
        //                continue;
        //            }

        //            invoiceId = invoiceNumToCheck;
        //            Avo_LogDebug("Record Inv Num(" + invoiceId + ")", 2);  //debug

        //            oldestDate = statusDate;
        //        } else {
        //            var altInvNum = parseInt(invoiceModel.customizedNbr, 10);
        //            Avo_LogDebug("Record Inv Alt Num(" + altInvNum + ")", 2);	//debug

        //            if (altInvNum != paymentInvoiceNum) {
        //                continue;
        //            }

        //            invoiceId = invoiceModel.invNbr;
        //            Avo_LogDebug("Record Inv Num(" + invoiceId + ")", 2);  //debug
        //            Avo_LogDebug("Found matching invoice " + invoiceId + " on record " + altId, 1);
        //            break;
        //        }
        //    }

        //    if (!invoiceId) {
        //        if (String(paymentArr[4]).length > 0 || isNaN(paymentInvoiceNum) != true) {
        //            Avo_LogDebug("Failed to find matching invoice number " + paymentInvoiceNum + " on record " + altId, 1);
        //        }
        //        if (String(paymentArr[4]).length == 0 || isNaN(paymentInvoiceNum) == true) {
        //            Avo_LogDebug("Failed to find oldest invoice number on record " + altId, 1);
        //        }
        //    } else {
        //        Avo_LogDebug("Invoice #(" + invoiceId + ")", 2);	//debug
        //    }
        //}

        //// Check for unpaid, invoiced fees
        //if (Object.keys(allFeeObjs).length == 0) {
        //    Avo_LogDebug("No unpaid, invoiced fees on record " + altId, 1);

        //    var allAssocRecs = getRelatedRecords(recordId);
        //    if (allAssocRecs[0].recordId != recordId) {
        //        for (var j in allAssocRecs) {
        //            var childRecordId = allAssocRecs[j].recordId;
        //            var childAltId = allAssocRecs[j].altId;
        //            Avo_LogDebug("Child(" + childAltId + ")", 2);	//debug

        //            var childFeeObjs = getFeeInfo(childRecordId, childAltId);

        //            if (Object.keys(childFeeObjs).length == 0) {
        //                Avo_LogDebug("No unpaid, invoiced fees on child record " + childAltId, 1);
        //                continue;
        //            }

        //            recordId = childRecordId;
        //            altId = childAltId;
        //            allFeeObjs = childFeeObjs;
        //            break;
        //        }
        //    }
        //}

        var paymentRemaining = totalPayment;
        var allReceivables = getRelatedRecords(recordId);

        var cap = aa.cap.getCap(recordId).getOutput();
        var recordType = cap.capType;
        Avo_LogDebug("Record Type(" + recordType + ")", 2);	//debug

        // Get file date
        var fileDate = new Date(cap.fileDate.epochMilliseconds);
        Avo_LogDebug("File date(" + aa.util.formatDate(fileDate, "MM/dd/yyyy") + ")", 2);	//debug

        var recordToAdd = new Object();
        recordToAdd.recordId = recordId;
        recordToAdd.altId = altId;
        recordToAdd.type = recordType;
        recordToAdd.openDate = fileDate;
        allReceivables.unshift(recordToAdd);
        Avo_LogDebug("Total Receivables(" + allReceivables.length + ")", 2);    //debug

        for (var j in allReceivables) {
            Avo_LogDebug("Payment Remaining($" + paymentRemaining + ")", 2);    //debug
            if (paymentRemaining == 0) {
                Avo_LogDebug("Payment Complete", 1);
                break;
            }

            if (!allReceivables[j]) {
                continue;
            }

            var recordId = allReceivables[j].recordId;
            //var altId = cap.capModel.altID;
            var altId = allReceivables[j].altId;
            Avo_LogDebug(br + "Receivable(" + altId + ")", 2);  //debug

            var result = aa.cap.getCapDetail(recordId);
            if (result.getSuccess() != true) {
                Avo_LogDebug("Failed to get record details for record " + altId + ". " + result.errorType + ': ' + result.errorMessage, 1);
                continue;
            }

            var capDetails = result.getOutput();
            var balanceRemaining = capDetails.getBalance();
            Avo_LogDebug("Balance Due($" + balanceRemaining + ")", 2);    //debug

            if (balanceRemaining <= 0) {
                continue;
            }

            var result = aa.cap.getCap(recordId);
            if (result.getSuccess() != true) {
                Avo_LogDebug("Failed to get cap for record " + altId + ". " + result.errorType + ': ' + result.errorMessage, 1);
                continue;
            }

            var cap = result.getOutput();

            if (balanceRemaining > paymentRemaining) {
                balanceRemaining = paymentRemaining;
            }

            var iterations = 0;
            while (balanceRemaining > 0) {
                iterations++;
                Avo_LogDebug("Iteration(" + iterations + ")", 2);   //debug

                if (iterations > 10) {
                    break;
                }

                //Avo_LogDebug("Balance Remaining($" + balanceRemaining + ")", 2);    //debug

                // Get all unpaid, invoiced fees
                var allFeeObjs = new Object();

                // Get invoice for oldest receivable if none provided
                //if (invoiceId == null) {
                if (invoiceNum == null) {
                    result = aa.finance.getInvoiceByCapID(recordId, aa.util.newQueryFormat());
                    if (result.getSuccess() != true) {
                        Avo_LogDebug("Failed to get all invoices on record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);
                    } else {
                        var oldestDate = null;

                        var allInvoices = result.getOutput();
                        for (var k in allInvoices) {
                            var invoiceModel = allInvoices[k].invoiceModel;

                            var invoiceIdToCheck = parseInt(invoiceModel.invNbr, 10);
                            Avo_LogDebug("Invoice ID to Check(" + invoiceIdToCheck + ")", 2);	//debug

                            var invoiceNumToCheck = parseInt(invoiceModel.customizedNbr, 10);
                            Avo_LogDebug("Invoice # to Check(" + invoiceNumToCheck + ")", 2);	//debug

                            if (!("balanceDue" in invoiceModel)) {
                                continue;
                            }

                            var invoiceBalance = parseFloat(invoiceModel.balanceDue);
                            Avo_LogDebug("Invoice Balance($" + invoiceBalance + ")", 2);    //debug

                            if (invoiceBalance <= 0) {
                                continue;
                            }

                            // Find oldest receivable
                            var statusDate = new Date(invoiceModel.invStatusDate.time);
                            Avo_LogDebug("Status Date(" + aa.util.formatDate(statusDate, "MM/dd/yyyy") + ")", 2);	//debug

                            //if (oldestDate) {
                            //    Avo_LogDebug("Oldest Date Time(" + oldestDate.getTime() + ")", 2);	//debug
                            //    Avo_LogDebug("Status Date Time(" + statusDate.getTime() + ")", 2);	//debug
                            //}

                            if (oldestDate && statusDate.getTime() > oldestDate.getTime()) {
                                continue;
                            }

                            //if (isNaN(invoiceIdToCheck) == true || (invoiceId && invoiceIdToCheck > invoiceId)) {
                            //    continue;
                            //}
                            if (isNaN(invoiceNumToCheck) == true) {
                                Avo_LogDebug("Invalid invoice # " + invoiceModel.customizedNbr + ". Skipping", 1); //debug
                                continue;
                            }

                            //if ((oldestDate && statusDate.getTime() == oldestDate.getTime()) && (invoiceNum && invoiceNumToCheck > invoiceNum)) {
                            //    continue;
                            //}

                            //invoiceId = invoiceNumToCheck;
                            //Avo_LogDebug("Record Invoice ID(" + invoiceId + ")", 2);  //debug

                            invoiceNum = invoiceModel.customizedNbr;
                            Avo_LogDebug("Record Invoice Num(" + invoiceNum + ")", 2);  //debug

                            oldestDate = statusDate;
                        }

                        //if (!invoiceId) {
                        //    Avo_LogDebug("Failed to find oldest invoice number on record " + altId, 1);
                        //}
                        if (!invoiceNum) {
                            Avo_LogDebug("Failed to find oldest invoice number on record " + altId, 1);
                        }
                    }
                }

                //Avo_LogDebug("Invoice ID(" + invoiceId + ")", 2);	//debug
                Avo_LogDebug("Invoice #(" + invoiceNum + ")", 2);	//debug

                //if (invoiceId != null) {
                //    allFeeObjs = getFeeInfo(recordId, altId, invoiceId);
                if (invoiceNum != null) {
                    allFeeObjs = getFeeInfo(recordId, altId, invoiceNum);
                } else {
                    allFeeObjs = getFeeInfo(recordId, altId);
                }

                var totalFeeObjs = Object.keys(allFeeObjs).length;
                Avo_LogDebug("Fee Objs(" + totalFeeObjs + ")", 2);	//debug

                var feeSeqNbrArray = new Array();
                var invIdArray = new Array();
                var allInvoices = "";
                var allFeeCodes = "";
                var invoiceNumArr = new Object();
                var feeAllocArray = new Array();

                var totalFeeBalance = 0;
                var totalToApply = 0;

                // for (var j = 0; j < totalFeeObjs; j++) {
                for (var feeId in allFeeObjs) {
                    //if (totalToApply >= balanceRemaining) {
                    //    break;
                    //}

                    // var feeId = Object.keys(allFeeObjs)[j];
                    // Avo_LogDebug("Fee Id(" + feeId + ")", 2);	//debug

                    var feeObj = allFeeObjs[feeId];
                    var feeSeqNbr = feeObj.feeId;
                    Avo_LogDebug("Fee Seq Nbr(" + feeSeqNbr + ")", 2);	//debug

                    var feeInvNum = feeObj.invoiceNum;
                    Avo_LogDebug("Fee Inv #(" + feeInvNum + ")", 2);	//debug

                    var feeInvId = feeObj.invoiceId;
                    Avo_LogDebug("Fee Inv ID(" + feeInvId + ")", 2);	//debug

                    //if (invoiceId && feeInvId != invoiceId) {
                    //    Avo_LogDebug("Failed to match invoice number on fee " + feeId, 1);
                    //    continue;
                    //}
                    if (invoiceNum && feeInvNum != invoiceNum) {
                        Avo_LogDebug("Failed to match invoice number on fee " + feeId, 1);
                        continue;
                    }

                    Avo_LogDebug("Fee($" + feeObj.feeAmount + ")", 2);	//debug
                    Avo_LogDebug("Paid($" + feeObj.paidAmount + ")", 2);	//debug

                    var feeBalance = feeObj.feeAmount - feeObj.paidAmount;
                    Avo_LogDebug("Fee Balance($" + feeBalance + ")", 2);	//debug

                    totalFeeBalance += feeBalance;

                    if (totalToApply >= balanceRemaining) {
                        continue;
                    }

                    var feeAllocation = feeBalance;
                    if (feeAllocation + totalToApply > balanceRemaining) {
                        feeAllocation = balanceRemaining - totalToApply;
                    }

                    Avo_LogDebug("Fee Allocation($" + feeAllocation + ")", 2);	//debug

                    // feeSeqNbrArray[j] = feeSeqNbr;
                    // invIdArray[j] = invoiceId;
                    // feeAllocArray[j] = amountUnpaid;
                    feeSeqNbrArray.push(feeSeqNbr);
                    invIdArray.push(feeInvId);
                    feeAllocArray.push(feeAllocation);
                    totalToApply += feeAllocation;

                    if (feeInvNum in invoiceNumArr) {
                        continue;
                    }

                    if (allInvoices.length > 0) {
                        allInvoices += ", ";
                    }
                    //allInvoices += feeInvId;
                    allInvoices += feeInvNum;
                    invoiceNumArr[feeInvNum] = feeInvNum;

                    if (allFeeCodes.length > 0) {
                        allFeeCodes += ", ";
                    }

                    allFeeCodes += feeObj.feeCode;
                }

                Avo_LogDebug("Total Fee Balance($" + totalFeeBalance + ")", 2);	//debug
                Avo_LogDebug("Total to apply($" + totalToApply + ")", 2);	//debug

                // Populate paymentModel
                var paymentModel = aa.finance.createPaymentScriptModel();
                // var paymentId = paymentModel.getPaymentSeqNbr();
                // Avo_LogDebug("Payment Seq(" + paymentId + ")", 2);  //debug

                paymentModel.setAuditDate(aa.date.getCurrentDate());
                paymentModel.setAuditStatus("A");
                paymentModel.setCapID(recordId);
                paymentModel.setCashierID(systemSource);
                // paymentModel.setPaymentSeqNbr(paymentId);
                paymentModel.setPaymentAmount(totalToApply);
                paymentModel.setAmountNotAllocated(totalToApply);
                paymentModel.setPaymentChange(0);
                paymentModel.setPaymentDate(dateTime);
                paymentModel.setPaymentMethod(method);
                paymentModel.setPaymentStatus("Paid");

                // Make payment
                var result = aa.finance.makePayment(paymentModel);
                if (result.getSuccess() != true) {
                    Avo_LogDebug("Failed to make payment on record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);
                    //continue;
                    break;
                }

                var paymentId = result.getOutput();
                //Avo_LogDebug("Made Payment Seq(" + paymentId + ")", 2);  //debug

                Avo_LogDebug("Made payment " + paymentId + " of $" + totalToApply + " on record " + altId, 1);

                // Update balances
                paymentRemaining -= totalToApply;
                balanceRemaining -= totalToApply;

                result = aa.finance.getPaymentByPK(recordId, paymentId, currentUserID);
                if (result.getSuccess() != true) {
                    Avo_LogDebug("Failed to retrieve payment from record " + altId + ", must apply payment manually. " + result.errorType + ": " + result.errorMessage, 1);
                    //continue;
                    break;
                }

                paymentModel = result.getOutput();

                Avo_LogDebug("Payment Seq(" + paymentModel.getPaymentSeqNbr() + ")", 2);  //debug

                if (paymentId != paymentModel.getPaymentSeqNbr()) {
                    Avo_LogDebug("Failed to get current payment model for payment " + paymentId + " on record " + altId, 1);
                    //continue;
                    break;
                }

                // Generate Receipt
                result = aa.finance.generateReceipt(recordId, aa.date.getCurrentDate(), paymentModel.getPaymentSeqNbr(), paymentModel.getCashierID(), null);
                if (result.getSuccess() != true) {
                    Avo_LogDebug("Failed to generate receipt for payment " + paymentModel.getPaymentSeqNbr() + " on record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);
                } else {
                    Avo_LogDebug("Generated receipt for payment " + paymentModel.getPaymentSeqNbr() + " on record " + altId, 1);
                }

                // Apply payments
                // need to figure out how to get payment script model of resulting payment, and paymentFeeStatus and paymentIvnStatus
                var invoiceBalanceDue = totalFeeBalance - totalToApply;
                Avo_LogDebug("Invoice Balance Remaining($" + invoiceBalanceDue + ")", 2);	//debug

                result = aa.finance.applyPayment(recordId, paymentModel.getPaymentSeqNbr(), 0, feeSeqNbrArray, invIdArray, feeAllocArray, aa.date.getCurrentDate(), "Paid", "Paid", paymentModel.getCashierID(), null);
                if (result.getSuccess() != true) {
                    Avo_LogDebug("Failed to apply payment " + paymentModel.getPaymentSeqNbr() + " on record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);
                    //continue;
                    break;
                }

                Avo_LogDebug("Success applying payment " + paymentModel.getPaymentSeqNbr() + " of $" + totalToApply + " towards fee(s) " + allFeeCodes + " on invoice(s) " + allInvoices + " on record " + altId, 1);
                //Avo_LogDebug("Success applying payment " + paymentModel.getPaymentSeqNbr() + " of $" + totalToApply + " towards invoice " + invoiceNum + " on record " + altId, 1);

                //invoiceId = null;
                invoiceNum = null;

                Avo_LogDebug("Balance Remaining($" + balanceRemaining + ")", 2);    //debug
                if (balanceRemaining > 0) {
                    continue;
                }

                var capTypeModel = cap.capType;
                Avo_LogDebug("Record Type(" + String(capTypeModel) + ")", 2); //debug

                var capSubType = capTypeModel.getSubType();
                var capCategory = capTypeModel.getCategory();
                if (capCategory != "Application" && capCategory != "Renewal") {
                    continue;
                }

                var taskStatus;
                capId = recordId;

                if (capCategory == "Application") {
                    taskStatus = "Closed: Issued";

                    if (capSubType == "Alarm User") {
                        taskStatus = "Closed: Permit Issued";
                    }

                    // Check for an active task
                    var allTaskNames = ["Permit Application", "Issue License", "Issue Permit"];
                    for (var k in allTaskNames) {
                        var taskName = allTaskNames[k];

                        var result = aa.workflow.getTask(recordId, taskName);
                        if (result.getSuccess() != true) {
                            Avo_LogDebug('Failed to get "' + taskName + '" task. ' + result.errorType + ": " + result.errorMessage, 1);
                            continue;
                        }

                        var task = result.getOutput();
                        var isActive = task.activeFlag == "Y";
                        Avo_LogDebug(taskName + " Active(" + isActive + ")", 2);  //debug

                        if (isActive != true) {
                            continue;
                        }

                        // Close task
                        var success = closeTask(taskName, taskStatus, "Closed by Batch_ImportPaymentData", "");
                        if (success == false) {
                            Avo_LogDebug('Failed to close task "' + taskName + '" with status of "' + taskStatus + '" on application ' + altId, 1);
                            continue;
                        }

                        Avo_LogDebug('Closed task "' + taskName + '" with status of "' + taskStatus + '" on application ' + altId, 1);

                        // Create license
                        include("Avo_CreateLicense");
                        break;
                    }
                }

                if (capCategory == "Renewal") {
                    taskStatus = "Closed: Paid";
                    var taskName = "Renewal";

                    // Check Renewal task is active
                    var result = aa.workflow.getTask(recordId, taskName);
                    if (result.getSuccess() != true) {
                        Avo_LogDebug('Failed to get "' + taskName + '" task. ' + result.errorType + ": " + result.errorMessage, 1);
                        continue;
                    }

                    var task = result.getOutput();
                    var isActive = task.activeFlag == "Y";
                    Avo_LogDebug(taskName + " Active(" + isActive + ")", 2);  //debug

                    if (isActive != true) {
                        continue;
                    }

                    // Close task
                    var success = closeTask(taskName, taskStatus, "Closed by Batch_ImportPaymentData", "");
                    if (success == false) {
                        Avo_LogDebug('Failed to close task "' + taskName + '" with status of "' + taskStatus + '" on renewal ' + altId, 1);
                        continue;
                    }

                    Avo_LogDebug('Closed task "' + taskName + '" with status of "' + taskStatus + '" on renewal ' + altId, 1);

                    // Update license
                    include("Avo_UpdateLicense");
                }
            }
        }

        if (paymentRemaining == 0) {
            continue;
        }

        // Populate paymentModel
        var paymentModel = aa.finance.createPaymentScriptModel();
        paymentModel.setAuditDate(aa.date.getCurrentDate());
        paymentModel.setAuditStatus("A");
        paymentModel.setCapID(primaryRecordId);
        paymentModel.setCashierID(systemSource);
        paymentModel.setPaymentAmount(paymentRemaining);
        paymentModel.setAmountNotAllocated(paymentRemaining);
        paymentModel.setPaymentChange(0);
        paymentModel.setPaymentDate(dateTime);
        paymentModel.setPaymentMethod(method);
        paymentModel.setPaymentStatus("Paid");

        // Make payment
        var result = aa.finance.makePayment(paymentModel);
        if (result.getSuccess() != true) {
            Avo_LogDebug("Failed to make payment to primary record " + primaryAltId + ". " + result.errorType + ": " + result.errorMessage, 1);
            continue;
        }

        var paymentId = result.getOutput();
        //Avo_LogDebug("Made Payment Seq(" + paymentId + ")", 2);  //debug

        Avo_LogDebug("Made payment " + paymentId + " of $" + paymentRemaining + " to primary record " + primaryAltId, 1);
    }
}

function getApp(licNum) {
    Avo_LogDebug("getApp(" + licNum + ")", 2);  //debug

    var selectString = "Select B1_ALT_ID "
        + "From B1PERMIT "
        + "Where B1_ALT_ID Like '%-" + licNum + "-APP'";

    try {
        var initialContext = aa.proxyInvoker.newInstance("javax.naming.InitialContext", null).getOutput();
        var ds = initialContext.lookup("java:/AA");
        var conn = ds.getConnection();
        var SQLStatement = conn.prepareStatement(selectString);
        var rSet = SQLStatement.executeQuery();

        while (rSet.next()) {
            var altId = String(rSet.getString("B1_ALT_ID"));
            if (!altId || altId.length == 0 || altId == "null") {
                continue;
            }

            return altId;
        }

        return '';
    }
    catch (e) {
        Avo_LogDebug("Exception getting data from B3CONTACT: " + e.message, 1);
    }
    finally {
        initialContext.close();
        if (SQLStatement) {
            SQLStatement.close();

            if (rSet) {
                rSet.close();
            }
        }

        conn.close();
    }
}

function getRelatedRecords(recordId) {
    var altId = aa.cap.getCap(recordId).getOutput().capModel.altID;
    Avo_LogDebug("getRelatedRecords(" + altId + ")", 2);

    var allAssocRecs = new Array();

    //var result = aa.cap.getCap(recordId);
    //if (result.getSuccess() != true) {
    //    Avo_LogDebug("Failed to get cap for license " + recordId + ". " + result.errorMessage, 1);
    //    return allAssocRecs;
    //}

    //var licCap = result.getOutput();
    //var licAltId = licCap.getCapModel().altID;

    //// Get record type
    //var recordType = licCap.capType;
    //Avo_LogDebug("Record Type(" + recordType + ")", 2);	//debug

    //// Get file date
    //var fileDate = new Date(licCap.fileDate.epochMilliseconds);
    //Avo_LogDebug("File date(" + aa.util.formatDate(fileDate, "MM/dd/yyyy") + ")", 2);	//debug

    //var licToAdd = new Object();
    //licToAdd.recordId = recordId;
    //licToAdd.altId = licAltId;
    //licToAdd.type = recordType;
    //licToAdd.openDate = fileDate;

    //allAssocRecs.push(licToAdd);

    // Get children
    var allChildren = getChildren("Licenses/Business/*/*", recordId);
    if (allChildren && allChildren.length > 0) {
        for (var i in allChildren) {
            var childCapId = allChildren[i];
            if (!childCapId) {
                Avo_LogDebug("Invalid capId", 1);
                continue;
            }

            var result = aa.cap.getCap(childCapId);
            if (result.getSuccess() != true) {
                Avo_LogDebug("Failed to get cap for child record " + childCapId + ". " + result.errorType + ": " + result.errorMessage, 1);
                continue;
            }

            var childCap = result.getOutput()
            var childAltId = childCap.getCapModel().altID;
            Avo_LogDebug("Child(" + childAltId + ")", 2);	//debug

            var recordType = childCap.capType;
            Avo_LogDebug("Record Type(" + recordType + ")", 2);	//debug

            // Get file date
            var fileDate = new Date(childCap.fileDate.epochMilliseconds);
            Avo_LogDebug("File date(" + aa.util.formatDate(fileDate, "MM/dd/yyyy") + ")", 2);	//debug

            var childToAdd = new Object();
            childToAdd.recordId = childCapId;
            childToAdd.altId = childAltId;
            childToAdd.type = recordType;
            childToAdd.openDate = fileDate;

            allAssocRecs.push(childToAdd);
        }
    }

    // Get renewals
    var result = aa.cap.getProjectByMasterID(recordId, "Renewal", null);
    if (result.getSuccess() != true) {
        Avo_LogDebug("No renewals on record " + altId, 1);
    } else {
        var allRenewals = result.getOutput();
        for (var i in allRenewals) {
            var renewalCapId = allRenewals[i].capID;
            if (!renewalCapId) {
                Avo_LogDebug("Invalid capId", 1);
                continue;
            }

            var result = aa.cap.getCap(renewalCapId);
            if (result.getSuccess() != true) {
                Avo_LogDebug("Failed to get cap for child record " + renewalCapId + ". " + result.errorType + ": " + result.errorMessage, 1);
                continue;
            }

            var renewalCap = result.getOutput()
            var renewalAltId = renewalCap.getCapModel().altID;
            Avo_LogDebug("Renewal(" + renewalAltId + ")", 2);	//debug

            var recordType = renewalCap.capType;
            Avo_LogDebug("Record Type(" + recordType + ")", 2);	//debug

            // Get file date
            var fileDate = new Date(renewalCap.fileDate.epochMilliseconds);
            Avo_LogDebug("File date(" + aa.util.formatDate(fileDate, "MM/dd/yyyy") + ")", 2);	//debug

            var childToAdd = new Object();
            childToAdd.recordId = renewalCapId;
            childToAdd.altId = renewalAltId;
            childToAdd.type = recordType;
            childToAdd.openDate = fileDate;

            allAssocRecs.push(childToAdd);
        }
    }

    // Sort list by date asc
    allAssocRecs.sort(function (a, b) { return a.openDate.getTime() - b.openDate.getTime(); });

    // for(i in allAssocRecs) {
    // var record = allAssocRecs[i];

    // printLine(record.altId);
    // }

    return allAssocRecs;
}

function getFeeInfo(recordId, altId) {
    //var invoiceId = null;
    //if (arguments.length > 2 && arguments[2]) {
    //    invoiceId = arguments[2];
    //}

    //Avo_LogDebug("getFeeInfo(" + altId + ", " + invoiceId + ")", 2); //debug
    var invoiceNum = null;
    if (arguments.length > 2 && arguments[2]) {
        invoiceNum = arguments[2];
    }

    Avo_LogDebug("getFeeInfo(" + altId + ", " + invoiceNum + ")", 2); //debug

    var allFeeObjs = new Object();

    // Get invoice info for all unpaid, invoice fees
    result = aa.finance.getFeeItemInvoiceByCapID(recordId, aa.util.newQueryFormat());
    if (result.getSuccess() != true) {
        Avo_LogDebug("Failed to get all invoice info for fees on record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);
        return allFeeObjs;
    }

    var allFeeItems = result.getOutput();
    for (var i in allFeeItems) {
        var feeItemInvoiceModel = allFeeItems[i];
        //Avo_LogDebug(serialize(feeItemInvoiceModel), 2);    //debug

        var feeCode = feeItemInvoiceModel.feeCode;
        Avo_LogDebug(br + "Fee Code(" + feeCode + ")", 2);   //debug

        var feeStatus = feeItemInvoiceModel.feeitemStatus;
        Avo_LogDebug("Fee Status(" + feeStatus + ")", 2);  //debug

        if (feeStatus != "INVOICED") {
            continue;
        }

        //if (feeStatus == "VOIDED" || feeStatus == "CREDITED") {
        //    continue;
        //}

        var feeInvoiceId = feeItemInvoiceModel.invoiceNbr;
        Avo_LogDebug("Fee Invoice Id(" + feeInvoiceId + ")", 2);   //debug

        var feeInvoiceNum = feeItemInvoiceModel.x4FeeItemInvoice.customizedInvNbr;
        Avo_LogDebug("Fee Invoice #(" + feeInvoiceNum + ")", 2);   //debug

        //if (invoiceId != null && invoiceId != feeInvoiceId) {
        //    //Avo_LogDebug("Failed to match invoice number " + invIdToMatch + " on record " + altId, 1);
        //    continue;
        //}
        if (invoiceNum != null && invoiceNum != feeInvoiceNum) {
            continue;
        }

        var feeObj = new Object();

        var feeId = feeItemInvoiceModel.feeSeqNbr;
        feeObj.feeId = feeId;
        Avo_LogDebug("Fee ID(" + feeId + ")", 2);   //debug

        feeObj.feeCode = feeCode;

        var amount = feeItemInvoiceModel.fee;
        feeObj.feeAmount = amount;
        Avo_LogDebug("Fee($" + amount + ")", 2);   //debug

        feeObj.paidAmount = 0;

        feeObj.invoiceId = feeInvoiceId;
        feeObj.invoiceNum = feeInvoiceNum

        allFeeObjs[feeId] = feeObj;
    }

    /* 	// Verify all unpaid, invoiced fees
        var result = aa.finance.getFeeItemByCapID(recordId);
        if (result.getSuccess() != true) {
            Avo_LogDebug("Failed to get all fees on record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);
            return allFeeObjs;
        }
    
        var allFees = result.getOutput();
        for (i in allFees) {
            var feeItem = allFees[i];
    
            var invStatus = feeItem.feeitemStatus;
            Avo_LogDebug("Inv Status(" + invStatus + ")", 2);	//debug
            
            if (invStatus != "INVOICED") {
                continue;
            }
            
            var feeId = feeItem.feeSeqNbr;
            if(!(feeId in allFeeObjs)) {
                Avo_LogDebug("Failed to find invoiced fee " + feeId + " on record " + altId, 1);
                continue;
            }
    
            var feeObj = allFeeObjs[feeId]
    
            var amount = feeItem.fee;
            if(amount != feeObj.feeAmount) {
                Avo_LogDebug("Failed to match amount for fee " + feeId, 1);
                continue;
            }
    
            var feeCode = feeItem.feeCod;
            if(feeCode != feeObj.feeCode) {
                Avo_LogDebug("Failed to match code for fee " + feeId, 1);
                continue;
            }
            
            feeObj.paidAmount = 0;
    
            allFeeObjs[feeItem.feeSeqNbr] = feeObj;
        } */

    // Get all payments
    result = aa.finance.getPaymentFeeItems(recordId, aa.util.newQueryFormat());
    if (result.getSuccess() != true) {
        Avo_LogDebug("Failed to get all payments on record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);
        return allFeeObjs;
    }

    var allPayments = result.getOutput();
    if (allPayments.length == 0) {
        Avo_LogDebug("No payments on record " + altId, 1);
        return allFeeObjs;
    }

    for (var i in allPayments) {
        var payment = allPayments[i];
        var paymentId = payment.paymentSeqNbr;

        var feeId = payment.feeSeqNbr;
        if (!(feeId in allFeeObjs)) {
            continue;
        }

        var feeObj = allFeeObjs[feeId];

        var invoiceId = payment.invoiceNbr;
        if (invoiceId != feeObj.invoiceId) {
            Avo_LogDebug("Failed to match invoice numbers for fee " + feeId + " and payment " + paymentId, 1);
            continue;
        }

        var fee = feeObj.feeAmount;
        var paidAmount = payment.feeAllocation;
        Avo_LogDebug("Paid Amount($" + paidAmount + ")", 2);   //debug

        if (paidAmount >= fee) {
            delete allFeeObjs[feeId];
            continue;
        }

        feeObj.paymentId = paymentId;
        feeObj.paidAmount += paidAmount;

        allFeeObjs[feeId] = feeObj;
    }

    return allFeeObjs;
}

function getDataFromFTP(sftpFolder, archiveFolder, filename) {
    var paymentData = new Array();

    //// Testing
    //paymentData = [
    //    //"2009513,10.00,10/01/2019,Cash,20190001453,WFLB-TL",
    //    "2009514,100.00,10/01/2019,Cash,20190001458,WFLB-TL",
    //    ""
    //];
    //return paymentData; // End testing

    include("EMSE:SFTPADAPTERAPI");
    eval(getScriptText("EMSE:SFTPADAPTERAPI"));

    adapterToken = getToken(adapterUsername, adapterPassword);
    Avo_LogDebug("Token(" + adapterToken + ")", 2);	//debug

    if (!adapterToken) {
        Avo_LogDebug("Failed to get token from SFTP Adapter @ " + adapterUrl, 1);
        return;
    }

    var extension = String(filename.substring(filename.lastIndexOf(".") + 1).toUpperCase());
    Avo_LogDebug("Extension(" + extension + ")", 2);  //debug

    var response = getData(sftpFolder, filename);
    //Avo_LogDebug("Response(" + response + ")", 2);  //debug

    if (!response) {
        Avo_LogDebug("Could not find file " + filename + " in folder " + sftpFolder, 1);
        return;
    }

    if ("ExceptionMessage" in response) {
        Avo_LogDebug('Failed to get file "' + filename + '" in folder "' + sftpFolder + '". ' + response["ExceptionMessage"], 1);
        Avo_LogDebug('"StackTrace":' + response["StackTrace"], 1);
        return;
    }

    // Archive file
    var contents = response.Contents;
    var saveResponse = saveData(archiveFolder, filename, contents);
    //Avo_LogDebug("Save Response(" + saveResponse + ")", 2);  //debug

    if (saveResponse === false) {
        Avo_LogDebug("Could not upload file " + filename + " to folder " + archiveFolder, 1);

        if (saveResponse && ("ExceptionMessage" in saveResponse)) {
            Avo_LogDebug('Failed to upload file "' + filename + '" to folder "' + archiveFolder + '". ' + saveResponse["ExceptionMessage"], 1);
            Avo_LogDebug('"StackTrace":' + saveResponse["StackTrace"], 1);
        }
    } else {
        Avo_LogDebug("Uploaded file " + filename + " to folder " + archiveFolder, 1);
    }

    switch (extension) {
        case "XML":
            var stream = aa.proxyInvoker.newInstance("java.io.StringBufferInputStream", new Array(response.Contents)).getOutput();
            var saxBuilder = aa.proxyInvoker.newInstance("org.jdom.input.SAXBuilder").getOutput();
            var document = saxBuilder.build(stream);

            var root = document.getRootElement();
            // printLine(Serialize(root));

            var errorNode = root.getChild("Error");

            var allActors = root.children;
            // printLine(Serialize(allActors));
            // printLine("Nodes(" + allActors.size() + ")");	//debug

            for (var i = 0; i < allActors.size(); i++) {
                var actor = allActors.get(i);
                // printLine(Serialize(actor));

                // var firstName = actor.getChild("FirstName");
                // var lastName = actor.getChild("LastName");
                // var age = actor.getChild("Age");
                // var city = actor.getChild("City");
                // var country = actor.getChild("Country");

                // printLine("Name: " + firstName + ", " + lastName);
                for (var j = 0; j < actor.children.size(); j++) {
                    var attribute = actor.children.get(j);
                    //printLine(Serialize(attribute));
                    break;
                }
            }
            break;

        case "CSV":
            var allLines = String(response.Contents).split(/\r\n|\n/);
            Avo_LogDebug("Total Lines(" + allLines.length + ")", 2);	//debug

            for (var i in allLines) {
                var line = allLines[i];
                //Avo_LogDebug("Line " + i + "(" + line + ")", 2);    //debug

                // Remove empty lines
                line = line.trim();
                if (line.length == 0) {
                    Avo_LogDebug("Line " + String(parseInt(i, 10) + 1) + " is blank", 1);
                    continue;
                }

                paymentData.push(line);
            }
            break;

        default:
            Avo_LogDebug('File type "' + extension + '" not supported', 1);
            break;
    }

    return paymentData;
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}