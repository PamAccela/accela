/*******************************************************
| Script Title: Batch_FTPAdapterTest (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 13Oct17
| Usage: 
| Modified by: ()
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
//aa.env.setValue("param1","Yes");
//aa.env.setValue("param1","No");
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var param1 = aa.env.getValue("param1");
//var param2 = aa.env.getValue("param2");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");
var currentUserID = "ADMIN";

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
//eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    aa.print("Batch Job " + batchJobName + " Job ID is " + batchJobID);
}
else {
    aa.print("Batch job ID not found " + batchJobResult.getErrorMessage());
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    aa.print("Start of Job");

    if (!timeExpired) {
        try {
            getData();
        }
        catch (e) {
            aa.print("Error in process " + e.message);
        }
    }
    else {
        aa.print("End of Job: Elapsed Time : " + elapsed() + " Seconds");
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

function saveData() {
    printLine("saveData()");

    var url = "http://10.220.106.28/Identity";

    var tokenUrl = "/Token";

    var username = "nic.bunting@avocette.com";
    var password = "Omega0!";

    var params = aa.httpClient.initPostParameters();
    params.put("grant_type", "password");
    params.put("username", username);
    params.put("Password", password);

    // printLine(Serialize(params, false));	//debug

    var client = aa.httpClient.post(url + tokenUrl, params);
    if (!client.getSuccess()) {
        printLine("**ERROR: " + client.message);
        return;
    }

    var response = JSON.parse(String(client.getOutput()));

    var token = response.access_token;
    if (token.length <= 0) {
        printLine("**ERROR: unable to get token");
        return;
    }

    // printLine("Token(" + token + ")");	//debug

    var docUrl = "/api/Document/";

    var filename = "testing.csv";
    var contents = "Hello in tv land";

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "text/plain; charset=utf-8");
    headers.put("Authorization", "Bearer " + token);

    client = aa.httpClient.post(url + docUrl + filename, headers, contents);
    if (!client.getSuccess()) {
        printLine("**ERROR: " + client.message);
        return;
    }

    response = client.getOutput();
    printLine("Ok");
}

function getData() {
    printLine("getData()");

    var url = "http://172.19.61.35/Identity";

    var tokenUrl = "/Token";

    var username = "AccelaFTPUser";
    var password = "x1transfer1234$";

    var params = aa.httpClient.initPostParameters();
    params.put("grant_type", "password");
    params.put("username", username);
    params.put("Password", password);

    // printLine(Serialize(params, false));	//debug

    var client = aa.httpClient.post(url + tokenUrl, params);
    if (!client.getSuccess()) {
        printLine("**ERROR: " + client.message);
        return;
    }

    var response = JSON.parse(String(client.getOutput()));

    var token = response.access_token;
    if (token.length <= 0) {
        printLine("**ERROR: unable to get token");
        return;
    }

    // printLine("Token(" + token + ")");	//debug

    var docUrl = "/api/Document/";
    //var filename = "test20171016_134251.xml";
    var filename = "TESTFILEFORNIC.csv";

    var extension = filename.substring(filename.lastIndexOf(".") + 1).toUpperCase();
    printLine("Extension(" + extension + ")");  //debug

    var fileHeaders = new Array("FirstName", "LastName", "Age", "City", "Country");

    var host = "172.19.61.35";
    var port = 21;
    var username = "AccelaFTP";
    var password = "txfr1234!";

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + token);

    client = aa.httpClient.get(url + docUrl + filename, headers);
    if (!client.getSuccess()) {
        printLine("**ERROR: " + client.message);
        return;
    }

    response = JSON.parse(String(client.getOutput()));

    switch (extension) {
        case "XML":
            var stream = aa.proxyInvoker.newInstance("java.io.StringBufferInputStream", new Array(response.Contents)).getOutput();
            var saxBuilder = aa.proxyInvoker.newInstance("org.jdom.input.SAXBuilder").getOutput();
            var document = saxBuilder.build(stream);

            var root = document.getRootElement();
            // printLine(Serialize(root));

            var errorNode = root.getChild("Error");

            var allActors = root.children;
            // printLine(Serialize(allActors));
            // printLine("Nodes(" + allActors.size() + ")");	//debug

            for (var i = 0; i < allActors.size() ; i++) {
                var actor = allActors.get(i);
                // printLine(Serialize(actor));

                // var firstName = actor.getChild("FirstName");
                // var lastName = actor.getChild("LastName");
                // var age = actor.getChild("Age");
                // var city = actor.getChild("City");
                // var country = actor.getChild("Country");

                // printLine("Name: " + firstName + ", " + lastName);
                for (var j = 0; j < actor.children.size() ; j++) {
                    var attribute = actor.children.get(j);
                    printLine(Serialize(attribute));
                    break;
                }
            }
            break;

        case "CSV":
            var allRows = String(response.Contents).split(/\r\n|\n/);
            for (var i = 0; i < allRows.length; i++) {
                var row = allRows[i];
                printLine(row);
                //var allValues = allRows[i].split(',');

                //var row = "";
                //for (var j = 0; j < allValues.length; j++) {
                //    var value = allValues[j];

                //    if (j > 0) {
                //        row += " ";
                //    }

                //    row += headers[j] + ": " + value + ";";
                //}

                //printLine(row);
            }
            break;

        default:
            printLine('File type "' + extension + '" not supported');
            break;
    }
}

function printLine(pStr) {
    aa.print(br + pStr);
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}