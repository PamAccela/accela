//WTUA:Licenses/*/*/Renewal

//call script ** ID5 ** script/functions based on requirements
if (wfTask == "Renewal") {
    if (wfStatus == "Closed: Paid") {
        include("Avo_UpdateLicense");
    } else {
        Avo_CloseLicense();
    }
}
if (wfTask == "Issue License") {
    if (wfStatus == "Closed: Issued") {
        include("Avo_UpdateLicense");
    } else {
        Avo_CloseLicense();
    }
}

/*******************************************************
| Script/Function: Avo_CloseLicense() - (ID5)
| Created by: Nicolaj Bunting
| Created on: 12Dec17
| Usage: If task "Renewal" has status other than "Closed: Paid" then get parent license and close active task with the renewal task status if possible otherwise use "Closed: Non Renwal"
| Modified by: ()
*********************************************************/
function Avo_CloseLicense() {
    logDebug("ID5 Avo_CloseLicense()");

    var renCapId = capId;

    // Get parent license
    aa.runScript("WORKFLOWTASKUPDATEAFTER4RENEW");	// Use event specific Renew script

    var result = aa.cap.getProjectByChildCapID(renCapId, "Renewal", "");
    if (result.getSuccess() == false) {
        Avo_LogDebug("Unable to retrieve parent license", 1);
        return;
    }

    var licCapId = result.getOutput()[0].getProjectID();
    var isLic = appMatch("Licenses/*/*/License", licCapId);
    if (isLic == false) {
        Avo_LogDebug("Parent record isn't a license", 1);
        return;
    }

    capId = licCapId;

    var licAltId = aa.cap.getCap(licCapId).getOutput().getCapModel().altID;
    Avo_LogDebug("Lic CapId(" + licAltId + ")", 2);    //debug

    // Get license active task
    var firstActiveTaskName = "";

    var allTasks = aa.workflow.getTasks(licCapId).getOutput();
    //Avo_LogDebug("Worktask Items(" + wfItems.length + ")", 2);	//debug

    for (i in allTasks) {
        var task = allTasks[i];
        var taskName = task.getTaskDescription();

        var active = isTaskActive(taskName);
        if (active == false) {
            continue;
        }

        firstActiveTaskName = taskName;
        break;
    }

    // Determine if license task has renewal status
    var taskObj = aa.workflow.getTask(capId, firstActiveTaskName).getOutput();

    var validStatus = aa.workflow.getTaskStatus(taskObj, wfStatus).getSuccess();
    Avo_LogDebug(wfStatus + " valid(" + validStatus + ")", 2); //debug

    if (validStatus == true) {
        // Close active task with renewal status
        closeTask(firstActiveTaskName, wfStatus, "Updated by EMSE ID5", "");
    } else {
        closeTask(firstActiveTaskName, "Closed: Non Renewal", "Updated by EMSE ID5", "");
    }

    capId = renCapId;
}