//ASA: Licenses/Business/Bus Reg Service/Application

//call script ** BRS01 ** script/functions based on requirements
Avo_BusRegServiceLicAppFees();

//call script ** BRS02 ** script/functions based on requirements
Avo_BusRegServicePenFees(capId);

aa.sendMail("tlicense@scottsdaleaz.gov", "PI_Test@avocette.com", "", "ASA:Licenses/Business/Bus Reg Service/Application", debug); //debug

/*******************************************************
| Script/Function: Avo_BusRegServiceLicAppFees() - (BRS01)
| Created by: Nicolaj Bunting
| Created on: 9Jan18
| Usage: Add and invoice fees "BRSAPP" and "BRSLIC" from schedule "LIC_BRS_APP"
| Modified by: ()
*********************************************************/
function Avo_BusRegServiceLicAppFees() {
    logDebug("BRS01 Avo_BusRegServiceLicAppFees()");

    addFee("BRSAPP", "LIC_BRS_APP", "FINAL", 1, "Y", capId);
    Avo_LogDebug("Added fee BRSAPP from schedule LIC_BRS_APP", 1);

    addFee("BRSLIC", "LIC_BRS_APP", "FINAL", 1, "Y", capId);
    Avo_LogDebug("Added fee BRSLIC from schedule LIC_BRS_APP", 1);
}