/*******************************************************
| Script/Function: CopyOpenedDate() - (ID89)
| Created by: Nicolaj Bunting
| Created on: 12Oct18
| Usage: Copy the record creation date to ASI "Date Setup In System"
| Modified by: ()
*********************************************************/
(function () {
    // Get creation date
    var result = aa.cap.getCap(capId);
    if (result.getSuccess() != true) {
        Avo_LogDebug("Failed to get cap. " + result.errorMessage, 1);
        return;
    }

    var cap = result.getOutput();
    var fileDate = new Date(cap.fileDate.epochMilliseconds);
    var fileDateStr = aa.util.formatDate(fileDate, "MM/dd/yyyy");
    Avo_LogDebug("File date(" + fileDateStr + ")", 2);	//debug

    // Update ASI
    editAppSpecific("Date Setup In System", fileDateStr, capId);
})();