/*******************************************************
| Script Title: Batch_AddToCollections (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 22Mar18
| Usage: For all "Licenses/Business/NA/NA" records if no payment has been recorded on an invoice 59 days after invoice date 
| then add condition "Past Due Balance" of type "Collection" and using first letter of app name as the key get user from std choice "COLLECTIONS_RANGE" 
| and assign that user to the record
| Modified by: Nic Bunting (9Oct19)
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
//aa.env.setValue("param1","Yes");
//aa.env.setValue("param1","No");
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var param1 = aa.env.getValue("param1");
//var param2 = aa.env.getValue("param2");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

// Print debug using aa.print instead of aa.debug
useLogDebug = false;

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    aa.print("Batch Job " + batchJobName + " Job ID is " + batchJobID);
}
else {
    aa.print("Batch job ID not found " + batchJobResult.getErrorMessage());
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    aa.print("Start of Job");

    if (!timeExpired) {
        try {
            AddCollectionsCondition();
        }
        catch (e) {
            aa.print("Error in process " + e.message);
        }
    }
    else {
        aa.print("End of Job: Elapsed Time : " + elapsed() + " Seconds");
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

function AddCollectionsCondition() {
    Avo_LogDebug("ID56 AddCollectionsCondition()", 1);

    var group = "Licenses";
    var type = "Business";
    var appsArray = aa.cap.getByAppType(group, type).getOutput();
    if (appsArray.length == 0) {
        Avo_LogDebug('No records of type "' + group + "/" + type + '/*/* found', 1);
        return;
    }

    var today = new Date();
    var fiftyNineDaysAgo = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 59);
    Avo_LogDebug("59 Days ago(" + aa.util.formatDate(fiftyNineDaysAgo, "MM/dd/yyyy") + ")", 2); //debug

    for (var a in appsArray) {
        capId = appsArray[a].getCapID();
        var altId = capId.getCustomID();

        var inCollections = false;

        var result = aa.finance.getInvoiceByCapID(capId, aa.util.newQueryFormat());
        if (result.getSuccess() == false) {
            continue;
        }
        var allInvoices = result.getOutput();
        for (var i in allInvoices) {
            var invoiceModel = allInvoices[i].invoiceModel;

            var amount = invoiceModel.invAmount;
            var balance = invoiceModel.balanceDue;
            if (!balance) {
                balance = 0;
            }

            // if(amount != balance) {
            // continue;
            // }
            if (balance == 0) {
                continue;
            }

            var invoiceDateStr = invoiceModel.invDate;
            var invoiceDate = Avo_GetDateFromAccelaDateString(invoiceDateStr);
            Avo_LogDebug(altId + " Invoice Date(" + aa.util.formatDate(invoiceDate, "MM/dd/yyyy") + ")", 2);    //debug

            if (invoiceDate.getTime() > fiftyNineDaysAgo.getTime()) {
                continue;
            }

            inCollections = true;
            break;
        }

        if (inCollections == false) {
            continue;
        }

        // Add condition
        if (appHasCondition("Collection", null, "Past Due Balance", null) == false) {
            addStdConditionAsUser("Collection", "Past Due Balance", "ADMIN", capId);

            if (appHasCondition("Collection", null, "Past Due Balance", null) == true) {
                Avo_LogDebug("Added Collection condition to record " + altId, 1);
            } else {
                Avo_LogDebug("Failed to add Collection condition to record " + altId, 1);
            }
        } else {
            Avo_LogDebug("Collection condition has already been added to record " + altId, 1);
        }

        // Get app name to determine key
        var appName = String(aa.cap.getCap(capId).getOutput().specialText);
        var firstChar = appName.substring(0, 1);

        var isDigit = !isNaN(firstChar);
        var key = "# - C";

        if (isDigit == true) {
            key = "# - C";
        } else {
            firstChar = firstChar.toUpperCase();

            if (firstChar >= "A" && firstChar <= "C") {
                key = "# - C";
            }

            if (firstChar >= "D" && firstChar <= "K") {
                key = "D - K";
            }

            if (firstChar >= "L" && firstChar <= "R") {
                key = "L - R";
            }

            if (firstChar >= "S" && firstChar <= "Z") {
                key = "S - Z";
            }
        }

        // Get user and assign to record
        var stdChoice = "COLLECTIONS_RANGE";
        var username = String(lookup(stdChoice, key));

        var isAssigned = assignCap(username, capId);
        if (isAssigned != false) {
            Avo_LogDebug("Assigned user " + username + " to record " + altId, 1);
        } else {
            Avo_LogDebug("Failed to assign user " + username + " to record " + altId, 1);
        }
    }
}

function addStdConditionAsUser(type, desc, username, recordId) {
    var customId = aa.cap.getCap(recordId).getOutput().capModel.altID;
    Avo_LogDebug("Record(" + customId + ")", 2);  //debug

    if (!aa.capCondition.getStandardConditions) {
        Avo_LogDebug("addStdCondition function is not available in this version of Accela Automation.", 1);
        return false;
    }

    var result = aa.capCondition.getStandardConditions(type, desc);
    if (result.getSuccess() != true) {
        Avo_LogDebug('Failed to get std condition "' + desc + '" of type "' + type + '". ' + result.errorType + ": " + result.errorMessage, 1);
        return false;
    }

    var allStdConditions = result.getOutput();
    if (allStdConditions.length == 0) {
        Avo_LogDebug('Found no std conditions named "' + desc + '" of type "' + type + '"', 1);
        return false;
    }

    result = aa.person.getUser(username);
    if (result.getSuccess() != true) {
        Avo_LogDebug("Failed to get sys user " + username + ". " + result.errorType + ": " + result.errorMessage, 1);
        return false;
    }

    var sysUserModel = result.getOutput();

    var conditionAdded = false;
    for (var i in allStdConditions) {
        var standardCondition = allStdConditions[i];
        var addCapCondResult = aa.capCondition.addCapCondition(recordId, standardCondition.getConditionType(), standardCondition.getConditionDesc(),
            standardCondition.getConditionComment(), sysDate, null, sysDate, null, null, standardCondition.getImpactCode(), sysUserModel, sysUserModel,
            "Applied", username, "A", null, standardCondition.getDisplayConditionNotice(), standardCondition.getIncludeInConditionName(),
            standardCondition.getIncludeInShortDescription(), standardCondition.getInheritable(), standardCondition.getLongDescripton(),
            standardCondition.getPublicDisplayMessage(), standardCondition.getResolutionAction(), null, null, standardCondition.getConditionNbr(),
            standardCondition.getConditionGroup(), standardCondition.getDisplayNoticeOnACA(), standardCondition.getDisplayNoticeOnACAFee(),
            standardCondition.getPriority(), standardCondition.getConditionOfApproval());

        if (addCapCondResult.getSuccess() != true) {
            Avo_LogDebug('Failed to add condition "' + standardCondition.getConditionDesc() + '" to record ' + customId + '. '
                + addCapCondResult.errorType + ': ' + addCapCondResult.errorMessage, 1);
            continue;
        }

        Avo_LogDebug('Added condition "' + standardCondition.getConditionDesc() + '" to record ' + customId, 1);
        conditionAdded = true;
    }

    return conditionAdded;
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}