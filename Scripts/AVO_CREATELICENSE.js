/*******************************************************
| Script/Function: Avo_CreateLicense() - (ID1, ID2)
| Created by: Nicolaj Bunting
| Created on: 1Dec17
| Usage: When application has task "Issue License" set to "Closed: Issued" 
| Or if record type is "Licenses/Business/Alarm User/Application" and task "Permit Application" has status set to "Closed: Permit Issued"
| Then create license record as parent, set altid, set app name, set issued date, copy all ASI, ASIT, and docs, 
| set expiration date, and generate report "" and attach to the license record
| Modified by: Nic Bunting (7Mar18)
| Modified by: Nic Bunting (9Mar18)
| Modified by: Nic Bunting (15Mar18)
| Modified by: Nic Bunting (17Apr18)
| Modified by: Nic Bunting (18Apr18)
| Modified by: Nic Bunting (18May18)
| Modified by: Nic Bunting (15Oct18)
| Modified by: Nic Bunting (3Jul19)
| Modified by: Nic Bunting (23Aug19)
| Modified by: Nic Bunting (2Oct19)
*********************************************************/
(function () {
    logDebug("ID1, ID2 Avo_CreateLicense()");

    var altId = aa.cap.getCap(capId).getOutput().getCapModel().altID;

    // Get Subtype
    var cap = aa.cap.getCap(capId).getOutput();
    var capTypeModel = cap.getCapType();
    var capSubType = String(capTypeModel.getSubType());

    var licCapId;
    if (capSubType != "Alarm User") {
        licCapId = createParent("Licenses", "Business", capSubType, "License", "");
    } else {
        licCapId = createParent("Licenses", "Business", "Alarm User", "Permit", "");
    }

    var licAltId = aa.cap.getCap(licCapId).getOutput().getCapModel().altID;
    Avo_LogDebug("Created parent license " + licAltId, 2);

    // Copy ASI, ASIT, and docs
    //copyAppSpecific(licCapId);
    Avo_CopyAppSpecific(capId, licCapId);
    copyASITables(capId, licCapId);

    //call script ** ID2 ** script/functions based on requirements
    // Set license number
    const regex = /\w{3}\d{4}-(\d{7})-APP/;

    var licNum = regex.exec(altId)[1];
    Avo_LogDebug("Lic Num(" + licNum + ")", 2);   //debug

    result = aa.cap.updateCapAltID(licCapId, licNum);
    if (!result.getSuccess()) {
        Avo_LogDebug("Failed to update license's alt ID", 1);
    } else {
        Avo_LogDebug("Updated license's alt ID to " + licNum, 1);
    }

    // Update task status
    var success = updateTask("License", "Active", "Updated by ID1, ID2 CreateLicense", "", "", licCapId);
    if (success == false) {
        Avo_LogDebug('Failed to set task "License" to status of "Active" on license ' + licNum, 1);
    } else {
        Avo_LogDebug('Set task "License" to status of "Active" on license ' + licNum, 1);
    }

    //call script ** ID64 ** script/functions based on requirements
    Avo_SetAppName(licCapId);

    // Set License start date
    var startDate = Avo_GetDateFromAccelaDateString(wfDateMMDDYYYY);
    var expDate = null;

    Avo_LogDebug("Subtype(" + capSubType + ")", 2);    //debug
    //logDebug("typeof Subtype(" + typeof (capSubType) + ")");    //debug

    switch (capSubType) {
        case "Adult Service Provider":
        case "Sexually Oriented Business Mgr":
            var appCapModel = aa.cap.getCap(capId).getOutput().capModel;
            var openDate = appCapModel.getFileDate();
            //logDebug("Open Date(" + aa.util.formatDate(openDate, "MM/dd/yyyy") + ")");

            startDate = new Date(openDate.time);
            break;

        case "After Hours Establishment":
        case "Bus Reg Merchant":
        case "Bus Reg Service":
        case "Recycling":
        case "Solid Waste":
            var busStartDate = getAppSpecific("Business Start Date", capId);
            startDate = Avo_GetDateFromAccelaDateString(busStartDate);
            break;

        case "Charitable Solicitations":
            var activityType = getAppSpecific("Activity Type", capId);
            if (activityType == "Permanent Location") {
                var busStartDate = getAppSpecific("Business Start Date", capId);
                startDate = Avo_GetDateFromAccelaDateString(busStartDate);
            }

            if (activityType == "Charitable Solicitation") {
                var startDateField = getAppSpecific("Start Date", capId);
                startDate = Avo_GetDateFromAccelaDateString(startDateField);
            }
            break;

        case "Close Out Sales":
            var saleDate = getAppSpecific("Date Sale to Commence", capId);
            startDate = Avo_GetDateFromAccelaDateString(saleDate);
            break;

        case "Liquor Special Event":
            var eventStartDate = getAppSpecific("Event Start Date", capId);
            startDate = Avo_GetDateFromAccelaDateString(eventStartDate);
            break;

        case "Liquor":
            var stateApprDate = getAppSpecific("State Approval Date", capId);
            startDate = Avo_GetDateFromAccelaDateString(stateApprDate);
            break;

        case "Promoter":
            var licStartDate = getAppSpecific("Requested License Start Date", capId);
            startDate = Avo_GetDateFromAccelaDateString(licStartDate);
            break;

        case "Solicitor For Profit":
            var tableName = "SOLICITATION DATES";
            startDate = null;
            expDate = null;

            var table = loadASITable(tableName);
            if (!table) {
                Avo_LogDebug(tableName + " does not exist on this record type", 1);
                return;
            }
            if (table.length == 0) {
                Avo_LogDebug(tableName + " table is empty", 1);
                return;
            }

            var dateColumn = "Date";

            for (var a in table) {
                var field = table[a][dateColumn];
                var entry = field.fieldValue;
                var readOnly = field.readOnly;

                var solicitationDate = Avo_GetDateFromAccelaDateString(String(entry));
                if (!startDate) {
                    startDate = solicitationDate;
                }
                if (!expDate) {
                    expDate = solicitationDate;
                }

                if (solicitationDate.getTime() < startDate.getTime()) {
                    startDate = solicitationDate;
                }

                if (solicitationDate.getTime() > expDate.getTime()) {
                    expDate = solicitationDate;
                }
            }
            break;

        case "Teletrack Operator":
        case "Teletrack Establishment":
            var reviewDate = getAppSpecific("City Council Review Date", capId);
            startDate = Avo_GetDateFromAccelaDateString(reviewDate);
            break;

        case "Valet Parking Special Event":
            var eventStartDate = getAppSpecific("Requested Event Start Date", capId);
            startDate = Avo_GetDateFromAccelaDateString(eventStartDate);
            break;

        case "Alarm User":
            var inOperationDate = getAppSpecific("Date Placed into Operation", capId);
            startDate = Avo_GetDateFromAccelaDateString(inOperationDate);

            var yearAgo = Avo_GetToday();
            yearAgo.setDate(yearAgo.getDate() - 365);
            Avo_LogDebug("Year ago(" + aa.util.formatDate(yearAgo, "MM/dd/yyyy") + ")", 2);   //debug

            if (startDate.getTime() <= yearAgo.getTime()) {
                Avo_LogDebug("Placed into operation a year ago or more", 2);    //debug
                var currentYear = Avo_GetToday().getFullYear();
                startDate.setFullYear(currentYear);
            }

            break;
    }

    if (!startDate) {
        Avo_LogDebug("Business start date is invalid or could not be found", 1);
        return;
    }

    Avo_LogDebug("Start Date(" + aa.util.formatDate(startDate, "MM/dd/yyyy") + ")", 2);

    // Set the record opened date
    var licCapModel = aa.cap.getCap(licCapId).getOutput().capModel;
    licCapModel.setFileDate(startDate);
    var result = aa.cap.editCapByPK(licCapModel);
    if (result.getSuccess() == false) {
        Avo_LogDebug("Failed to set Date Business Setup in System", 1);
    }

    // Set license end date
    if (capSubType != "Solicitor For Profit") {
        expDate = startDate;
    }

    switch (capSubType) {
        case "Adult Service Provider":
        case "Sexually Oriented Business Mgr":
        case "Teletrack Operator":
        case "Teletrack Establishment":
            Avo_LogDebug("Added 36 months", 1);
            expDate.setDate(expDate.getDate() + 1094);
            break;

        case "After Hours Establishment":
        case "Bus Reg Merchant":
        case "Bus Reg Service":
        case "Liquor":
        case "Recycling":
        case "Solid Waste":
            Avo_LogDebug("Dec 31st of this year", 1);
            expDate = new Date(expDate.getFullYear(), 12 - 1, 31);
            break;

        case "Alarm User":
        case "Auction House":
        case "Auctioneer":
        case "Escort Bureau":
        case "Escort":
        case "Magic Arts":
        case "Massage Facility":
        case "Neighborhood Street Vendor":
        case "Secondhand":
        case "Sexually Oriented Business":
        case "Teen Dance Center":
        case "Valet Parking":
            Avo_LogDebug("Added 12 months", 1);
            expDate.setDate(expDate.getDate() + 364);
            break;

        case "Charitable Solicitations":
            var activityType = getAppSpecific("Activity Type", capId);
            if (activityType == "Permanent Location") {
                Avo_LogDebug("Add 12 months", 1);
                expDate.setDate(expDate.getDate() + 364);
            }

            if (activityType == "Charitable Solicitation") {
                Avo_LogDebug("End Date", 1);
                var endDate = getAppSpecific("End Date", capId);
                expDate = Avo_GetDateFromAccelaDateString(endDate);
            }
            break;

        case "Close Out Sales":
            Avo_LogDebug("Added 30 days", 1);
            expDate.setDate(expDate.getDate() + 30);
            break;

        case "Liquor Special Event":
            Avo_LogDebug("Event End Date", 1);
            var eventEndDate = getAppSpecific("Event End Date", capId);
            expDate = Avo_GetDateFromAccelaDateString(eventEndDate);
            break;

        case "Promoter":
            Avo_LogDebug("Requested License End Date", 1);
            var licEndDate = getAppSpecific("Requested License End Date", capId);
            expDate = Avo_GetDateFromAccelaDateString(licEndDate);
            break;

        case "Valet Parking Special Event":
            Avo_LogDebug("Requested Event End Date", 1);
            var endDate = getAppSpecific("Requested Event End Date", capId);
            expDate = Avo_GetDateFromAccelaDateString(endDate);
            break;
    }

    if (!expDate) {
        Avo_LogDebug("No valid expiration date to set", 1);
        return;
    }

    Avo_LogDebug("Exp Date(" + aa.util.formatDate(expDate, "MM/dd/yyyy") + ")", 2);

    // Set expiration date
    var b1ExpResults = aa.expiration.getLicensesByCapID(licCapId);
    if (!b1ExpResults.getSuccess()) {
        Avo_LogDebug("Unable to retrieve expiration model", 1);
        return;
    }
    var b1Exp = b1ExpResults.getOutput();
    var b1ExpModel = b1Exp.getB1Expiration();
    if (!b1ExpModel) {
        Avo_LogDebug("Expiration model is null", 1);
        return;
    }

    //set the status
    b1ExpModel.setExpStatus("Active");

    //set the date
    b1ExpModel.setExpDate(expDate);

    //need to use editB1Expiration to make changes
    var b1EditResult = aa.expiration.editB1Expiration(b1ExpModel);
    if (b1EditResult.getSuccess() == false) {
        Avo_LogDebug("Failed to update expiration date and status", 1);
        return;
    }

    Avo_LogDebug("Set license expiration date to " + aa.util.formatDate(expDate, "MM/dd/yyyy"), 1);
})();