// DUA: Licenses/*/*/*

//call script ** ID52 ** script/functions based on requirements
Avo_SetDPSNum();

/*******************************************************
| Script/Function: Avo_SetDPSNum() - (ID52)
| Created by: Nicolaj Bunting
| Created on: 26Mar18
| Usage: When document is uploaded If doc group is "LIC_DOCS" and type is "Records Check", "Records Check - RESUBMITTAL", or "Records Check - DO NOT SUBMIT"
| then set field "DPS #" to YYYYMMDD And set field "Report Number" to "XX007272L"
| Modified by: Nic Bunting (19Apr18)
| Modified by: Nic Bunting (2May18)
| Modified by: Nic Bunting (4May18)
| Modified by: Nic Bunting (10Jun18)
*********************************************************/
function Avo_SetDPSNum() {
    logDebug("ID52 Avo_SetDPSNum()");

    //var docModelList = aa.env.getValue("DocumentModelList");

    //var entityAssociationModel = aa.env.getValue("DocumentReviewModel");
    //Avo_LogDebug(Serialize(entityAssociationModel, false), 2);  //debug

    //var document = aa.document.getDocumentByPK(entityAssociationModel.getDocumentID()).getOutput();
    //var docGroup = document.docGroup;
    //var docType = document.docCategory;
    //Avo_LogDebug("Doc Group(" + docGroup + "), Category(" + docType + ")", 2);  //debug

    var today = new Date();

    var year = String(today.getFullYear());
    var month = today.getMonth() + 1;
    var day = today.getDate();

    var dpsNum = year + (month < 10 ? "0" + month : String(month)) + (day < 10 ? "0" + day : String(day));
    Avo_LogDebug("DPS #(" + dpsNum + ")", 2);   //debug

    // Get Subtype
    var cap = aa.cap.getCap(capId).getOutput();
    var capTypeModel = cap.getCapType();
    var capSubType = String(capTypeModel.getSubType());
    Avo_LogDebug("Sub Type(" + capSubType + ")", 2);    //debug

    // Get report number
    var reportNum = null;
    switch (capSubType) {
        case "Auctioneer":
        case "Auction House":
        case "Secondhand":
        case "Solicitor For Profit":
        case "Adult Service Provider":
        case "After Hours Establishment":
        case "Escort Bureau":
        case "Escort":
        case "Magic Arts":
        case "Massage Facility":
        case "Neighborhood Street Vendor":
        case "Sexually Oriented Business Mgr":
        case "Sexually Oriented Business":
        case "Teen Dance Center":
        case "Teletrack Operator":
        case "Teletrack Establishment":
            reportNum = "XX007272L";
            break;

        default:
            return;
    }

    //Avo_LogDebug(Serialize(documentModelArray, true), 2);   //debug

    var docList = documentModelArray.toArray();
    for (i in docList) {
        var docModel = docList[i];
        //Avo_LogDebug(Serialize(docModel, false), 2);  //debug

        var group = docModel.docGroup;
        Avo_LogDebug("Group(" + group + ")", 2);   //debug

        if (group != "LIC_DOCS") {
            continue;
        }

        var category = docModel.docCategory;
        Avo_LogDebug("Category(" + category + ")", 2);   //debug

        if (category != "Records Check" && category != "Records Check - RESUBMITTAL" && category != "Records Check - DO NOT SUBMIT") {
            continue;
        }

        var docId = docModel.documentNo;
        Avo_LogDebug("Id(" + docId + ")", 2);   //debug

        var doc = aa.document.getDocumentByPK(docId).getOutput();
        //Avo_LogDebug(Serialize(doc, false), 2); //debug

        var dpsNumSet = false;
        var reportNumSet = false;

        var forms = doc.template.templateForms.toArray();
        for (j in forms) {
            var template = forms[j];
            var groupName = template.groupName;
            var allSubGroups = template.subgroups.toArray();
            for (k in allSubGroups) {
                var subGroup = allSubGroups[k];
                var subGroupName = subGroup.displayName;
                // printLine(Serialize(subGroup, false));

                var allFields = subGroup.fields.toArray();
                for (l in allFields) {
                    if (dpsNumSet == true && reportNumSet == true) {
                        break;
                        break;
                        break;
                    }

                    var field = allFields[l];
                    Avo_LogDebug("Field Name(" + field.fieldName + ")", 2);    //debug

                    Avo_LogDebug("Value(" + field.defaultValue + ")", 2);   //debug
                    //if (field.defaultValue) {
                    //    continue;
                    //}

                    if (field.fieldName == "DPS #") {
                        // field.checklistComment = "tested";
                        field.defaultValue = dpsNum;
                        // printLine(Serialize(field, true));
                        dpsNumSet = true;
                    }

                    if (field.fieldName == "Report Number" && reportNum != null) {
                        field.defaultValue = reportNum;
                        reportNumSet = true;
                    }
                }
            }
        }

        // printLine(Serialize(doc, false));
        var successMsg = "";
        if (dpsNumSet == true) {
            successMsg += "DPS #";
        }
        if (dpsNumSet == true && reportNumSet == true) {
            successMsg += " and ";
        }
        if (reportNumSet == true) {
            successMsg += "Report Number"
        }

        if (successMsg.length == 0) {
            Avo_LogDebug("DPS # and Report Number were not be set in document " + docId, 1);
            continue;
        }

        var result = aa.document.updateDocument(doc);
        if (result.getSuccess() == true) {
            Avo_LogDebug("Updated " + successMsg + " in document " + docId, 1);
        } else {
            Avo_LogDebug("Failed to update " + successMsg + " in document " + docId + ". " + result.getErrorMessage(), 1);
        }
    }
}