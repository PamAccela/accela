//CTRCA: Licenses/Business/*/Renewal

//call script ** ID2 ** script/functions based on requirements
Avo_SetRenewalID();

/*******************************************************
| Script/Function: Avo_SetRenewalID() - (ID2)
| Created by: Nicolaj Bunting
| Created on: 19Apr18
| Usage: When Renewal is created then set alt id to sibling application's record type prefix then the current year 
| then the application's license number and ending with "-REN" ie. AUH2018-1234567-REN
| Modified by: ()
*********************************************************/
function Avo_SetRenewalID() {
    logDebug("ID2 Avo_SetRenewalID()");

    // Get SubType
    var cap = aa.cap.getCap(capId).getOutput();
    var capTypeModel = cap.getCapType();
    var capSubType = capTypeModel.getSubType();
    Avo_LogDebug("Sub Type(" + capSubType + ")", 2);   //debug

    var category = "License";
    if (capSubType == "Alarm User") {
        category = "Permit";
    }

    // Get parent license
    var licCapId = parentCapId;
    if (!licCapId) {
        var allParents = aa.cap.getProjectByChildCapID(capId, "", "").getOutput();
        if (allParents.length == 0) {
            Avo_LogDebug("No parent records", 1);
            return;
        }

        for (i in allParents) {
            parentCapId = allParents[i].projectID;

            if (appMatch("Licenses/Business/" + capSubType + "/" + category, parentCapId) == false) {
                continue;
            }

            licCapId = parentCapId;
            break;
        }
    }

    // Get Sibling application
    var appCapId = getChildren("Licenses/Business/" + capSubType + "/Application", licCapId)[0];
    if (!appCapId) {
        Avo_LogDebug("No sibling app record matching amendment subtype " + capSubType, 1);
        return;
    }

    var appAltId = aa.cap.getCap(appCapId).getOutput().getCapModel().altID;
    const regex = /(\w{3})\d{4}-(\d{7})-APP/;

    var matches = regex.exec(appAltId)
    var prefix = matches[1];
    Avo_LogDebug("Prefix(" + prefix + ")", 2);   //debug

    var licNum = matches[2];
    Avo_LogDebug("Lic Num(" + licNum + ")", 2);   //debug

    var year = new Date().getFullYear();

    var renAltId = prefix + year + "-" + licNum + "-REN";
    Avo_LogDebug("Updated AltId(" + renAltId + ")", 2);  //debug

    result = aa.cap.updateCapAltID(capId, renAltId);
    if (!result.getSuccess()) {
        Avo_LogDebug("Failed to update renewal's alt id", 1);
    }
}