//ASA: Licenses/Business/Solid Waste/Renewal

//call script ** SWD02 ** script/functions based on requirements
Avo_AddSWDRenFee();

/*******************************************************
| Script/Function: Avo_AddSWDRenFee() - (SWD02)
| Created by: Nicolaj Bunting
| Created on: 11Jan18
| Usage: If ASI "Total # of Trucks" > 0 Then add and invoice fee "SWDREN" from schedule "LIC_SWD_REN" with quantity of total trucks
| Modified by: ()
*********************************************************/
function Avo_AddSWDRenFee() {
    logDebug("SWD02 Avo_AddSWDRenFee()");

    var totalTrucks = getAppSpecific("Total # of Trucks", capId);
    if (totalTrucks <= 0) {
        Avo_LogDebug("Total # of Trucks isn't a positive number", 1);
        return;
    }

    addFee("SWDREN", "LIC_SWD_REN", "FINAL", totalTrucks, "Y", capId);
    Avo_LogDebug("Added fee SWDREN from schedule LIC_SWD_REN with quantity " + String(totalTrucks), 1);
}