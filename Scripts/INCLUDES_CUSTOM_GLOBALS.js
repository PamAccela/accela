/*------------------------------------------------------------------------------------------------------/
| Program : INCLUDES_CUSTOM_GLOBALS.js
| Event   : N/A
|
| Usage   : Accela Custom Includes.  Required for all Custom Parameters
|
| Notes   : 
|
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| Custom Parameters
|	Ifchanges are made, please add notes above.
/------------------------------------------------------------------------------------------------------*/
feeEstimate = false;
if (vEventName.equals("FeeEstimateAfter4ACA")) {
    feeEstimate = true;
}

if (matches(currentUserID, "ADMIN")) {
    showDebug = true;
}

var br = "</br>";

// 0: no debug
// 1: minimal debugging
// 2: full debugging
var debugLevel = 2;
var useLogDebug = true;

/*------------------------------------------------------------------------------------------------------/
| END Custom Parameters
/------------------------------------------------------------------------------------------------------*/