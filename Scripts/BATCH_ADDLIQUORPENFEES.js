/*******************************************************
| Script Title: Batch_AddLiquorPenFees (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 10Jan18
| Usage: For all "Licenses/Business/Liquor/Application" records if date assessed of permit fee is 31 days from today 
| and balance > 0 then add fee "LIQPEN" from schedule "LIC_LIQ_LIC" with quantity of 10% of permit fee amount
| Modified by: ()
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
//aa.env.setValue("param1","Yes");
//aa.env.setValue("param1","No");
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var param1 = aa.env.getValue("param1");
//var param2 = aa.env.getValue("param2");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
//eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    aa.print("Batch Job " + batchJobName + " Job ID is " + batchJobID);
}
else {
    aa.print("Batch job ID not found " + batchJobResult.getErrorMessage());
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    aa.print("Start of Job");

    if (!timeExpired) {
        try {
            AddLiquorPenFees();
        }
        catch (e) {
            aa.print("Error in process " + e.message);
        }
    }
    else {
        aa.print("End of Job: Elapsed Time : " + elapsed() + " Seconds");
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

function AddLiquorPenFees() {
    printLine("LIQ06 AddLiquorPenFees()");

    var group = "Licenses";
    var type = "Business";
    var subType = "Liquor";
    var category = "Application";
    var appsArray = aa.cap.getByAppType(group, type, subType, category).getOutput();
    if (appsArray.length == 0) {
        printLine('No records of type "' + group + "/" + type + "/" + subType + "/" + category + '" found');
        return;
    }

    var thirtyOneDaysAgo = Avo_GetToday();
    thirtyOneDaysAgo.setDate(thirtyOneDaysAgo.getDate() - 31);
    printLine("31 days ago(" + aa.util.formatDate(thirtyOneDaysAgo, "MM/dd/yyyy") + ")");	//debug

    for (a in appsArray) {
        capId = appsArray[a].getCapID();
        var altId = capId.getCustomID();
        printLine("CapId(" + altId + ")");  //debug

        var result = aa.cap.getCapDetail(capId);
        if (!result.getSuccess()) {
            printLine("Unable to load record details for " + altId);
            continue;
        }

        var capDetails = result.getOutput();
        var balanceDue = capDetails.getBalance();
        printLine("Balance($" + balanceDue + ")");  //debug

        if (balanceDue <= 0) {
            continue;
        }

        var stateSeries = getAppSpecific("State Series #", capId);
        logDebug("#(" + stateSeries + ")"); //debug

        var prefix = String(stateSeries).substring(0, 2);
        if (isNaN(prefix) == true) {
            prefix = "0" + prefix.substring(0, 1);
        }

        var permitFeeCode = prefix;

        // Get permit fee assessment date and amount
        var amount = 0;
        var count = 0;
        var applyDate = null;

        var result = aa.fee.getFeeItems(capId);
        if (result.getSuccess() != true) {
            printLine("No fee items could be found on record " + altId);
            continue;
        }

        var allFeeItems = result.getOutput();
        for (i in allFeeItems) {
            var feeItem = allFeeItems[i];
            if (feeItem.feeCod != permitFeeCode) {
                continue;
            }

            count++;

            amount += feeAmount(permitFeeCode);
            printLine("Amount($" + amount + ")");	//debug

            applyDate = new Date(feeItem.applyDate.epochMilliseconds);

            // Ignore time component
            applyDate.setHours(0);
            applyDate.setMinutes(0);
            applyDate.setSeconds(0);
            applyDate.setMilliseconds(0);
            printLine("Apply Date(" + aa.util.formatDate(applyDate, "MM/dd/yyyy") + ")");	//debug
        }

        printLine("Count(" + count + ")");  //debug

        if (applyDate == null) {
            printLine("No permit fee of type " + permitFeeCode + " could be found on record " + altId);
            continue;
        }

        if (applyDate.getTime() != thirtyOneDaysAgo.getTime()) {
            continue;
        }

        // Check that fee hasn't already been added
        var quantity = feeQty("LIQPEN");
        printLine("Qty(" + quantity + ")");	//debug
        if (quantity > 0) {
            continue;
        }

        quantity = 0.1 * amount;

        addFee("LIQPEN", "LIC_LIQ_LIC", "FINAL", quantity, "Y", capId);
        printLine("Added fee LIQPEN to record " + altId + " with quantity " + String(quantity));
    }
}

function printLine(pStr) {
    aa.print(pStr + br);
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}