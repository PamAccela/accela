// Get Adapter config
var stdChoice = "SFTP_Adapter_Configs";

adapterUrl = lookup(stdChoice, "url");
adapterUsername = lookup(stdChoice, "username");
adapterPassword = lookup(stdChoice, "password");

docUrl = "/api/Documents";

function getToken(adapterUsername, adapterPassword) {
    Avo_LogDebug("getToken(" + adapterUsername + ", *****)", 1);

    var tokenUrl = "/Token";

    var params = aa.httpClient.initPostParameters();
    params.put("grant_type", "password");
    params.put("username", adapterUsername);
    params.put("Password", adapterPassword);

    // logDebug(Serialize(params, false));	//debug

    var result = aa.httpClient.post(adapterUrl + tokenUrl, params);
    if (!result.getSuccess()) {
        Avo_LogDebug("**ERROR: " + result.errorType + ":" + result.errorMessage, 1);
        return false;
    }

    var json = String(result.getOutput());
    //Avo_LogDebug("Json(" + json + ")", 2);  //debug

    if (!json) {
        Avo_LogDebug("Response is empty. Returning null", 1);
        return null;
    }

    // Check for 404 error
    if (json.indexOf('404') != -1) {
        Avo_LogDebug("**ERROR: Adapter not found", 1);
        return false;
    }

    // Check for server error
    if (json.indexOf('Server Error in ' / ' Application.') != -1) {
        Avo_LogDebug("**ERROR: server error in '/' application", 1);
        return false;
    }

    // Check for exceptions
    if (json.toLowerCase().indexOf('exception') != -1) {
        Avo_LogDebug("**ERROR: Security exception", 1);
        return false;
    }

    // Check for dangerous request error
    if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
        const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
        var invalidChars = regex.exec(json)[1];
        Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
        return false;
    }

    var response = JSON.parse(json);
    if ("error" in response) {
        Avo_LogDebug("**ERROR: unable to get token. " + response.error + ": " + response.error_description, 1);
        return false;
    }

    var adapterToken = response.access_token;
    if (!adapterToken || adapterToken.length <= 0) {
        Avo_LogDebug("**ERROR: unable to get token", 1);
        return false;
    }

    // Avo_LogDebug("Token(" + adapterToken + ")", 2);	//debug
    return adapterToken;
}

function getList(sftpFolder) {
    Avo_LogDebug("getList(" + sftpFolder + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/List/" + sftpFolder);
    Avo_LogDebug("List(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (client.getSuccess() != true) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function getListOnHost(sftpUrl, sftpPort, sftpUsername, sftpPassword, sftpFolder) {
    Avo_LogDebug("getListOnHost(" + sftpUrl + ", " + sftpPort + ", " + sftpUsername + ", *****, " + sftpFolder + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!sftpUrl) {
        Avo_LogDebug("Invalid SFTP url provided", 1);
        return false;
    }
    if (sftpUrl.length == 0) {
        Avo_LogDebug("No SFTP url provided", 1);
        return false;
    }

    if (!sftpPort) {
        Avo_LogDebug("Invalid SFTP port provided", 1);
        return false;
    }
    if (isNaN(sftpPort) == true) {
        Avo_LogDebug("SFTP port is not a number", 1);
        return false;
    }

    if (!sftpUsername) {
        Avo_LogDebug("Invalid SFTP username provided", 1);
        return false;
    }
    if (sftpUsername.length == 0) {
        Avo_LogDebug("No SFTP username provided", 1);
        return false;
    }

    if (!sftpPassword) {
        Avo_LogDebug("Invalid SFTP password provided", 1);
        return false;
    }
    if (sftpPassword.length == 0) {
        Avo_LogDebug("No SFTP password provided", 1);
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/" + sftpUrl + "/" + String(sftpPort) + "/" + sftpUsername + "/" + sftpPassword + "/List/" + sftpFolder);
    Avo_LogDebug("List(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (client.getSuccess() != true) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function getData(sftpFolder, filename) {
    Avo_LogDebug("getData(" + sftpFolder + ", " + filename + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/Get/" + sftpFolder + filename);
    Avo_LogDebug("Get(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function getDataOnHost(sftpUrl, sftpPort, sftpUsername, sftpPassword, sftpFolder, filename) {
    Avo_LogDebug("getDataOnHost(" + sftpUrl + ", " + sftpPort + ", " + sftpUsername + ", *****, " + sftpFolder + ", " + filename + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!sftpUrl) {
        Avo_LogDebug("Invalid SFTP url provided", 1);
        return false;
    }
    if (sftpUrl.length == 0) {
        Avo_LogDebug("No SFTP url provided", 1);
        return false;
    }

    if (!sftpPort) {
        Avo_LogDebug("Invalid SFTP port provided", 1);
        return false;
    }
    if (isNaN(sftpPort) == true) {
        Avo_LogDebug("SFTP port is not a number", 1);
        return false;
    }

    if (!sftpUsername) {
        Avo_LogDebug("Invalid SFTP username provided", 1);
        return false;
    }
    if (sftpUsername.length == 0) {
        Avo_LogDebug("No SFTP username provided", 1);
        return false;
    }

    if (!sftpPassword) {
        Avo_LogDebug("Invalid SFTP password provided", 1);
        return false;
    }
    if (sftpPassword.length == 0) {
        Avo_LogDebug("No SFTP password provided", 1);
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/" + sftpUrl + "/" + String(sftpPort) + "/" + sftpUsername + "/" + sftpPassword + "/Get/" + sftpFolder + filename);
    Avo_LogDebug("Get(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function getDataAsBytes(sftpFolder, filename) {
    Avo_LogDebug("getDataAsBytes(" + sftpFolder + ", " + filename + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/Get/Bytes/" + sftpFolder + filename);
    Avo_LogDebug("Get(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function getDataAsBytesOnHost(sftpUrl, sftpPort, sftpUsername, sftpPassword, sftpFolder, filename) {
    Avo_LogDebug("getDataAsBytesOnHost(" + sftpUrl + ", " + sftpPort + ", " + sftpUsername + ", *****, " + sftpFolder + ", " + filename + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!sftpUrl) {
        Avo_LogDebug("Invalid SFTP url provided", 1);
        return false;
    }
    if (sftpUrl.length == 0) {
        Avo_LogDebug("No SFTP url provided", 1);
        return false;
    }

    if (!sftpPort) {
        Avo_LogDebug("Invalid SFTP port provided", 1);
        return false;
    }
    if (isNaN(sftpPort) == true) {
        Avo_LogDebug("SFTP port is not a number", 1);
        return false;
    }

    if (!sftpUsername) {
        Avo_LogDebug("Invalid SFTP username provided", 1);
        return false;
    }
    if (sftpUsername.length == 0) {
        Avo_LogDebug("No SFTP username provided", 1);
        return false;
    }

    if (!sftpPassword) {
        Avo_LogDebug("Invalid SFTP password provided", 1);
        return false;
    }
    if (sftpPassword.length == 0) {
        Avo_LogDebug("No SFTP password provided", 1);
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/" + sftpUrl + "/" + String(sftpPort) + "/" + sftpUsername + "/" + sftpPassword + "/Get/Bytes/" + sftpFolder + filename);
    Avo_LogDebug("Get(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function saveData(sftpFolder, filename, contents) {
    Avo_LogDebug("saveData(" + sftpFolder + ", " + filename + ", " + contents + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    var headers = aa.httpClient.initPostParameters();
    // headers.put("Content-Type", "text/plain; charset=utf-8");
    headers.put("Content-Type", "application/json; charset=utf-8");	//; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    // contents = '{"Body":"' + contents + '"}';
    var json = JSON.stringify(contents);

    var fullUrl = encodeURI(adapterUrl + docUrl + "/Post/" + sftpFolder + filename);
    Avo_LogDebug("Post(" + fullUrl + ")", 2);    //debug

    try {
        client = aa.httpClient.post(fullUrl, headers, json);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function saveDataOnHost(sftpUrl, sftpPort, sftpUsername, sftpPassword, sftpFolder, filename, contents) {
    Avo_LogDebug("saveDataOnHost(" + sftpUrl + ", " + sftpPort + ", " + sftpUsername + ", *****, " + sftpFolder + ", " + filename + ", " + contents + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!sftpUrl) {
        Avo_LogDebug("Invalid SFTP url provided", 1);
        return false;
    }
    if (sftpUrl.length == 0) {
        Avo_LogDebug("No SFTP url provided", 1);
        return false;
    }

    if (!sftpPort) {
        Avo_LogDebug("Invalid SFTP port provided", 1);
        return false;
    }
    if (isNaN(sftpPort) == true) {
        Avo_LogDebug("SFTP port is not a number", 1);
        return false;
    }

    if (!sftpUsername) {
        Avo_LogDebug("Invalid SFTP username provided", 1);
        return false;
    }
    if (sftpUsername.length == 0) {
        Avo_LogDebug("No SFTP username provided", 1);
        return false;
    }

    if (!sftpPassword) {
        Avo_LogDebug("Invalid SFTP password provided", 1);
        return false;
    }
    if (sftpPassword.length == 0) {
        Avo_LogDebug("No SFTP password provided", 1);
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    var headers = aa.httpClient.initPostParameters();
    // headers.put("Content-Type", "text/plain; charset=utf-8");
    headers.put("Content-Type", "application/json; charset=utf-8");	//; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    // contents = '{"Body":"' + contents + '"}';
    var json = JSON.stringify(contents);

    var fullUrl = encodeURI(adapterUrl + docUrl + "/" + sftpUrl + "/" + String(sftpPort) + "/" + sftpUsername + "/" + sftpPassword + "/Post/" + sftpFolder + filename);
    Avo_LogDebug("Post(" + fullUrl + ")", 2);    //debug

    try {
        client = aa.httpClient.post(fullUrl, headers, json);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function saveDataAsBytes(sftpFolder, filename) {
    Avo_LogDebug("saveDataAsBytes(" + sftpFolder + ", " + filename + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/Post/Bytes/" + sftpFolder + filename);
    Avo_LogDebug("Post(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.post(fullUrl, headers, "", filename);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function saveDataAsBytesOnHost(sftpUrl, sftpPort, sftpUsername, sftpPassword, sftpFolder, filename) {
    Avo_LogDebug("saveDataAsBytesOnHost(" + sftpUrl + ", " + sftpPort + ", " + sftpUsername + ", *****, " + sftpFolder + ", " + filename + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/" + sftpUrl + "/" + String(sftpPort) + "/" + sftpUsername + "/" + sftpPassword + "/Post/Bytes/" + sftpFolder + filename);
    Avo_LogDebug("Post(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.post(fullUrl, headers, "", filename);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function uploadData(sftpFolder, filename, altId, docGroup, docCategory, docDesc) {
    Avo_LogDebug("uploadData(" + sftpFolder + ", " + filename + ", " + altId + ", " + docGroup + ", " + docCategory + ", " + docDesc + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    if (!docGroup) {
        Avo_LogDebug("Invalid document group provided", 1);
        return false;
    }
    if (docGroup.length == 0) {
        Avo_LogDebug("No document group provided", 1);
        return false;
    }

    if (!docCategory) {
        Avo_LogDebug("Invalid document category provided", 1);
        return false;
    }
    if (docCategory.length == 0) {
        Avo_LogDebug("No document category provided", 1);
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (!altId) {
        Avo_LogDebug("Invalid record ID provided", 1);
        return false;
    }
    if (altId.length == 0) {
        Avo_LogDebug("No record ID provided", 1);
        return false;
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/Upload/" + docGroup + "/" + docCategory + "/" + docDesc + "/" + altId + "/" + sftpFolder + filename);
    Avo_LogDebug("Upload(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        // Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function uploadDataToEnv(sftpFolder, filename, env, altId, docGroup, docCategory, docDesc) {
    Avo_LogDebug("uploadData(" + sftpFolder + ", " + filename + ", " + env + ", " + altId + ", " + docGroup + ", " + docCategory + ", " + docDesc + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (sftpFolder && String(sftpFolder).length > 0) {
        sftpFolder += "/";
    } else {
        sftpFolder = "";
    }

    if (!env) {
        Avo_LogDebug("Invalid environment provided", 1);
        return false;
    }
    if (env.length == 0) {
        Avo_LogDebug("No environment provided", 1);
        return false;
    }

    if (!docGroup) {
        Avo_LogDebug("Invalid document group provided", 1);
        return false;
    }
    if (docGroup.length == 0) {
        Avo_LogDebug("No document group provided", 1);
        return false;
    }

    if (!docCategory) {
        Avo_LogDebug("Invalid document category provided", 1);
        return false;
    }
    if (docCategory.length == 0) {
        Avo_LogDebug("No document category provided", 1);
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (!altId) {
        Avo_LogDebug("Invalid record ID provided", 1);
        return false;
    }
    if (altId.length == 0) {
        Avo_LogDebug("No record ID provided", 1);
        return false;
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/Upload/" + env + "/" + docGroup + "/" + docCategory + "/" + docDesc + "/" + altId + "/" + sftpFolder + filename);
    Avo_LogDebug("Upload(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        // Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}