/*
 * Add a the Application Fee when the status is Pending.
 * 
 * 
 */
 
if (currentUserID == "ADMIN") showDebug = true;
logDebug("WTUA:/// Add Application Fee");

if ((wfTask == "Application Submittal") && (wfStatus == "Pending")){
	logDebug("Adding Fee LIC_020");
	addFee("LIC_020","LIC_BUSINESS_GENERAL","FINAL",1000,"N");
}