//ASA: Licenses/Business/Liquor Special Event/Application

//call script ** LQS01 ** script/functions based on requirements
Avo_LiquorEventLicenseFee();

/*******************************************************
| Script/Function: Avo_LiquorEventLicenseFee() - (LQS01)
| Created by: Nicolaj Bunting
| Created on: 10Jan18
| Usage: Update and invoice fee "LQSLIC" from schedule "LIC_LQS_LIC" with quanitity of the number of days between ASI "Event End Date" and "Event Start Date"
| Modified by: ()
*********************************************************/
function Avo_LiquorEventLicenseFee() {
    logDebug("LQS01 Avo_LiquorEventLicenseFee()");

    var start = getAppSpecific("Event Start Date", capId);
    if (!start) {
        return;
    }

    var startDate = Avo_GetDateFromAccelaDateString(start);
    Avo_LogDebug("Start Date(" + aa.util.formatDate(startDate, "MM/dd/yyyy") + ")", 2);    //debug

    var end = getAppSpecific("Event End Date", capId);
    if (!end) {
        return;
    }

    var endDate = Avo_GetDateFromAccelaDateString(end);
    Avo_LogDebug("End Date(" + aa.util.formatDate(endDate, "MM/dd/yyyy") + ")", 2);    //debug

    var timeDiff = endDate.getTime() - startDate.getTime();
    var totalDays = Math.round(timeDiff / (24 * 60 * 60 * 1000)) + 1;
    Avo_LogDebug("Days(" + totalDays + ")", 2);    //debug

    updateFee("LQSLIC", "LIC_LQS_LIC", "FINAL", totalDays, "Y");
    Avo_LogDebug("Updated fee LQSLIC quantity to " + totalDays.toString(), 1);
}