//ASA: Licenses/Business/Teletrack Operator/Application

//call script ** TTO01 ** script/functions based on requirements
Avo_AddTTOMachineFee();

/*******************************************************
| Script/Function: Avo_AddTTOMachineFee() - (SWD01)
| Created by: Nicolaj Bunting
| Created on: 18Jan18
| Usage: If ASI "# of Machines" > 4 then add and invoice fee "TTOMCH" from schedule "LIC_TTO_APP" with quantity of number of machines - 4
| Modified by: ()
*********************************************************/
function Avo_AddTTOMachineFee() {
    logDebug("SWD01 Avo_AddSWDLicFee()");

    var totalMachines = getAppSpecific("# of Machines", capId);
    if (!totalMachines) {
        return;
    }

    totalMachines = parseInt(totalMachines, 10);
    if (isNaN(totalMachines) == true) {
        return;
    }

    if (totalMachines <= 4) {
        return;
    }

    addFee("TTOMCH", "LIC_TTO_APP", "FINAL", totalMachines - 4, "Y", capId);
    logDebug("Added and invoiced fee TTOMCH from schedule LIC_TTO_APP with quantity " + String(totalMachines - 4));
}