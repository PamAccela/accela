//ASA: Licenses/Business/Solid Waste/Application

//call script ** SWD01 ** script/functions based on requirements
Avo_AddSWDLicFee();

/*******************************************************
| Script/Function: Avo_AddSWDLicFee() - (SWD01)
| Created by: Nicolaj Bunting
| Created on: 11Jan18
| Usage: Add and invoice fee "SWDLIC" from schedule "LIC_SWD_LIC" with quantity of quarter percentage using ASI "Business Start Date" and ASI "Total # of Trucks"
| Modified by: ()
*********************************************************/
function Avo_AddSWDLicFee() {
    logDebug("SWD01 Avo_AddSWDLicFee()");

    var busStart = getAppSpecific("Business Start Date", capId);
    if (!busStart) {
        logDebug("Business start date not set");
        return;
    }

    var totalTrucks = getAppSpecific("Total # of Trucks", capId);
    if (totalTrucks <= 0) {
        logDebug("Total # of Trucks isn't a positive number");
        return;
    }

    var percentage = Avo_GetQuarterPercentage(busStart);

    addFee("SWDLIC", "LIC_SWD_LIC", "FINAL", totalTrucks * percentage, "Y", capId);
    logDebug("Added and invoiced fee SWDLIC from schedule LIC_SWD_LIC with quantity " + String(totalTrucks * percentage));
}