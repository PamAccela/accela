/*******************************************************
| Script/Function: CopyAddressToContact() - (ID40)
| Created by: Nicolaj Bunting
| Created on: 20Sep18
| Usage: Set the "Business", "Alarm User", or "Applicant" contact to be primary contact by default in ACA
| Modified by: ()
*********************************************************/
(function SetPrimaryContact() {
    Avo_LogDebug("ID40 SetPrimaryContact()", 1);

    //if (publicUser != true) {
    //    return;
    //}

    var result = aa.people.getCapContactByCapID(capId);
    if (result.getSuccess() != true) {
        Avo_LogDebug("No contacts on record", 1);
        return;
    }

    var allContacts = result.getOutput();
    for (i in allContacts) {
        var contact = allContacts[i];

        var contactModel = contact.capContactModel;

        var contactName = contactModel.contactName;
        Avo_LogDebug("Contact Name(" + contactName + ")", 2);	//debug

        var contactType = contactModel.contactType;
        Avo_LogDebug("Contact Type(" + contactType + ")", 2);	//debug

        if (contactType != "Business" && contactType != "Alarm User" && contactType != "Applicant") {
            continue;
        }

        // var contactTypeFlag = contactModel.contactTypeFlag;

        var primaryFlag = contactModel.primaryFlag;
        contactModel.primaryFlag = "Y";

        var result = aa.people.editCapContact(contactModel);
        if (result.getSuccess() != true) {
            Avo_LogDebug("Failed to make contact " + contactName + " primary." + result.errorMessage, 1);
            continue;
        }

        Avo_LogDebug("Set contact " + contactName + " to primary", 1);
        break;
    }
})();