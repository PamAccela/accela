/*******************************************************
| Script Title: Batch_ExpireLicenses (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 23Mar18
| Usage: For all "Licenses/Business/NA/License" records If expiration status is "About to Expire" and expiration date + X days <= today 
| Then set expiration status to "Expired", close task "License" with status "Closed: Expired" And if associated renewal 
| then close task "Renewal" with status "Closed: Non Renewal" and credit any unpaid fees
| Subtype                           X Days
| Adult Service Provider            7
| After Hours Establishment         0
| Alarm User                        120
| Auction House                     7
| Auctioneer                        7
| Charitable Solicitations          0  *if ASI "Activity Type" is "Charitable Solicitation"
| Close Out Sales                   0
| Escort Bureau                     7
| Escort                            7
| Liquor Special Event              0
| Magic Arts                        7
| Massage Facility                  7
| Neighborhood Street Vendor        7
| Promoter                          0
| Recycling                         7
| Secondhand                        7
| Sexually Oriented Business Mgr    7
| Sexually Oriented Business        7
| Solicitor For Profit              0
| Solid Waste                       7
| Teen Dance Center                 7
| Teletrack Operator                7
| Teletrack Establishment           7
| Valet Parking Special Event       0
| Valet Parking                     7
| Modified by: ()
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
//aa.env.setValue("param1","Yes");
//aa.env.setValue("param1","No");
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var param1 = aa.env.getValue("param1");
//var param2 = aa.env.getValue("param2");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
//eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    aa.print("Batch Job " + batchJobName + " Job ID is " + batchJobID);
}
else {
    aa.print("Batch job ID not found " + batchJobResult.getErrorMessage());
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    aa.print("Start of Job");

    if (!timeExpired) {
        try {
            CreateRenewals();
        }
        catch (e) {
            aa.print("Error in process " + e.message);
        }
    }
    else {
        aa.print("End of Job: Elapsed Time : " + elapsed() + " Seconds");
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

function CreateRenewals() {
    printLine("ID73 CreateRenewals()");

    var group = "Licenses";
    var type = "Business";
    var subType = null;
    var category = "License";
    var appsArray = aa.cap.getByAppType(group, type, subType, category).getOutput();
    if (appsArray.length == 0) {
        if (!group) {
            group = "*";
        }
        if (!type) {
            type = "*";
        }
        if (!subType) {
            subType = "*";
        }
        if (!category) {
            category = "*";
        }

        printLine('No records of type "' + group + "/" + type + '/' + subType + '/' + category + '" found');
        return;
    }

    var today = new Date();

    for (a in appsArray) {
        capId = appsArray[a].getCapID();
        var altId = capId.getCustomID();
        printLine("License CapId(" + altId + ")");  //debug

        // Get Subtype
        var cap = aa.cap.getCap(capId).getOutput();
        var capTypeModel = cap.getCapType();
        subType = String(capTypeModel.getSubType());
        printLine("Subtype(" + subType + ")");    //debug
        //logDebug("typeof Subtype(" + typeof (capSubType) + ")");    //debug

        var gracePeriod = null;
        switch (subType) {
            case "Adult Service Provider":
            case "Auction House":
            case "Auctioneer":
            case "Escort Bureau":
            case "Escort":
            case "Magic Arts":
            case "Massage Facility":
            case "Neighborhood Street Vendor":
            case "Recycling":
            case "Secondhand":
            case "Sexually Oriented Business Mgr":
            case "Sexually Oriented Business":
            case "Solid Waste":
            case "Teen Dance Center":
            case "Teletrack Operator":
            case "Teletrack Establishment":
                printLine("7 day grace");
                gracePeriod = 7;
                break;

            case "Alarm User":
                printLine("120 day grace");
                gracePeriod = 120;
                break;

            case "Charitable Solicitations":
                var activityType = getAppSpecific("Activity Type", capId);
                printLine("Activity Type(" + activityType + ")");   //debug

                if (activityType != "Charitable Solicitation") {
                    break;
                }
            case "After Hours Establishment":
            case "Close Out Sales":
            case "Liquor Special Event":
            case "Promoter":
            case "Solicitor For Profit":
            case "Valet Parking Special Event":
            case "Valet Parking":
                printLine("0 day grace");
                gracePeriod = 0;
                break;
        }

        if (gracePeriod == null) {
            printLine("No expiration rules for " + subType + " licenses");
            continue;
        }

        var graceEndDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() - gracePeriod);
        printLine("Grace period end date(" + aa.util.formatDate(graceEndDate, "MM/dd/yyyy") + ")");   //debug

        // Get expiration date
        var b1ExpResults = aa.expiration.getLicensesByCapID(capId);
        if (b1ExpResults.getSuccess() != true) {
            printLine("No expiration info for record " + altId);
            continue;
        }

        var b1Exp = b1ExpResults.getOutput();
        var b1ExpModel = b1Exp.getB1Expiration();
        if (!b1ExpModel) {
            printLine("Unable to load b1ExpModel for record " + altId);
            continue;
        }

        var expStatus = b1ExpModel.getExpStatus();
        printLine("Exp status(" + expStatus + ")"); //debug

        if (expStatus != "About to Expire") {
            continue;
        }

        var expiration = b1ExpModel.getExpDate();
        var expDate = new Date(expiration.time);
        printLine("Exp date(" + aa.util.formatDate(expDate, "MM/dd/yyyy") + ")");   //debug

        if (expDate.getTime() > graceEndDate.getTime()) {
            continue;
        }

        // Update expiration status
        b1ExpModel.setExpStatus("Expired");
        var b1EditResult = aa.expiration.editB1Expiration(b1ExpModel);
        if (b1EditResult.getSuccess() == true) {
            printLine("Set expiration status on record " + altId + ' to "Expired"');
        } else {
            printLine("Failed to update expiration status on record " + altId);
        }

        // Update workflow
        closeTask("License", "Closed: Expired", "Updated by Batch_ExpireLicenses", "");
        printLine('Set task "License" to status of "Closed: Expired"');

        // Get associated renewal
        var result = aa.cap.getProjectByMasterID(capId, "Renewal", null);
        if (result.getSuccess() != true) {
            printLine("No renewals associated with license " + altId);
            continue;
        }

        var licCapId = capId;

        var allRenewals = result.getOutput();
        for (a in allRenewals) {
            var renewalCapId = allRenewals[a].getCapID();
            capId = renewalCapId;

            var renewalStatus = allRenewals[a].status;
            if (renewalStatus == "Complete") {
                continue;
            }

            var renewalAltId = aa.cap.getCap(renewalCapId).getOutput().getCapModel().altID;
            printLine("Renewal CapId(" + renewalAltId + ")");   //debug

            // Update renewal workflow
            closeTask("Renewal", "Closed: Non Renewal", "Updated by Batch_ExpireLicenses", "");
            printLine('Set task "Renewal" to status of "Closed: Non Renewal"');

            // Get unpaid invoices
            var unpaidInvoices = new Array();
            var result = aa.finance.getInvoiceByCapID(renewalCapId, aa.util.newQueryFormat());
            if (result.getSuccess()) {
                var allInvoices = result.getOutput();
                for (i in allInvoices) {
                    var invoiceModel = allInvoices[i].invoiceModel;

                    var invoiceNum = invoiceModel.invNbr;
                    printLine("Invoice(" + invoiceNum + ")");	//debug

                    var amount = invoiceModel.invAmount;
                    printLine("Amount($" + amount + ")");	//debug

                    var balance = invoiceModel.balanceDue;
                    printLine("Balance($" + balance + ")");	//debug

                    if (balance != amount) {
                        continue;
                    }

                    unpaidInvoices.push(invoiceNum);
                }
            }

            // Remove unpaid, uninvoiced fees
            var feeItems = aa.fee.getFeeItems(renewalCapId).getOutput();
            for (i in feeItems) {
                var feeItem = feeItems[i];

                var feeCode = feeItem.feeCod;
                printLine("Code(" + feeCode + ")");	//debug

                var status = feeItem.feeitemStatus;
                if (status == "NEW") {
                    var feeSched = feeItem.f4FeeItemModel.feeSchudle;
                    printLine("Schedule(" + feeSched + ")");	//debug

                    var feePeriod = feeItem.paymentPeriod;
                    printLine("Period(" + feePeriod + ")");	//debug

                    var quantity = feeItem.feeUnit;
                    printLine("Quantity(" + quantity + ")");	//debug

                    var totalQuantity = parseInt(feeQty(feeCode), 10);
                    totalQuantity -= quantity;

                    if (totalQuantity == 0) {
                        removeFee(feeCode, feePeriod);
                    } else {
                        var feeResult = updateFee(feeCode, feeSched, feePeriod, totalQuantity, "N");
                        if (feeResult == null) {
                            printLine("Fee " + feeCode + " has been decremented on renewal " + renewalAltId);
                        } else {
                            printLine("Failed to update the fee " + feeCode + " on renewal " + renewalAltId);
                        }
                    }

                    continue;
                }

                if (unpaidInvoices.length == 0) {
                    continue;
                }

                if (status != "INVOICED") {
                    continue;
                }

                // Check that fee is on unpaid invoice
                var feeId = feeItem.feeSeqNbr;
                printLine("Seq Num(" + feeId + ")");	//debug

                var creditFee = false;

                result = aa.finance.getFeeItemInvoiceByFeeNbr(renewalCapId, feeId, aa.util.newQueryFormat());
                if (result.getSuccess() == false) {
                    printLine("Unable to load invoice information for fee " + feeCode + " on renewal " + renewalAltId);
                    continue;
                }

                var feeItemInvoice = result.getOutput()[0];

                var invoiceNumToCheck = feeItemInvoice.invoiceNbr;
                printLine("Invoice # to Check(" + invoiceNumToCheck + ")");	//debug

                for (j in unpaidInvoices) {
                    var invoiceNum = unpaidInvoices[j];
                    printLine("Invoice #(" + invoiceNum + ")");	//debug

                    if (invoiceNum != invoiceNumToCheck) {
                        continue;
                    }

                    printLine("Match"); //debug
                    creditFee = true;
                    break;
                }

                if (creditFee == false) {
                    continue;
                }

                // Credit invoiced fee
                var feeItemModel = feeItem.f4FeeItemModel;
                feeItemModel.feeitemStatus = "CREDITED";

                var result = aa.finance.editFeeItem(feeItemModel);
                if (result.getSuccess() == true) {
                    printLine("Credited fee " + feeCode + ' on invoice ' + invoiceNumToCheck + ' on renewal ' + renewalAltId);
                } else {
                    printLine("Failed to credit fee " + feeCode + " on invoice " + invoiceNumToCheck + " on renewal " + renewalAltId + ". " + result.getErrorMessage());
                }
            }
        }
    }
}

function printLine(pStr) {
    aa.print(pStr + br);
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}