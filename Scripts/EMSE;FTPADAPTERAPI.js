// Get Adapter config
var stdChoice = "FTP_Adapter_Configs";

adapterUrl = lookup(stdChoice, "url");
adapterUsername = lookup(stdChoice, "username");
adapterPassword = lookup(stdChoice, "password");

docUrl = "/api/Documents";

function getToken(adapterUsername, adapterPassword) {
    Avo_LogDebug("getToken(" + adapterUsername + ", *****)", 1);

    var tokenUrl = "/Token";

    var params = aa.httpClient.initPostParameters();
    params.put("grant_type", "password");
    params.put("username", adapterUsername);
    params.put("Password", adapterPassword);

    var result = aa.httpClient.post(adapterUrl + tokenUrl, params);
    if (!result.getSuccess()) {
        Avo_LogDebug("**ERROR: " + result.errorType + ":" + result.errorMessage, 1);
        return false;
    }

    var json = String(result.getOutput());
    Avo_LogDebug("Json(" + json + ")", 2);  //debug

    if (!json) {
        Avo_LogDebug("Response is empty. Returning null", 1);
        return null;
    }

    // Check for 404 error
    if (json.indexOf('404') != -1) {
        Avo_LogDebug("**ERROR: Adapter not found", 1);
        return false;
    }

    // Check for server error
    if (json.indexOf('Server Error in ' / ' Application.') != -1) {
        Avo_LogDebug("**ERROR: server error in '/' application", 1);
        return false;
    }

    // Check for exceptions
    if (json.toLowerCase().indexOf('exception') != -1) {
        Avo_LogDebug("**ERROR: Security exception", 1);
        return false;
    }

    // Check for dangerous request error
    if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
        const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
        var invalidChars = regex.exec(json)[1];
        Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
        return false;
    }

    var response = JSON.parse(json);
    if ("error" in response) {
        Avo_LogDebug("**ERROR: unable to get token. " + response.error + " " + response.error_description, 1);
        return false;
    }

    var adapterToken = response.access_token;
    if (!adapterToken || adapterToken.length <= 0) {
        Avo_LogDebug("**ERROR: unable to get token", 1);
        return false;
    }

    // Avo_LogDebug("Token(" + adapterToken + ")", 2);	//debug
    return adapterToken;
}

function getList(ftpFolder) {
    Avo_LogDebug("getList(" + ftpFolder + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (ftpFolder && String(ftpFolder).length > 0) {
        ftpFolder += "/";
    } else {
        ftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/List/" + ftpFolder);
    Avo_LogDebug("List(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (client.getSuccess() != true) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function getListOnHost(ftpUrl, ftpPort, ftpUsername, ftpPassword, ftpFolder) {
    Avo_LogDebug("getListOnHost(" + ftpUrl + ", " + ftpPort + ", " + ftpUsername + ", *****, " + ftpFolder + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!ftpUrl) {
        Avo_LogDebug("Invalid FTP url provided", 1);
        return false;
    }
    if (ftpUrl.length == 0) {
        Avo_LogDebug("No FTP url provided", 1);
        return false;
    }

    if (!ftpPort) {
        Avo_LogDebug("Invalid FTP port provided", 1);
        return false;
    }
    if (isNaN(ftpPort) == true) {
        Avo_LogDebug("FTP port is not a number", 1);
        return false;
    }

    if (!ftpUsername) {
        Avo_LogDebug("Invalid FTP username provided", 1);
        return false;
    }
    if (ftpUsername.length == 0) {
        Avo_LogDebug("No FTP username provided", 1);
        return false;
    }

    if (!ftpPassword) {
        Avo_LogDebug("Invalid FTP password provided", 1);
        return false;
    }
    if (ftpPassword.length == 0) {
        Avo_LogDebug("No FTP password provided", 1);
        return false;
    }

    if (ftpFolder && String(ftpFolder).length > 0) {
        ftpFolder += "/";
    } else {
        ftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/" + ftpUrl + "/" + String(ftpPort) + "/" + ftpUsername + "/" + ftpPassword + "/List/" + ftpFolder);
    Avo_LogDebug("List(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (client.getSuccess() != true) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function getData(ftpFolder, filename) {
    Avo_LogDebug("getData(" + ftpFolder + ", " + filename + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (ftpFolder && String(ftpFolder).length > 0) {
        ftpFolder += "/";
    } else {
        ftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/Get/" + ftpFolder + filename);
    Avo_LogDebug("Get(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function getDataOnHost(ftpUrl, ftpPort, ftpUsername, ftpPassword, ftpFolder, filename) {
    Avo_LogDebug("getDataOnHost(" + ftpUrl + ", " + ftpPort + ", " + ftpUsername + ", *****, " + ftpFolder + ", " + filename + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!ftpUrl) {
        Avo_LogDebug("Invalid FTP url provided", 1);
        return false;
    }
    if (ftpUrl.length == 0) {
        Avo_LogDebug("No FTP url provided", 1);
        return false;
    }

    if (!ftpPort) {
        Avo_LogDebug("Invalid FTP port provided", 1);
        return false;
    }
    if (isNaN(ftpPort) == true) {
        Avo_LogDebug("FTP port is not a number", 1);
        return false;
    }

    if (!ftpUsername) {
        Avo_LogDebug("Invalid FTP username provided", 1);
        return false;
    }
    if (ftpUsername.length == 0) {
        Avo_LogDebug("No FTP username provided", 1);
        return false;
    }

    if (!ftpPassword) {
        Avo_LogDebug("Invalid FTP password provided", 1);
        return false;
    }
    if (ftpPassword.length == 0) {
        Avo_LogDebug("No FTP password provided", 1);
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (ftpFolder && String(ftpFolder).length > 0) {
        ftpFolder += "/";
    } else {
        ftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/" + ftpUrl + "/" + String(ftpPort) + "/" + ftpUsername + "/" + ftpPassword + "/Get/" + ftpFolder + filename);
    Avo_LogDebug("Get(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function getDataAsBytes(ftpFolder, filename) {
    Avo_LogDebug("getDataAsBytes(" + ftpFolder + ", " + filename + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (ftpFolder && String(ftpFolder).length > 0) {
        ftpFolder += "/";
    } else {
        ftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/Get/Bytes/" + ftpFolder + filename);
    Avo_LogDebug("Get(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function getDataAsBytesOnHost(ftpUrl, ftpPort, ftpUsername, ftpPassword, ftpFolder, filename) {
    Avo_LogDebug("getDataAsBytesOnHost(" + ftpUrl + ", " + ftpPort + ", " + ftpUsername + ", *****, " + ftpFolder + ", " + filename + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (!ftpUrl) {
        Avo_LogDebug("Invalid FTP url provided", 1);
        return false;
    }
    if (ftpUrl.length == 0) {
        Avo_LogDebug("No FTP url provided", 1);
        return false;
    }

    if (!ftpPort) {
        Avo_LogDebug("Invalid FTP port provided", 1);
        return false;
    }
    if (isNaN(ftpPort) == true) {
        Avo_LogDebug("FTP port is not a number", 1);
        return false;
    }

    if (!ftpUsername) {
        Avo_LogDebug("Invalid FTP username provided", 1);
        return false;
    }
    if (ftpUsername.length == 0) {
        Avo_LogDebug("No FTP username provided", 1);
        return false;
    }

    if (!ftpPassword) {
        Avo_LogDebug("Invalid FTP password provided", 1);
        return false;
    }
    if (ftpPassword.length == 0) {
        Avo_LogDebug("No FTP password provided", 1);
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (ftpFolder && String(ftpFolder).length > 0) {
        ftpFolder += "/";
    } else {
        ftpFolder = "";
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/" + ftpUrl + "/" + String(ftpPort) + "/" + ftpUsername + "/" + ftpPassword + "/Get/Bytes/" + ftpFolder + filename);
    Avo_LogDebug("Get(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        //Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function saveDataOnHost(ftpUrl, ftpPort, ftpUsername, ftpPassword, ftpFolder, filename) {
    Avo_LogDebug("saveDataOnHost(" + ftpUrl + ", " + ftpPort + ", " + ftpUsername + ", *****, " + ftpFolder + ", " + filename + ")", 1);

    if (!ftpUrl) {
        Avo_LogDebug("Invalid FTP url provided", 1);
        return false;
    }
    if (ftpUrl.length == 0) {
        Avo_LogDebug("No FTP url provided", 1);
        return false;
    }

    if (!ftpPort) {
        Avo_LogDebug("Invalid FTP port provided", 1);
        return false;
    }
    if (isNaN(ftpPort) == true) {
        Avo_LogDebug("FTP port is not a number", 1);
        return false;
    }

    if (!ftpUsername) {
        Avo_LogDebug("Invalid FTP username provided", 1);
        return false;
    }
    if (ftpUsername.length == 0) {
        Avo_LogDebug("No FTP username provided", 1);
        return false;
    }

    if (!ftpPassword) {
        Avo_LogDebug("Invalid FTP password provided", 1);
        return false;
    }
    if (ftpPassword.length == 0) {
        Avo_LogDebug("No FTP password provided", 1);
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    var path = "C://Temp/";
    var file = null;

    try {
        file = aa.util.writeToFile(exportData, path + filename);
    } catch (ex) {
        Avo_LogDebug("Failed to create export file. " + ex.message, 1);
        return false;
    }

    if (!file) {
        Avo_LogDebug("Unable to create export file", 1);
        return false;
    }

    // Upload file to FTP
    var result = aa.util.ftpUpload(ftpUrl, ftpPort, ftpUsername, ftpPassword, ftpFolder, file);
    if (result.getSuccess()) {
        Avo_LogDebug("Uploaded " + filename + " to " + ftpUrl + ":" + ftpPort.toString() + " in directory " + ftpFolder, 1);
        return true;
    } else {
        Avo_LogDebug("Failed to upload " + filename + " to " + ftpUrl + ":" + ftpPort.toString() + ". " + result.errorMessage, 1);
        return false;
    }
}

function uploadData(ftpFolder, filename, altId, docGroup, docCategory, docDesc) {
    Avo_LogDebug("uploadData(" + ftpFolder + ", " + filename + ", " + altId + ", " + docGroup + ", " + docCategory + ", " + docDesc + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (ftpFolder && String(ftpFolder).length > 0) {
        ftpFolder += "/";
    } else {
        ftpFolder = "";
    }

    if (!docGroup) {
        Avo_LogDebug("Invalid document group provided", 1);
        return false;
    }
    if (docGroup.length == 0) {
        Avo_LogDebug("No document group provided", 1);
        return false;
    }

    if (!docCategory) {
        Avo_LogDebug("Invalid document category provided", 1);
        return false;
    }
    if (docCategory.length == 0) {
        Avo_LogDebug("No document category provided", 1);
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (!altId) {
        Avo_LogDebug("Invalid record ID provided", 1);
        return false;
    }
    if (altId.length == 0) {
        Avo_LogDebug("No record ID provided", 1);
        return false;
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/Upload/" + docGroup + "/" + docCategory + "/" + docDesc + "/" + altId + "/" + ftpFolder + filename);
    Avo_LogDebug("Upload(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        // Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}

function uploadDataToEnv(ftpFolder, filename, env, altId, docGroup, docCategory, docDesc) {
    Avo_LogDebug("uploadDataToEnv(" + ftpFolder + ", " + filename + ", " + env + ", " + altId + ", " + docGroup + ", " + docCategory + ", " + docDesc + ")", 1);

    if (!adapterToken) {
        return false;
    }

    if (ftpFolder && String(ftpFolder).length > 0) {
        ftpFolder += "/";
    } else {
        ftpFolder = "";
    }

    if (!env) {
        Avo_LogDebug("Invalid environment provided", 1);
        return false;
    }
    if (env.length == 0) {
        Avo_LogDebug("No environment provided", 1);
        return false;
    }

    if (!docGroup) {
        Avo_LogDebug("Invalid document group provided", 1);
        return false;
    }
    if (docGroup.length == 0) {
        Avo_LogDebug("No document group provided", 1);
        return false;
    }

    if (!docCategory) {
        Avo_LogDebug("Invalid document category provided", 1);
        return false;
    }
    if (docCategory.length == 0) {
        Avo_LogDebug("No document category provided", 1);
        return false;
    }

    if (!filename) {
        Avo_LogDebug("Invalid filename provided", 1);
        return false;
    }
    if (filename.length == 0) {
        Avo_LogDebug("No filename provided", 1);
        return false;
    }

    if (!altId) {
        Avo_LogDebug("Invalid record ID provided", 1);
        return false;
    }
    if (altId.length == 0) {
        Avo_LogDebug("No record ID provided", 1);
        return false;
    }

    var fullUrl = encodeURI(adapterUrl + docUrl + "/Upload/" + env + "/" + docGroup + "/" + docCategory + "/" + docDesc + "/" + altId + "/" + ftpFolder + filename);
    Avo_LogDebug("Upload(" + fullUrl + ")", 2);    //debug

    var headers = aa.httpClient.initPostParameters();
    headers.put("Content-Type", "application/json; charset=utf-8");
    headers.put("Authorization", "Bearer " + adapterToken);

    try {
        var client = aa.httpClient.get(fullUrl, headers);
        if (!client.getSuccess()) {
            var errorType = String(client.errorType);
            errorType = errorType.substring(errorType.lastIndexOf(".") + 1);

            Avo_LogDebug("**ERROR: " + errorType + ": " + client.errorMessage, 1);
            return false;
        }

        var json = String(client.getOutput());
        // Avo_LogDebug("Json(" + json + ")", 2);  //debug

        if (!json) {
            Avo_LogDebug("Response is empty. Returning null", 1);
            return null;
        }

        // Check for 404 error
        if (json.indexOf('<title>404 - File or directory not found.</title>') != -1) {
            Avo_LogDebug("**ERROR: File or directory not found", 1);
            return false;
        }

        // Check for bad request
        if (json.toLowerCase().indexOf('<TITLE>Bad Request</TITLE>'.toLowerCase()) != -1) {
            Avo_LogDebug("**ERROR: Bad request", 1);
            return false;
        }

        // Check for server error
        if (json.indexOf('Server Error in ' / ' Application.') != -1) {
            Avo_LogDebug("**ERROR: server error in '/' application", 1);
            return false;
        }

        // Check for dangerous request error
        if (json.indexOf('A potentially dangerous Request.Path value was detected from the client') != -1) {
            const regex = /<title>A potentially dangerous Request[.]Path value was detected from the client [(]([^)]+)[)][.]<\/title>/gm;
            var invalidChars = regex.exec(json)[1];
            Avo_LogDebug('**ERROR: invalid character "' + invalidChars + '" in request', 1);
            return false;
        }

        var response = JSON.parse(json);
        return response;
    } catch (ex) {
        Avo_LogDebug("**ERROR: " + ex.message, 1);
        return false;
    }
}