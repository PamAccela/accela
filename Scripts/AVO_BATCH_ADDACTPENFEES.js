/*******************************************************
| Script Title: Batch_AddActPenFees (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 20Apr18
| Usage: For all records of type "Licenses/Business/Alarm User/Permit" and for all invoiced ALUACT fees 
| If invoice date is 31 days prior to today and fee is unpaid Then add and invoice fee "ALUACTPEN"
| Modified by: ()
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
//aa.env.setValue("param1","Yes");
//aa.env.setValue("param1","No");
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var param1 = aa.env.getValue("param1");
//var param2 = aa.env.getValue("param2");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    aa.print("Batch Job " + batchJobName + " Job ID is " + batchJobID);
}
else {
    aa.print("Batch job ID not found " + batchJobResult.getErrorMessage());
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    aa.print("Start of Job");

    if (!timeExpired) {
        try {
            AddActPenFees();
        }
        catch (e) {
            aa.print("Error in process " + e.message);
        }
    }
    else {
        aa.print("End of Job: Elapsed Time : " + elapsed() + " Seconds");
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

function AddActPenFees() {
    Avo_BatchDebug("ALU03 AddActPenFees()", 1);

    // Get all Alarm user permits
    var group = "Licenses";
    var type = "Business";
    var subType = "Alarm User";
    var category = "Permit";
    var appsArray = aa.cap.getByAppType(group, type, subType, category).getOutput();
    if (appsArray.length == 0) {
        Avo_BatchDebug('No records of type "' + group + "/" + type + "/" + subType + "/" + category + '" found', 1);
        return;
    }

    var thirtyOneDaysAgo = Avo_GetToday();
    thirtyOneDaysAgo.setDate(thirtyOneDaysAgo.getDate() - 31);
    Avo_BatchDebug("31 Days Ago(" + aa.util.formatDate(thirtyOneDaysAgo, "MM/dd/yyyy") + ")", 2); //debug

    for (a in appsArray) {
        capId = appsArray[a].getCapID();
        var altId = aa.cap.getCap(capId).getOutput().getCapModel().altID;
        Avo_BatchDebug("CapId(" + altId + ")", 2);  //debug

        // Get all invoices
        var result = aa.finance.getInvoiceByCapID(capId, aa.util.newQueryFormat());
        if (result.getSuccess() == false) {
            Avo_BatchDebug("Unable to get invoices for permit " + altId + ". " + result.errorMessage, 1);
        }

        var allInvoices = result.getOutput();
        for (i in allInvoices) {
            var invoiceModel = allInvoices[i].invoiceModel;

            var invoiceNum = invoiceModel.invNbr;
            Avo_BatchDebug("Invoice#(" + invoiceNum + ")", 2);  //debug

            //var amount = invoiceModel.invAmount;
            var balance = invoiceModel.balanceDue;
            Avo_BatchDebug("Balance Due($" + balance + ")", 2); //debug

            if (balance <= 0) {
                continue;
            }

            var invoiceDateStr = invoiceModel.invDate;
            var invoiceDate = Avo_GetDateFromAccelaDateString(invoiceDateStr);
            Avo_BatchDebug("Invoice Date(" + aa.util.formatDate(invoiceDate, "MM/dd/yyyy") + ")", 2);   //debug

            if (invoiceDate.getTime() != thirtyOneDaysAgo.getTime()) {
                continue;
            }

            // Get all invoiced ALUACT fees by invoice num
            result = aa.finance.getFeeItemInvoiceByInvoiceNbr(capId, invoiceNum, aa.util.newQueryFormat());
            if (result.getSuccess() != true) {
                Avo_BatchDebug("Unable to get invoice fee items for permit " + altId + ". " + result.errorMessage, 1);
                continue;
            }

            var allFeeItems = result.getOutput();

            var qty = 0;
            for(j in allFeeItems) {
                var feeItem = allFeeItems[j];

                var feeCode = feeItem.feeCode;
                if(feeCode != "ALUACT") {
                    continue;
                }

                var feeSched = feeItem.feeSchedule;
                if(feeSched != "LIC_ALU_LIC") {
                    continue;
                }

                var feeInvoiceNum = feeItem.invoiceNbr;
                if(feeInvoiceNum != invoiceNum) {
                    continue;
                }

                var amount = feeItem.fee;
                if(amount == 0) {
                    continue;
                }

                var quantity = feeItem.unit;
                if(quantity == 0) {
                    continue;
                }

                qty++;
                Avo_BatchDebug("Fee Id(" + feeItem.feeSeqNbr + ")", 2); //debug
            }

            if(qty == 0) {
                continue;
            }

            addFee("ALUACTPEN", "LIC_ALU_LIC", "FINAL", qty, "Y", capId);
            Avo_BatchDebug("Added and invoiced fee ALUACTPEN from schedule LIC_ALU_LIC with quantity of " + String(qty) + " on permit " + altId, 1);
        }
    }
}

function Avo_BatchDebug(debug, importance) {
    if (isNaN(importance) == true) {
        aa.print("Importance must be an integer" + br);
        return;
    }

    if (importance < 1) {
        aa.print("Importance must be higher than 0" + br);
    }

    if (importance > debugLevel) {
        return;
    }

    aa.print(debug + br);
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}