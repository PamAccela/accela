/*******************************************************
| Script Title: Batch_ImportAlarmActivations (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 5Sep18
| Usage: Get alarm activations from SFTP Then for each row If record id included then use matching record Else find "Licenses/Business/Alarm User/Permit" 
| where task "License" is active and has status "Active" or "About to Expire" and expiration status isn't "Expired" If no valid permit found Then find
| "Licenses/Business/Alarm User/Application" record by address where records status isn't like "Closed:" Then add a row to record's ASIT "Activations", 
| run Avo_UpdateAlarmActFees, export record ID if not supplied
| Modified by: Nic Bunting (28Jan19)
| Modified by: Nic Bunting (3Jul19)
| Modified by: Nic Bunting (8Aug19)
| Modified by: Nic Bunting (22Aug19)
| Modified by: Nic Bunting (10Sep19)
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
//aa.env.setValue("sftpUrl", "172.19.61.35");
//aa.env.setValue("sftpPort", 21);
//aa.env.setValue("sftpFolder", "test");  //  Testing
//aa.env.setValue("sftpUsername", "AccelaFTP");
//aa.env.setValue("sftpPassword", "txfr1234!");
//aa.env.setValue("filename", "ACCELA_ALU_IMPORT_01112018.txt");  // Testing
//aa.env.setValue("adapterUrl", "http://172.19.61.35/AccelaFTPService");
//aa.env.setValue("adapterUsername", "AccelaFTPUser");
//aa.env.setValue("adapterPassword", "x1transfer1234$");
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var url = aa.env.getValue("sftpUrl");
//var port = aa.env.getValue("sftpPort");
var sftpFolder = aa.env.getValue("sftpFolder");
//var username = aa.env.getValue("sftpUsername");
//var password = aa.env.getValue("sftpPassword");
var filename = aa.env.getValue("filename");
//var adapterUrl = aa.env.getValue("adapterUrl");
//var adapterUsername = aa.env.getValue("adapterUsername");
//var adapterPassword = aa.env.getValue("adapterPassword");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

// Print debug using aa.print instead of aa.debug
useLogDebug = false;

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    Avo_LogDebug("Batch Job " + batchJobName + " Job ID is " + batchJobID, 1);
}
else {
    Avo_LogDebug("Batch job ID not found " + batchJobResult.getErrorMessage(), 1);
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    Avo_LogDebug("Start of Job", 1);

    if (!timeExpired) {
        try {
            importAlarmActivations(sftpFolder, filename);
        }
        catch (e) {
            Avo_LogDebug("Error in process " + e.message, 1);
        }
    }
    else {
        Avo_LogDebug("End of Job: Elapsed Time : " + elapsed() + " Seconds", 1);
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/
function importAlarmActivations(sftpFolder, impFilename) {
    Avo_LogDebug("importAlarmActivations(" + sftpFolder + ", " + impFilename + ")", 1);

    include("EMSE:SFTPADAPTERAPI");
    eval(getScriptText("EMSE:SFTPADAPTERAPI"));

    adapterToken = getToken(adapterUsername, adapterPassword);
    Avo_LogDebug("Token(" + adapterToken + ")", 2);	//debug

    if (!adapterToken) {
        Avo_LogDebug("Failed to get token from SFTP Adapter @ " + adapterUrl, 1);
        return;
    }

    var today = Avo_GetToday();

    impFilename = String(impFilename);
    Avo_LogDebug("Length(" + impFilename.length + ")", 2);  //debug

    if (!impFilename || impFilename.length == 0) {
        var month = String(parseInt(today.getMonth(), 10) + 1);
        if (month.length < 2) {
            month = "0" + month;
        }

        var day = String(today.getDate());
        if (day.length < 2) {
            day = "0" + day;
        }

        var year = String(today.getFullYear());

        impFilename = "ACCELA_ALU_IMPORT_" + month + day + year + ".txt";
        Avo_LogDebug("Filename(" + impFilename + ")", 2);   //debug
    }

    var response = getData(sftpFolder, impFilename);
    Avo_LogDebug("Response(" + response + ")", 2);  //debug

    if (!response) {
        Avo_LogDebug("Could not find file " + filename + " in folder " + sftpFolder, 1);
        return;
    }

    if ("ExceptionMessage" in response) {
        Avo_LogDebug('Failed to get file "' + filename + '" in folder "' + sftpFolder + '". ' + response["ExceptionMessage"], 1);
        Avo_LogDebug('"StackTrace":' + response["StackTrace"], 1);
        return;
    }

    var allActivations = processResponse(impFilename, response);
    if (!allActivations || allActivations.length == 0) {
        Avo_LogDebug("No activations found", 1);
        return;
    }

    var updatedActivationDetails = "";
    var updatedRecords = new Object();

    var tableName = "ACTIVATIONS";
    var alarmIdField = "Alarm ID";
    var alarmDateField = "Alarm Date";
    var dispositionField = "Disposition Code";
    var busNameField = "Business Name";
    var primaryUnitField = "Primary Unit";
    var callerNameField = "Caller Name";
    var callerNumField = "Caller Number";
    var policeFireField = "PoliceFire";
    var commentsField = "Comments";
    var alarmTimeField = "Alarm Time";
    var eventIdField = "Event ID";
    var methodField = "Method";
    var processedField = "Processed";
    var activeField = "Active?";
    var dateAddedField = "Date Added";

    var alarmIdIndex = 0;
    var alarmDateIndex = 1;
    var dispositionIndex = 2;
    //var incidentIndex = 3;
    var streetNumIndex = 4;
    var streetDirIndex = 5;
    var streetNameIndex = 6;
    var streetTypeIndex = 7;
    var unitNumIndex = 8;
    //var zipIndex = 10;
    //var cityIndex = 9;
    //var msLinkIndex = 11;
    var busNameIndex = 12;
    //var pointOfContactIndex = 13;
    var primaryUnitIndex = 14;
    //var officerNumIndex = 15;
    var callerNameIndex = 16;
    var callerNumIndex = 17;
    var policeFireIndex = 18;
    var commentsIndex = 19;
    var alarmPermitIndex = 20;
    //var bldgTypeIndex = 21;
    //var locationFullIndex = 22;
    //var xCoordIndex = 23;
    //var yCoordIndex = 24;
    //var exportDateIndex = 25;
    //var bannerExpDateIndex = 26;
    //var exportStatusIndex = 27;
    var alarmTimeIndex = 28;
    var eventIdIndex = 29;
    var recordIdIndex = 30;

    for (var i in allActivations) {
        var elapsedTime = elapsed();
        if (elapsedTime > timeOutInSeconds) { //only continue if time hasn't expired
            Avo_LogDebug("A script time-out has caused partial completion of this process.  Please re-run.  " + elapsedTime.toString() +
                " seconds elapsed, " + timeOutInSeconds + " allowed.");
            timeExpired = true;
            break;
        }

        var activation = allActivations[i];
        Avo_LogDebug(br + "Activation(" + activation + ")", 2);	//debug
        if (!activation) {
            continue;
        }

        var activationArr = activation.split("|");

        var alarmId = activationArr[alarmIdIndex];
        var alarmDateStr = activationArr[alarmDateIndex];

        // Convert to date
        alarmDateStr = alarmDateStr.substring(0, alarmDateStr.indexOf(" "));
        alarmDateStr = alarmDateStr.trim();
        alarmDateArr = alarmDateStr.split("-");
        var alarmDate = aa.util.formatDate(new Date(alarmDateArr[0], alarmDateArr[1] - 1, alarmDateArr[2]), "MM/dd/yyyy");
        Avo_LogDebug("Alarm Date(" + alarmDate + ")", 2); //debug

        var dispositionCode = activationArr[dispositionIndex];
        Avo_LogDebug("Disposition(" + dispositionCode + ")", 2);    //debug

        //var incidentId = activationArr[incidentIndex];
        var streetNum = activationArr[streetNumIndex];
        Avo_LogDebug("Street Num(" + streetNum + ")", 2);   //debug

        var streetDir = activationArr[streetDirIndex];
        Avo_LogDebug("Street Dir(" + streetDir + ")", 2);   //debug

        var streetName = activationArr[streetNameIndex];
        Avo_LogDebug("Street Name(" + streetName + ")", 2);   //debug

        var streetType = activationArr[streetTypeIndex];
        Avo_LogDebug("Street Type(" + streetType + ")", 2);   //debug

        var unitNum = activationArr[unitNumIndex];
        Avo_LogDebug("Unit Num(" + unitNum + ")", 2);   //debug

        //var zip = activationArr[zipIndex];
        //var city = activationArr[cityIndex];
        //var msLink = activationArr[msLinkIndex];
        var busName = activationArr[busNameIndex];

        // Remove non-alpha prefix
        var index = Math.max(busName.indexOf(":"), busName.indexOf("@"));
        busName = busName.substring(index + 1);
        busName = busName.trim();
        Avo_LogDebug("Business(" + busName + ")", 2);   //debug

        //var pointOfContact = activationArr[pointOfContactIndex];
        var primaryUnit = activationArr[primaryUnitIndex];
        Avo_LogDebug("Primary Unit(" + primaryUnit + ")", 2);   //debug

        //var officerNum = activationArr[officerNumIndex];
        var callerName = activationArr[callerNameIndex];
        Avo_LogDebug("Caller(" + callerName + ")", 2);   //debug

        var callerNum = activationArr[callerNumIndex];
        Avo_LogDebug("Caller #(" + callerNum + ")", 2);   //debug

        var policeFire = activationArr[policeFireIndex];
        Avo_LogDebug("PoliceFire(" + policeFire + ")", 2);   //debug

        var comments = activationArr[commentsIndex];
        Avo_LogDebug("Comments(" + comments + ")", 2);   //debug

        var cosRecordId = activationArr[recordIdIndex];
        Avo_LogDebug("Record(" + cosRecordId + ")", 2);   //debug

        // Find record using alt Id
        var recordId = null;
        if (cosRecordId) {
            var result = aa.cap.getCapID(cosRecordId);
            if (result.getSuccess() != true) {
                Avo_LogDebug("Failed to get record " + cosRecordId + ". " + result.errorType + ": " + result.errorMessage, 1);
                cosRecordId = null;
            } else {
                recordId = result.getOutput();

                recordId = getValidRecordId(recordId);
            }
        }

        // Find record using address
        if (!recordId) {
            Avo_LogDebug("No record id included in activation. Searching for record by address", 1);

            recordId = getRecordByAddress(streetNum, streetDir, streetName, streetType, unitNum);
        }

        //var bldgType = activationArr[bldgTypeIndex];
        //var locationFull = activationArr[locationFullIndex];
        //var xCoord = activationArr[xCoordIndex];
        //var yCoord = activationArr[yCoordIndex];
        //var exportDate = activationArr[exportDateIndex];
        //var bannerExpDate = activationArr[bannerExpDateIndex];
        //var exportStatus = activationArr[exportStatusIndex];
        var alarmTime = activationArr[alarmTimeIndex];
        Avo_LogDebug("Alarm Time(" + alarmTime + ")", 2);   //debug

        var eventId = activationArr[eventIdIndex];
        Avo_LogDebug("Event ID(" + eventId + ")", 2);   //debug

        if (!recordId) {
            Avo_LogDebug("**WARNING: No matching record found", 1);
            cosRecordId = null;
        } else {
            capId = recordId;
            var altId = aa.cap.getCap(capId).getOutput().capModel.altID;

            if (cosRecordId != altId) {
                cosRecordId = null;
            }

            var category = "permit";
            if (appMatch("Licenses/Business/Alarm User/Application", capId) == true) {
                category = "application";
            }

            // Add activation to ASIT
            var rowToAdd = new Object();
            rowToAdd[alarmIdField] = alarmId;
            rowToAdd[alarmDateField] = alarmDate;
            rowToAdd[dispositionField] = dispositionCode;
            rowToAdd[busNameField] = busName;
            rowToAdd[primaryUnitField] = primaryUnit;
            rowToAdd[callerNameField] = callerName;
            rowToAdd[callerNumField] = callerNum;
            rowToAdd[policeFireField] = policeFire;
            rowToAdd[commentsField] = comments;
            rowToAdd[alarmTimeField] = alarmTime;
            rowToAdd[eventIdField] = eventId;
            rowToAdd[methodField] = "System";
            rowToAdd[processedField] = "UNCHECKED";
            rowToAdd[activeField] = "Yes";
            rowToAdd[dateAddedField] = aa.util.formatDate(today, "MM/dd/yyyy");

            addToASITable(tableName.toUpperCase(), rowToAdd, capId);
            Avo_LogDebug('Added new row to ASIT "' + tableName + '" on ' + category + ' ' + altId, 1);

            // Add to record list
            updatedRecords[altId] = capId;
        }

        if (cosRecordId) {
            continue;
        }

        var updatedActivation = "";

        // Update export file
        for (var j = 0; j < recordIdIndex; j++) {
            updatedActivation += activationArr[j] + "|";
        }

        // Add record id to export file contents
        if (recordId) {
            var altId = aa.cap.getCap(capId).getOutput().capModel.altID;
            updatedActivation += altId;
        }

        // Continue updating export file
        for (var j = recordIdIndex + 1; j < activationArr.length; j++) {
            updatedActivation += "|" + activationArr[j];
        }

        Avo_LogDebug("Updated Activation(" + updatedActivation + ")", 2);   //debug
        updatedActivationDetails += updatedActivation + "\r\n";
    }

    if (updatedActivationDetails.length > 0) {
        // Upload export file to file server
        Avo_LogDebug(br + "Exporting updated activations file", 1);
        var day = String(today.getDate());
        day = day.length == 2 ? day : "0" + day;

        var month = String(today.getMonth() + 1);
        month = month.length == 2 ? month : "0" + month;

        var year = today.getFullYear();

        var expFilename = "ACCELA_ALU_EXPORT_" + day + month + year + ".txt";
        var success = saveData(sftpFolder, expFilename, updatedActivationDetails);
        Avo_LogDebug("Upload Success(" + success + ")", 2); //debug
    }

    // Update activation fees
    for (var i in updatedRecords) {
        capId = updatedRecords[i];
        var altId = i;
        Avo_LogDebug(br + "CapId(" + altId + ")", 2); //debug

        eval(getScriptText("Avo_UpdateAlarmActFees"));
        Avo_UpdateAlarmActFees(capId);
    }
}

function AddUnpermittedFee(recordId, altId, alarmDate) {
    var result = aa.cap.getCap(recordId);
    if (result.getSuccess() != true) {
        Avo_LogDebug("Failed to get cap for record " + altId + ". " + result.errorType + ": " + result.errorMessage, 1);
        return null;
    }

    var cap = result.getOutput();
    var fileDate = new Date(cap.fileDate.epochMilliseconds);
    Avo_LogDebug("File date(" + aa.util.formatDate(fileDate, "MM/dd/yyyy") + ")", 2);	//debug

    var feeSched = aa.finance.getFeeScheduleByCapID(recordId).getOutput();	// string

    if (fileDate.getTime() > alarmDate.getTime()) {
        var capIdToStore = capId;
        capId = recordId;

        var feeResult = updateFee("ALUUNP", feeSched, "FINAL", 1, "Y");
        if (feeResult) {
            Avo_LogDebug("Fee ALUUNP has been added to record " + altId, 1);
            capId = capIdToStore;
            return true;
        }
        else if (feeResult == null) {
            Avo_LogDebug("Fee ALUUNP has been incremented on record " + altId, 1);
            capId = capIdToStore;
            return true;
        } else {
            Avo_LogDebug("Failed to add the fee ALUUNP to record " + altId, 1);
            capId = capIdToStore;
            return null;
        }
    }

    var b1ExpResults = aa.expiration.getLicensesByCapID(recordId);
    if (b1ExpResults.getSuccess() != true) {
        Avo_LogDebug("Failed to get expiration info for record " + altId + ". " + b1ExpResults.errorType + ": " + b1ExpResults.errorMessage, 1);
        return null;
    }

    var b1Exp = b1ExpResults.getOutput();
    var b1ExpModel = b1Exp.getB1Expiration();
    if (!b1ExpModel) {
        Avo_LogDebug("Failed to get expiration model for record " + altId, 1);
        return null;
    }

    var expStatus = b1ExpModel.getExpStatus();
    Avo_LogDebug("Exp Status(" + expStatus + ")", 2);   //debug

    if (expStatus == "Expired") {
        return false;
    }

    var expiration = b1ExpModel.getExpDate();
    var expDate = new Date(expiration.time);
    Avo_LogDebug("Exp date(" + aa.util.formatDate(expDate, "MM/dd/yyyy") + ")", 2);	//debug

    var expDatePlus120 = new Date(expDate.getTime());
    expDatePlus120.setDate(expDatePlus120.getDate() + 120);

    var today = Avo_GetToday();

    if (today.getTime() >= expDate.getTime() && today.getTime <= expDatePlus120.getTime()) {
        var capIdToStore = capId;
        capId = recordId;

        //Check if record has a renewal
        var result = aa.cap.getProjectByMasterID(recordId, "Renewal", null);
        if (result.getSuccess() == true) {
            Avo_LogDebug("Record has renewals", 1);

            var allRenewals = result.getOutput();
            for (var i in allRenewals) {
                var renewalCapId = allRenewals[i].capID;
                if (!renewalCapId) {
                    continue;
                }

                capId = renewalCapId;
                altId = aa.cap.getCap(renewalCapId).getOutput().capModel.altID;
            }
        }

        var feeResult = updateFee("ALUUNP", feeSched, "FINAL", 1, "Y");
        if (feeResult) {
            Avo_LogDebug("Fee ALUUNP has been added to record " + altId, 1);
            capId = capIdToStore;
            return true;
        }
        else if (feeResult == null) {
            Avo_LogDebug("Fee ALUUNP has been incremented on record " + altId, 1);
            capId = capIdToStore;
            return true;
        } else {
            Avo_LogDebug("Failed to add the fee ALUUNP to record " + altId, 1);
            capId = capIdToStore;
            return null;
        }
    }
}

function getRecordByAddress(streetNum, streetDir, streetName, streetType, unitNum) {
    var result = aa.address.getAddressByStreetName(streetName, aa.util.newQueryFormat());
    if (result.getSuccess() != true) {
        Avo_LogDebug('Failed to find address with street name "' + streetName + '". ' + result.errorMessage, 1);
        return null;
    }

    var allAddresses = result.getOutput();
    Avo_LogDebug("Addresses Found(" + allAddresses.length + ")", 2);    //debug

    for (var j in allAddresses) {
        var address = allAddresses[j];
        var addressModel = address.addressModel;

        //if (addressModel.city && addressModel.city != city) {
        //    continue;
        //}
        if (addressModel.houseNumberStart != streetNum) {
            //Avo_LogDebug("Street # " + addressModel.houseNumberStart + " does not match", 1);
            continue;
        }

        //Avo_LogDebug("Street #(" + addressModel.houseNumberStart + ")", 2); //debug

        if (addressModel.streetDirection != streetDir) {
            Avo_LogDebug("Street direction " + addressModel.streetDirection + " does not match", 1);
            continue;
        }

        //Avo_LogDebug("Street Dir(" + addressModel.streetDirection + ")", 2);    //debug

        if (addressModel.streetName != streetName) {
            //Avo_LogDebug("Street name " + addressModel.streetName + " does not match", 1);
            continue;
        }

        //Avo_LogDebug("Street Name(" + addressModel.streetName + ")", 2); //debug

        if (addressModel.streetSuffix != streetType) {
            Avo_LogDebug("Street type " + addressModel.streetSuffix + " does not match", 1);
            continue;
        }

        //Avo_LogDebug("Street Type(" + addressModel.streetSuffix + ")", 2); //debug

        if (unitNum) {
            Avo_LogDebug("Unit #(" + addressModel.unitStart + ")", 2); //debug

            if (addressModel.unitStart == null || !addressModel.unitStart) {
                Avo_LogDebug("Unit # is null", 1);
                continue;
            }

            if (addressModel.unitStart != unitNum) {
                Avo_LogDebug("Unit # " + addressModel.unitStart + " does not match", 1);
                continue;
            }
        }

        //if (addressModel.state && addressModel.state != state) {
        //    continue;
        //}
        //if (addressModel.zip && addressModel.zip != zip) {
        //    continue;
        //}

        Avo_LogDebug("Found matching address " + addressModel.addressLine1, 1);

        result = aa.address.getCapIdByAddress(addressModel);
        if (result.getSuccess() != true) {
            Avo_LogDebug("Failed to find record with address. " + result.errorMessage, 1);
            continue;
        }

        var allRecords = result.getOutput();
        Avo_LogDebug("Records with Address Found(" + allRecords.length + ")", 2);   //debug

        if (allRecords.length == 0) {
            if (unitNum) {
                Avo_LogDebug("No records associated with address " + addressModel.addressLine1 + " unit " + unitNum, 1);
            } else {
                Avo_LogDebug("No records associated with address " + addressModel.addressLine1, 1);
            }

            continue;
        }

        var allCategories = ["Permit", "Application"];

        for (var k in allCategories) {
            var category = allCategories[k];

            for (var i in allRecords) {
                var capIdModel = allRecords[i];
                var cap = aa.cap.getCap(capIdModel.ID1, capIdModel.ID2, capIdModel.ID3).getOutput();
                var recordId = cap.getCapID();

                var recordType = String(cap.capType);
                Avo_LogDebug("Record Type(" + recordType + ")", 2); //debug

                if (recordType != "Licenses/Business/Alarm User/" + category) {
                    continue;
                }

                recordId = getValidRecordId(recordId);
                if (!recordId) {
                    continue;
                }

                var altId = aa.cap.getCap(recordId).getOutput().capModel.altID;
                if (!altId) {
                    continue;
                }

                Avo_LogDebug("Found record " + altId + " with matching address", 1);
                return recordId;
            }
        }

        return null;
    }

    return null;
}

function getValidRecordId(recordId) {
    var cap = aa.cap.getCap(recordId).getOutput()
    var altId = cap.capModel.altID;
    Avo_LogDebug("Record Id(" + altId + ")", 2); //debug

    var recordType = String(cap.capType);
    Avo_LogDebug("Record Type(" + recordType + ")", 2); //debug

    if (recordType == "Licenses/Business/Alarm User/Application") {
        // Check record status
        var status = String(cap.getCapStatus());
        Avo_LogDebug("Status(" + status + ")", 2);  //debug

        if (status.indexOf("Closed:") == -1) {
            return recordId;
        } else {
            // Check for parent Permit
            var allParents = aa.cap.getProjectByChildCapID(recordId, "", "").getOutput();
            if (allParents && allParents.length > 0) {
                for (var i in allParents) {
                    var parentCapId = allParents[i].projectID;
                    var parentCap = aa.cap.getCap(parentCapId).getOutput();
                    var parentAltId = parentCap.capModel.altID;
                    Avo_LogDebug("Parent(" + parentAltId + ")", 2); //debug

                    var parentRecordtype = String(parentCap.capType);
                    Avo_LogDebug("Parent Record Type(" + parentRecordtype + ")", 2); //debug

                    if (parentRecordtype != "Licenses/Business/Alarm User/Permit") {
                        continue;
                    }

                    Avo_LogDebug("Parent Permit found. Checking if valid", 1);
                    return getValidRecordId(parentCapId);
                }
            } else {
                return null;
            }
        }
    }

    // Check record is valid
    var b1ExpResults = aa.expiration.getLicensesByCapID(recordId);
    if (b1ExpResults.getSuccess() != true) {
        Avo_LogDebug("Failed to get expiration info for record " + altId + ". " + b1ExpResults.errorType + ": " + b1ExpResults.errorMessage, 1);
        return null;
    }

    var b1Exp = b1ExpResults.getOutput();
    var b1ExpModel = b1Exp.getB1Expiration();
    if (!b1ExpModel) {
        Avo_LogDebug("Failed to get expiration model for record " + altId, 1);
        return null;
    }

    var expStatus = b1ExpModel.getExpStatus();
    Avo_LogDebug("Exp Status(" + expStatus + ")", 2);   //debug

    if (expStatus == "Expired") {
        Avo_LogDebug("Record " + altId + " is invalid. Returning null", 1);
        return null;
    }
    
    var wfTask = "License";
    var wfProcess = "LIC_ALU_LIC";
    var validStatuses = ["Active", "About to Expire"];

    var result = aa.workflow.getTaskItems(recordId, wfTask, wfProcess, null, null, null);
    if (result.getSuccess() == true) {
        var allTaskItems = result.getOutput();

        for (var i in allTaskItems) {
            var taskItem = allTaskItems[i];

            var taskName = taskItem.taskDescription;
            Avo_LogDebug("Task(" + taskName + ")", 2);  //debug

            if (!taskName.toUpperCase().equals(wfTask.toUpperCase())) {
                continue;
            }
            if (!taskItem.processCode.equals(wfProcess)) {
                continue;
            }

            var isActive = taskItem.activeFlag == "Y";
            if (isActive != true) {
                continue;
            }

            var status = taskItem.disposition;
            Avo_LogDebug("Task Status(" + status + ")", 2); //debug

            for (var j in validStatuses) {
                var statusToMatch = validStatuses[j];
                if (statusToMatch != status) {
                    continue;
                }

                Avo_LogDebug("Record " + altId + " is valid", 1);
                return recordId;
            }
        }
    }

    Avo_LogDebug("Record " + altId + " is invalid. Returning null", 1);
    return null;
}

function processResponse(filename, response) {
    var allActivations = new Array();

    //// Testing
    //allActivations = [
    //    "198175|2018-01-30 01:15:38|F|ALARM/459A|7225|E|DOVE VALLEY|RD||85266||: @EMERALD INDUSTRIES||1L3|1132|COX BUSINESS|(800) 633-2677|** LOI search completed at 01/30/18 01:15:39 INTERIOR FRONT MOTION C PREM NO ANSWER ATC RP'S C4 ** Event E0130180037 closed.|0||6842 E THOMAS RD SC: @EMERALD INDUSTRIES|69506810|90239440|2018-01-31 02:00:09.470000000||0|01:15:38.0000000|E0130180037|"
    //];
    //return allActivations; // End testing

    var extension = String(filename.substring(filename.lastIndexOf(".") + 1).toUpperCase());
    Avo_LogDebug("Extension(" + extension + ")", 2);  //debug

    switch (extension) {
        case "XML":
            var stream = aa.proxyInvoker.newInstance("java.io.StringBufferInputStream", new Array(response.Contents)).getOutput();
            var saxBuilder = aa.proxyInvoker.newInstance("org.jdom.input.SAXBuilder").getOutput();
            var document = saxBuilder.build(stream);

            var root = document.getRootElement();
            // printLine(Serialize(root));

            var errorNode = root.getChild("Error");

            var allActors = root.children;
            // printLine(Serialize(allActors));
            // printLine("Nodes(" + allActors.size() + ")");	//debug

            for (var i = 0; i < allActors.size(); i++) {
                var actor = allActors.get(i);
                // printLine(Serialize(actor));

                // var firstName = actor.getChild("FirstName");
                // var lastName = actor.getChild("LastName");
                // var age = actor.getChild("Age");
                // var city = actor.getChild("City");
                // var country = actor.getChild("Country");

                // printLine("Name: " + firstName + ", " + lastName);
                for (var j = 0; j < actor.children.size(); j++) {
                    var attribute = actor.children.get(j);
                    //printLine(Serialize(attribute));
                    break;
                }
            }
            break;

        case "CSV":
            var allLines = String(response.Contents).split(/\r\n,\n/);
            Avo_LogDebug("Total Lines(" + allLines.length + ")", 2);	//debug

            for (i in allLines) {
                var line = allLines[i];

                // Remove empty lines
                line = line.trim();
                if (line.length == 0) {
                    Avo_LogDebug("Line " + String(parseInt(i, 10) + 1) + " is blank", 1);
                    continue;
                }

                allActivations.push(line);
            }
            break;

        case "TXT":
            var allLines = String(response.Contents).split(/\r\n|\n/);
            Avo_LogDebug("Total Lines(" + allLines.length + ")", 2);	//debug

            for (i in allLines) {
                var line = allLines[i];

                // Remove empty lines
                line = line.trim();
                if (line.length == 0) {
                    Avo_LogDebug("Line " + String(parseInt(i, 10) + 1) + " is blank", 1);
                    continue;
                }

                allActivations.push(line);
            }
            break;

        default:
            Avo_LogDebug('File type "' + extension + '" not supported', 1);
            break;
    }

    return allActivations;
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

function elapsed() {
    var thisDate = new Date();
    var thisTime = thisDate.getTime();
    return ((thisTime - startTime) / 1000)
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}