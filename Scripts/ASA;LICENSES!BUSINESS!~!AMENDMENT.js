//ASA: Licenses/Business/*/Amendment

//call script ** ID3 ** script/functions based on requirements
Avo_SetAmendmentID();

/*******************************************************
| Script/Function: Avo_SetAmendmentID() - (ID3)
| Created by: Nicolaj Bunting
| Created on: 11Dec17
| Usage: When amendment is created then set alt id to sibling application's id prefix 
| with "-AMD-" and then the amendment number appended to the end
| Modified by: Nic Bunting (18Apr18)
*********************************************************/
function Avo_SetAmendmentID() {
    logDebug("ID3 Avo_SetAmendmentID()");

    // Get Type
    var cap = aa.cap.getCap(capId).getOutput();
    var capTypeModel = cap.getCapType();
    var capSubType = capTypeModel.getSubType();
    Avo_LogDebug("SubType(" + capSubType + ")", 2);   //debug

    var category = "License";
    if (capSubType == "Alarm User") {
        category = "Permit";
    }

    // Get parent license
    var licCapId = parentCapId;
    if (!licCapId) {
        var allParents = aa.cap.getProjectByChildCapID(capId, "", "").getOutput();
        if (allParents.length == 0) {
            Avo_LogDebug("No parent records", 1);
            return;
        }

        //var allParentLics = getParents("Licenses/Business/" + capSubType + "/License");
        //if (allParentLics.length < 1) {
        //    Avo_LogDebug("No parent license", 1);
        //    return;
        //}

        //licCapId = allParentLics[0];
        //if (!licCapId) {
        //    Avo_LogDebug("No parent license", 1);
        //    return;
        //}

        for (i in allParents) {
            parentCapId = allParents[i].projectID;

            if (appMatch("Licenses/Business/" + capSubType + "/" + category, parentCapId) == false) {
                continue;
            }

            licCapId = parentCapId;
            break;
        }
    }

    var licAltId = aa.cap.getCap(licCapId).getOutput().getCapModel().altID;
    Avo_LogDebug("Parent " + category + "(" + licAltId + ")", 2);   //debug

    // Get Sibling application
    var appCapId = getChildren("Licenses/Business/" + capSubType + "/Application", licCapId)[0];
    if (!appCapId) {
        Avo_LogDebug("No sibling app record matching amendment subtype " + capSubType, 1);
        return;
    }

    var appAltId = aa.cap.getCap(appCapId).getOutput().getCapModel().altID;

    // Get amendment number
    var amendmentNum = 1;

    var amendRecords = getChildren("Licenses/Business/" + capSubType + "/Amendment", licCapId);

    Avo_LogDebug("Amendments(" + amendRecords.length + ")", 2);    //debug
    if (amendRecords.length > 1) {
        const amendRegex = /\w{3}\d{4}-\d{7}-AMD-(\d+)/;

        for (a in amendRecords) {
            var amendCapId = amendRecords[a];
            var amendAltId = aa.cap.getCap(amendCapId).getOutput().getCapModel().altID;

            var matches = amendRegex.exec(amendAltId);
            if (!matches) {
                continue;
            }
            if (matches.length == 0) {
                continue;
            }

            var numToCheck = parseInt(matches[1], 10);
            if (numToCheck <= amendmentNum) {
                continue;
            }

            amendmentNum = numToCheck;
        }

        amendmentNum++;
    }

    const regex = /(\w{3})\d{4}-(\d{7})-APP/;

    var matches = regex.exec(appAltId)
    var prefix = matches[1];
    Avo_LogDebug("Prefix(" + prefix + ")", 2);   //debug

    var licNum = matches[2];
    Avo_LogDebug("Lic Num(" + licNum + ")", 2);   //debug

    var year = new Date().getFullYear();

    var amendAltId = prefix + year + "-" + licNum + "-AMD-" + amendmentNum;
    Avo_LogDebug("Updated AltId(" + amendAltId + ")", 2);  //debug

    result = aa.cap.updateCapAltID(capId, amendAltId);
    if (!result.getSuccess()) {
        Avo_LogDebug("Failed to update amendment's alt id", 1);
    }
}