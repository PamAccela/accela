/*******************************************************
| Script Title: Batch_CreateRenewals (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 23Mar18
| Usage: For all "Licenses/Business/NA/License" records If today's date is X days or less from expiration date Then set expiration status to "About to Expire", 
| create renewal record, and copy ASI, ASIT, address, parcel, and contacts to renewal record
| Subtype                           X Days
| Alarm User                        29
| Charitable Solicitations          45  *if ASI "Activity Type" is "Permanent Location"
| Bus Reg Merchant                  45 
| Bus Reg Service                   45
| Liquor                            60
| Adult Service Provider            90
| After Hours Establishment         90
| Auction House                     90
| Auctioneer                        90
| Magic Arts                        90
| Neighborhood Street Vendor        90
| Recycling                         90
| Secondhand                        90
| Sexually Oriented Business Mgr    90
| Sexually Oriented Business        90
| Solid Waste                       90
| Teen Dance Center                 90
| Valet Parking                     90
| Escort Bureau                     120
| Escort                            120
| Massage Facility                  120
| Teletrack Establishment           120
| Teletrack Operator                120
| Modified by: Nic Bunting (29Jun18)
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
//aa.env.setValue("param1","Yes");
//aa.env.setValue("param1","No");
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var param1 = aa.env.getValue("param1");
//var param2 = aa.env.getValue("param2");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
//eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    aa.print("Batch Job " + batchJobName + " Job ID is " + batchJobID);
}
else {
    aa.print("Batch job ID not found " + batchJobResult.getErrorMessage());
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    aa.print("Start of Job");

    if (!timeExpired) {
        try {
            CreateRenewals();
        }
        catch (e) {
            aa.print("Error in process " + e.message);
        }
    }
    else {
        aa.print("End of Job: Elapsed Time : " + elapsed() + " Seconds");
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

function CreateRenewals() {
    printLine("ID73 CreateRenewals()");

    var group = "Licenses";
    var type = "Business";
    var subType = null;
    var category = null;
    var appsArray = aa.cap.getByAppType(group, type, subType, category).getOutput();
    if (appsArray.length == 0) {
        if (!group) {
            group = "*";
        }
        if (!type) {
            type = "*";
        }
        if (!subType) {
            subType = "*";
        }
        if (!category) {
            category = "*";
        }

        printLine('No records of type "' + group + "/" + type + '/' + subType + '/' + category + '" found');
        return;
    }

    var today = new Date();

    for (a in appsArray) {
        capId = appsArray[a].getCapID();

        var isLic = appMatch("*/*/*/License", capId);
        var isPerm = appMatch("*/*/*/Permit", capId);
        if (isLic != true && isPerm != true) {
            continue;
        }

        var altId = capId.getCustomID();
        printLine("License CapId(" + altId + ")");  //debug

        if (isLic == true) {
            printLine("License");
        }
        if (isPerm == true) {
            printLine("Permit");
        }

        // Get Subtype
        var cap = aa.cap.getCap(capId).getOutput();
        var capTypeModel = cap.getCapType();
        subType = String(capTypeModel.getSubType());
        printLine("Subtype(" + subType + ")");    //debug
        //logDebug("typeof Subtype(" + typeof (capSubType) + ")");    //debug

        var daysAgo = null;
        switch (subType) {
            case "Alarm User":
                printLine("29 days ago");
                daysAgo = 29;
                break;

            case "Charitable Solicitations":
                var activityType = getAppSpecific("Activity Type", capId);
                printLine("Activity Type(" + activityType + ")");   //debug

                if (activityType != "Permanent Location") {
                    break;
                }
            case "Bus Reg Merchant":
            case "Bus Reg Service":
                printLine("45 days ago");
                daysAgo = 45;
                break;

            case "Liquor":
                printLine("60 days ago");
                daysAgo = 60;
                break;

            case "Adult Service Provider":
            case "After Hours Establishment":
            case "Auction House":
            case "Auctioneer":
            case "Magic Arts":
            case "Neighborhood Street Vendor":
            case "Recycling":
            case "Secondhand":
            case "Sexually Oriented Business Mgr":
            case "Sexually Oriented Business":
            case "Solid Waste":
            case "Teen Dance Center":
            case "Valet Parking":
                printLine("90 days ago");
                daysAgo = 90;
                break;

            case "Escort Bureau":
            case "Escort":
            case "Massage Facility":
            case "Teletrack Establishment":
            case "Teletrack Operator":
                printLine("120 days ago");
                daysAgo = 120;
                break;

            //case "Close Out Sales":
            //case "Liquor Special Event":
            //case "Promoter":
            //case "Solicitor For Profit":
            //case "Valet Parking Special Event":
        }

        if (daysAgo == null) {
            printLine("No expiration rules for " + subType + " licenses");
            continue;
        }

        var renewalDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() - daysAgo);
        printLine("Renewal date(" + aa.util.formatDate(renewalDate, "MM/dd/yyyy") + ")");   //debug

        // Get expiration date
        var b1ExpResults = aa.expiration.getLicensesByCapID(capId);
        if (b1ExpResults.getSuccess() != true) {
            printLine("No expiration info for record " + altId);
            continue;
        }

        var b1Exp = b1ExpResults.getOutput();
        var b1ExpModel = b1Exp.getB1Expiration();
        if (!b1ExpModel) {
            printLine("Unable to load b1ExpModel for record " + altId);
            continue;
        }

        var expiration = b1ExpModel.getExpDate();
        var expDate = new Date(expiration.time);
        printLine("Exp date(" + aa.util.formatDate(expDate, "MM/dd/yyyy") + ")");   //debug

        if (expDate.getTime() > renewalDate.getTime()) {
            continue;
        }

        // Update expiration status
        b1ExpModel.setExpStatus("About to Expire");
        var b1EditResult = aa.expiration.editB1Expiration(b1ExpModel);
        if (b1EditResult.getSuccess() == true) {
            printLine("Set expiration status on record " + altId + ' to "About to Expire"');
        } else {
            printLine("Failed to update expiration status on record " + altId);
        }

        // Check license doesn't have an incomplete renewal
        var result = aa.cap.getProjectByMasterID(capId, "Renewal", null);
        if (result.getSuccess() == true) {
            printLine("Record " + altId + " has incomplete renewal(s):");

            var renewals = result.getOutput();
            for (a in renewals) {
                var renewalCapId = renewals[a].getCapID();
                var renewalAltId = aa.cap.getCap(renewalCapId).getOutput().getCapModel().altID;
                printLine(renewalAltId);
            }

            continue;
        }

        // Create renewal record
        printLine("Renewal type(" + group + '/' + type + '/' + subType + '/Renewal)');  //debug

        var renewalCapId = aa.cap.createApp(group, type, subType, "Renewal", subType + " " + type + " Renewal").getOutput();
        var renewalCap = aa.cap.getCap(renewalCapId).getOutput();
        var renewalAltId = renewalCap.getCapModel().altID;
        printLine("Created renewal " + renewalAltId + " of type " + group + '/' + type + '/' + subType + '/Renewal');

        parentCapId = capId;
        capId = renewalCap.getCapID();
        renewalCapId = capId;

        result = aa.cap.createRenewalCap(parentCapId, capId, true);
        if (result.getSuccess() == true) {
            printLine("Linked renewal " + renewalAltId + " to parent license " + altId);
        } else {
            printLine("ERROR: Associate partial cap with parent CAP. " + result.getErrorMessage());
        }

        var capIdStr = capId.ID1 + "-" + capId.ID2 + "-" + capId.ID3;

        aa.env.setValue("CapId", capIdStr);
        aa.env.setValue("PermitId1", capId.ID1);
        aa.env.setValue("PermitId2", capId.ID2);
        aa.env.setValue("PermitId3", capId.ID3);

        aa.runScript("ConvertToRealCapAfter4Renew");

        aa.env.setValue("ScriptCode", "APPLICATIONSUBMITAFTERV3.0");
        aa.env.setValue("EventName", "ApplicationSubmitAfter");
        aa.runScript("APPLICATIONSUBMITAFTERV3.0");

        // Copy ASI
        copyASIFields(parentCapId, renewalCapId);
        printLine("All ASI fields copied from license " + altId + " to renewal " + renewalAltId);

        // Copy ASIT
        copyASITables(parentCapId, renewalCapId);
        printLine("All ASIT's copied from license " + altId + " to renewal " + renewalAltId);

        // Copy Addresses
        copyAddresses(parentCapId, renewalCapId);
        printLine("All addresses copied from license " + altId + " to renewal " + renewalAltId);

        // Copy Parcels
        copyParcels(parentCapId, renewalCapId);
        printLine("All parcels copied from license " + altId + " to renewal " + renewalAltId);

        // Copy Contacts
        copyContacts(parentCapId, renewalCapId);
        printLine("All contacts copied from license " + altId + " to renewal " + renewalAltId);
    }
}

function printLine(pStr) {
    aa.print(pStr + br);
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}