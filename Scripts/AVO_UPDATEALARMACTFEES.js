/*******************************************************
| Script/Function: Avo_UpdateAlarmActFees([recordId])
| Created by: Nicolaj Bunting
| Created on: 20Apr18
| Usage: Adjusts the number of alarm activation fees invoiced on recordId based on the entries in ASIT "Activations"
| Modified by: Nic Bunting (17May18)
| Modified by: Nic Bunting (21May19)
| Modified by: Nic Bunting (27May19)
| Modified by: Nic Bunting (30May19)
| Modified by: Nic Bunting (23Aug19)
*********************************************************/
function Avo_UpdateAlarmActFees(recordId) {
    if (arguments.length == 0 || !arguments[0]) {
        recordId = capId;
    }

    var cap = aa.cap.getCap(recordId).getOutput()
    var altId = cap.getCapModel().altID;
    Avo_LogDebug("Avo_UpdateAlarmActFees(" + altId + ")", 1);

    if (appMatch("Licenses/Business/Alarm User/*", recordId) != true) {
        Avo_LogDebug("Record " + altId + " is not an Alarm User record type", 1);
        return;
    }

    var tableName = "ACTIVATIONS";
    var alarmDateColumn = "Alarm Date";
    var dispositionColumn = "Disposition Code";
    var eventIdColumn = "Event ID";
    var processedColumn = "Processed";
    var activeColumn = "Active?";
    var allColumns = ["Alarm ID", alarmDateColumn, dispositionColumn, "Business Name", "Primary Unit", "Caller Name", "Caller Number", "Address",
        "Comments", "Alarm Time", eventIdColumn, "Method", processedColumn, activeColumn, "Date Added"];

    var allFeeCodes = ["ALUACT", "ALUACT50", "ALUACT75", "ALUACT100", "ALUACT200"];

    // Check record type
    var capTypeModel = cap.capType;
    var appType = capTypeModel.getCategory();
    Avo_LogDebug("App Type(" + appType + ")", 2);   //debug

    if (appType != "Application" && appType != "Permit") {
        return;
    }

    var table = loadASITable(tableName, recordId);
    if (!table) {
        Avo_LogDebug("Table " + tableName + " could not be loaded on " + appType + " " + altId, 1);
        return;
    }
    if (table.length == 0) {
        Avo_LogDebug("Table " + tableName + " is empty on " + appType + " " + altId, 1);
        return;
    }

    var today = Avo_GetToday();

    var allCurrentFeeArrays = new Array();  // ALU0, ALU50, ALU75, ALU100, ALU200
    var allFutureFeeArrays = new Array();

    for (var i = 0; i < 5; i++) {
        allCurrentFeeArrays.push(new Object());
        allFutureFeeArrays.push(new Object());
    }

    var allCurrentEvents = new Array();
    var allCurrentRemovedEvents = new Object();

    var allFutureEvents = new Array();
    var allFutureRemovedEvents = new Object();

    // Get current period start and end dates
    var capModel = aa.cap.getCap(recordId).getOutput().capModel;
    var startDate = capModel.fileDate;

    var endDate = null;

    var b1ExpResults = aa.expiration.getLicensesByCapID(recordId);
    if (b1ExpResults.getSuccess() == false) {
        Avo_LogDebug("Unable to get expiration info on " + appType + " " + altId, 1);
    } else {
        var b1Exp = b1ExpResults.getOutput();
        var b1ExpModel = b1Exp.getB1Expiration();
        if (!b1ExpModel) {
            Avo_LogDebug("Unable to get expiration model on " + appType + " " + altId, 1);
        } else {
            var expiration = b1ExpModel.getExpDate();
            endDate = new Date(expiration.time);
        }
    }

    Avo_LogDebug("Start(" + aa.util.formatDate(startDate, "MM/dd/yyyy") + ")", 2);    //debug
    if (endDate) {
        Avo_LogDebug("End(" + aa.util.formatDate(endDate, "MM/dd/yyyy") + ")", 2);  //debug
    }

    // Get all invoiced fees

    // If permit get invoiced fees from app
    if (appType == "Permit") {
        // Get child App
        var allChildren = getChildren("Licenses/Business/Alarm User/Application", recordId);

        if (!allChildren || allChildren.length == 0) {
            Avo_LogDebug("No child Alarm User apps on " + appType + " " + altId, 1);
        } else {
            for (var a in allChildren) {
                var childAppCapId = allChildren[a];
                var childAppAltId = aa.cap.getCap(childAppCapId).getOutput().capModel.altID;
                Avo_LogDebug("Child App(" + childAppAltId + ")", 2);  //debug

                for (var i = 0; i < allFeeCodes.length; i++) {
                    var feeCode = allFeeCodes[i];

                    var result = aa.fee.getFeeItems(childAppCapId, feeCode, "INVOICED");
                    if (result.getSuccess() != true) {
                        Avo_LogDebug("Unable to get " + feeCode + " fees on App " + childAppAltId, 1);
                        continue;
                    }

                    var allFees = result.getOutput();
                    for (var j in allFees) {
                        var feeItem = allFees[j];

                        if (feeItem.feeitemStatus != "INVOICED") {
                            continue;
                        }
                        if (feeItem.feeCod != feeCode) {
                            continue;
                        }

                        Avo_LogDebug("Fee Code(" + feeItem.feeCod + ")", 2);    //debug

                        var feeId = parseInt(feeItem.feeSeqNbr);
                        var note = String(feeItem.f4FeeItemModel.feeNotes);
                        Avo_LogDebug("Event Id(" + note + ")", 2);  //debug

                        var applyDate = new Date(feeItem.applyDate.epochMilliseconds);
                        Avo_LogDebug("Fee Date(" + aa.util.formatDate(applyDate, "MM/dd/yyyy") + ")", 2);  //debug

                        if (applyDate.getTime() < startDate.getTime()) {
                            Avo_LogDebug("Previous period", 2); //debug
                            continue;
                        }

                        var currentPeriod = true;
                        if (endDate && applyDate.getTime() > endDate.getTime()) {
                            currentPeriod = false;
                            Avo_LogDebug("Future period", 2);   //debug
                        }

                        var eventFeeObj = { eventId: note, feeId: feeId, alarmDate: applyDate };

                        if (currentPeriod == true) {
                            // Current Period
                            var feeArray = allCurrentFeeArrays[i];
                            feeArray[note] = eventFeeObj;

                            allCurrentEvents.push(note);
                            allCurrentRemovedEvents[note] = note;
                        } else {
                            // Future period
                            var feeArray = allFutureFeeArrays[i];
                            feeArray[note] = eventFeeObj;

                            allFutureEvents.push(note);
                            allFutureRemovedEvents[note] = note;
                        }
                    }
                }
            }
        }
    }

    for (var i = 0; i < allFeeCodes.length; i++) {
        var feeCode = allFeeCodes[i];

        var result = aa.fee.getFeeItems(recordId, feeCode, "INVOICED");
        if (result.getSuccess() != true) {
            Avo_LogDebug("Unable to get " + feeCode + " fees", 2);
            continue;
        }

        var allFees = result.getOutput();
        for (var j in allFees) {
            var feeItem = allFees[j];

            if (feeItem.feeitemStatus != "INVOICED") {
                continue;
            }
            if (feeItem.feeCod != feeCode) {
                continue;
            }

            Avo_LogDebug("Fee Code(" + feeItem.feeCod + ")", 2);    //debug

            var feeId = parseInt(feeItem.feeSeqNbr);
            var note = String(feeItem.f4FeeItemModel.feeNotes);
            Avo_LogDebug("Event Id(" + note + ")", 2);  //debug

            var applyDate = new Date(feeItem.applyDate.epochMilliseconds);
            Avo_LogDebug("Fee Date(" + aa.util.formatDate(applyDate, "MM/dd/yyyy") + ")", 2);  //debug

            if (applyDate.getTime() < startDate.getTime()) {
                Avo_LogDebug("Previous period", 2); //debug
                continue;
            }

            var currentPeriod = true;
            if (endDate && applyDate.getTime() > endDate.getTime()) {
                currentPeriod = false;
                Avo_LogDebug("Future period", 2);   //debug
            }

            var eventFeeObj = { eventId: note, feeId: feeId, alarmDate: applyDate };

            if (currentPeriod == true) {
                // Current Period
                var feeArray = allCurrentFeeArrays[i];
                feeArray[note] = eventFeeObj;

                allCurrentEvents.push(note);
                allCurrentRemovedEvents[note] = note;
            } else {
                // Future period
                var feeArray = allFutureFeeArrays[i];
                feeArray[note] = eventFeeObj;

                allFutureEvents.push(note);
                allFutureRemovedEvents[note] = note;
            }
        }
    }

    Avo_LogDebug("Current Events(" + allCurrentEvents.join(", ") + ")", 2);  //debug
    Avo_LogDebug("Future Events(" + allFutureEvents.join(", ") + ")", 2);  //debug

    var gracePeriodEnd = getAppSpecific("Grace Period End Date", recordId);
    var graceEndDate = null;
    if (gracePeriodEnd) {
        graceEndDate = Avo_GetDateFromAccelaDateString(gracePeriodEnd);
        Avo_LogDebug("Grace Period End Date(" + aa.util.formatDate(graceEndDate, "MM/dd/yyyy") + ")", 2);	//debug
    }

    var totalCurrentBillable = 0;
    var totalFutureBillable = 0;
    var totalNonBillable = 0;

    var tableArray = new Array();
    var changesMade = false;

    for (var a in table) {
        var rowArray = new Array();

        // Copy row to array
        for (var i = 0; i < allColumns.length; i++) {
            var columnName = allColumns[i];
            var field = table[a][columnName];
            var entry = field.fieldValue;
            var readOnly = field.readOnly;

            rowArray[columnName] = new asiTableValObj(columnName, entry, readOnly);
        }

        Avo_LogDebug("", 2);	//debug
        Avo_LogDebug("Entry(" + a + ")", 2);	//debug

        var processed = rowArray[processedColumn];
        var processedEntry = String(processed.fieldValue);
        Avo_LogDebug("Processed(" + processedEntry + ")", 2);	//debug

        // Get Event ID
        var event = rowArray[eventIdColumn];
        var eventId = String(event.fieldValue);
        Avo_LogDebug("Event(" + eventId + ")", 2); //debug

        var alarm = rowArray[alarmDateColumn];
        var alarmEntry = String(alarm.fieldValue);
        var alarmDate = Avo_GetDateFromAccelaDateString(alarmEntry);
        Avo_LogDebug("Alarm Date(" + aa.util.formatDate(alarmDate, "MM/dd/yyyy") + ")", 2);	//debug

        var eventIndex = allCurrentEvents.indexOf(eventId);
        Avo_LogDebug("Event(" + eventId + "), Index(" + eventIndex + ")", 2);	//debug

        if (alarmDate.getTime() < startDate.getTime()) {
            Avo_LogDebug("Activation was before current " + appType + " period", 2);
            if (eventIndex != -1) {
                allCurrentEvents.splice(eventIndex, 1);	// remove from all events
                delete allCurrentRemovedEvents[eventId];	// remove from events to remove
                Avo_LogDebug("Current Events(" + allCurrentEvents.join(", ") + ")", 2);  //debug
            }

            if (processedEntry != "CHECKED") {
                rowArray[processedColumn] = new asiTableValObj(processedColumn, "CHECKED", processed.readOnly);	// Mark as processed
                Avo_LogDebug("Processed", 2);	//debug
                changesMade = true;
            }
            tableArray.push(rowArray);	// Save row

            if (appType == "Application") {
                AddUnpermittedFee(alarmDate, eventId, recordId, altId, appType);
            }

            continue;
        }

        // Check if active
        var active = rowArray[activeColumn];
        var activeEntry = String(active.fieldValue);
        Avo_LogDebug("Active(" + activeEntry + ")", 2);	//debug

        var isActive = activeEntry == "Yes"
        if (isActive == false) {
            if (eventIndex == -1) {
                Avo_LogDebug("No fee matching event " + eventId + " in all current events", 2);	//debug
            } else {
                Avo_LogDebug("Fee matching event " + eventId + " found. Keeping in list to remove", 2); //debug
            }

            if (processedEntry != "CHECKED") {
                rowArray[processedColumn] = new asiTableValObj(processedColumn, "CHECKED", processed.readOnly);	// Mark as processed
                Avo_LogDebug("Processed", 2);	//debug
                changesMade = true;
            }
            tableArray.push(rowArray);	// Save row
            continue;
        }

        AddUnpermittedFee(alarmDate, eventId, recordId, altId, appType);

        if (graceEndDate && alarmDate.getTime() <= graceEndDate.getTime()) {
            totalNonBillable++;
            Avo_LogDebug("Non-Billable", 2);    //debug

            if (eventIndex == -1) {
                Avo_LogDebug("No fee matching event " + eventId + " in all current events", 2);	//debug
            } else {
                Avo_LogDebug("Fee matching event " + eventId + " found. Keeping in list to remove", 2); //debug
            }

            if (processedEntry != "CHECKED") {
                rowArray[processedColumn] = new asiTableValObj(processedColumn, "CHECKED", processed.readOnly);	// Mark as processed
                Avo_LogDebug("Processed", 2);	//debug
                changesMade = true;
            }
            tableArray.push(rowArray);	// Save row
            continue;
        }

        var disposition = rowArray[dispositionColumn];
        var dispositionEntry = String(disposition.fieldValue).toUpperCase();
        Avo_LogDebug("Disposition(" + dispositionEntry + ")", 2);   //debug

        if (dispositionEntry != "F" && dispositionEntry != "4" && dispositionEntry != "F2") {
            totalNonBillable++;

            Avo_LogDebug("Non-Billable", 2);    //debug

            if (eventIndex == -1) {
                Avo_LogDebug("No fee matching event " + eventId + " in all current events", 2);	//debug
            } else {
                Avo_LogDebug("Fee matching event " + eventId + " found. Keeping in list to remove", 2); //debug
            }

            if (processedEntry != "CHECKED") {
                rowArray[processedColumn] = new asiTableValObj(processedColumn, "CHECKED", processed.readOnly);	// Mark as processed
                Avo_LogDebug("Processed", 2);	//debug
                changesMade = true;
            }
            tableArray.push(rowArray);	// Save row
            continue;
        }

        if (endDate && alarmDate.getTime() > endDate.getTime()) {
            Avo_LogDebug("Activation is in future " + appType + " period", 2);

            if (today.getTime() <= endDate.getTime()) {
                Avo_LogDebug(appType + " hasn't yet expired. Not adding future fee", 2);    //debug
                continue;
            }

            totalFutureBillable = AddFees(totalFutureBillable, allFutureEvents, allFutureRemovedEvents, allFutureFeeArrays, recordId, altId, appType);
            Avo_LogDebug("All Future Events(" + allFutureEvents.join(", ") + ")", 2);  //debug
            continue;
        }

        Avo_LogDebug("Activation is in current " + appType + " period", 2);
        totalCurrentBillable = AddFees(totalCurrentBillable, allCurrentEvents, allCurrentRemovedEvents, allCurrentFeeArrays, recordId, altId, appType);

        Avo_LogDebug("All Current Events(" + allCurrentEvents.join(", ") + ")", 2);  //debug
    }

    Avo_LogDebug("", 2);	//debug
    Avo_LogDebug("Changes(" + String(changesMade) + ")", 2);	//debug

    if (changesMade != true) {
        return;
    }

    // Clear current table and replace with newly updated table
    try {
        removeASITable(tableName);
        addASITable(tableName, tableArray);
        Avo_LogDebug("Updated " + tableName + " table on record " + altId, 1);
    }
    catch (err) {
        Avo_LogDebug("Failed to update " + tableName + " table on record " + altId + ". " + err.message, 1);
    }

    var removedOutput = "";
    for (var a in allCurrentRemovedEvents) {
        removedOutput += a + ", ";
    }
    Avo_LogDebug("Current Removed Events(" + removedOutput + ")", 2);   //debug

    removedOutput = "";
    for (var a in allFutureRemovedEvents) {
        removedOutput += a + ", ";
    }
    Avo_LogDebug("Future Removed Events(" + removedOutput + ")", 2);   //debug

    // Current period
    RemoveAndReAddFees(totalCurrentBillable, allCurrentEvents, allCurrentRemovedEvents, allCurrentFeeArrays, recordId, altId, appType);

    if (endDate && today.getTime() > endDate.getTime()) {
        Avo_LogDebug("In future " + appType + " period", 2);
        RemoveAndReAddFees(totalFutureBillable, allFutureEvents, allFutureRemovedEvents, allFutureFeeArrays, recordId, altId, appType);
    }

    function AddFees(totalBillable, allEvents, allRemovedEvents, allFeeArrays, recordId, altId, appType) {
        totalBillable++;
        Avo_LogDebug("Billable(" + totalBillable + ")", 2);	//debug

        // Check if Event has matching fee
        Avo_LogDebug("All Events(" + allEvents.join(", ") + ")", 2);  //debug

        var eventIndex = allEvents.indexOf(eventId);
        Avo_LogDebug("Event(" + eventId + "), Index(" + eventIndex + ")", 2);	//debug

        if (eventIndex != -1) {
            Avo_LogDebug("Event " + eventId + " found in allEvents", 2);   //debug
            delete allRemovedEvents[eventId];// remove from events to remove
        } else {
            Avo_LogDebug("Event " + eventId + " could not be found in allEvents", 2);  //debug
            processedEntry = "UNCHECKED";   // Fee is missing, not actually processed
        }

        // Check for changes
        if (processedEntry == "CHECKED") {
            tableArray.push(rowArray);	// Save row
            return totalBillable;
        }

        // Process entry
        rowArray[processedColumn] = new asiTableValObj(processedColumn, "CHECKED", processed.readOnly);	// Mark as processed
        Avo_LogDebug("Processed", 2);	//debug
        changesMade = true;
        tableArray.push(rowArray);	// Save row

        // Check if fee has already been added
        if (eventIndex != -1) {
            return totalBillable;
        }

        // Add fee
        var index = null;

        // 1-2 activations
        if (totalBillable < 3) {
            index = 0;
        }

        // 3rd activation
        if (totalBillable == 3) {
            index = 1;
        }

        // 4th activation
        if (totalBillable == 4) {
            index = 2;
        }

        // 5-6 activations
        if (totalBillable > 4 && totalBillable < 7) {
            index = 3;
        }

        // 7+ activations
        if (totalBillable >= 7) {
            index = 4;
        }

        if (index == null) {
            Avo_LogDebug("Invalid operation", 1);
            return totalBillable;
        }

        var feeCode = allFeeCodes[index];
        var feeArray = allFeeArrays[index];
        Avo_LogDebug(feeCode + "(" + Object.keys(feeArray).length + ")", 2);	//debug

        if (index == 0 && totalBillable <= Object.keys(feeArray).length) {
            Avo_LogDebug(feeCode + " has already been added " + String(Object.keys(feeArray).length) + " times to " + appType + " " + altId, 1);
            return totalBillable;
        }
        if (index == 1 && Object.keys(feeArray) > 0) {
            Avo_LogDebug(feeCode + " has already been added to " + appType + " " + altId, 1);
            return totalBillable;
        }
        if (index == 2 && Object.keys(feeArray) > 0) {
            Avo_LogDebug(feeCode + " has already been added to " + appType + " " + altId, 1);
            return totalBillable;
        }
        if (index == 3 && (totalBillable - 4) <= Object.keys(feeArray).length) {
            return totalBillable;
        }
        if (index == 4 && (totalBillable - 6) <= Object.keys(feeArray).length) {
            return totalBillable;
        }

        var feeId = feeId = Avo_AddFeeOnDateWithComment(feeCode, feeSched, "FINAL", 1, "Y", alarmDate, eventId, recordId);
        Avo_LogDebug('Added and invoiced fee ' + feeCode + ' from schedule ' + feeSched + ' to ' + appType + ' ' + altId, 1);

        var eventFeeObj = { eventId: note, feeId: feeId, alarmDate: alarmDate };
        feeArray[note] = eventFeeObj;
        allEvents.push(eventId);
        return totalBillable
    }

    function AddUnpermittedFee(alarmDate, eventId, recordId, altId, appType) {
        if (appType == "Permit") {
            var b1ExpResults = aa.expiration.getLicensesByCapID(recordId);
            if (b1ExpResults.getSuccess() != true) {
                Avo_LogDebug("Failed to get expiration info for " + appType + " " + altId + ". " + b1ExpResults.errorType + ": " + b1ExpResults.errorMessage, 1);
                return;
            }

            var b1Exp = b1ExpResults.getOutput();
            var b1ExpModel = b1Exp.getB1Expiration();
            if (!b1ExpModel) {
                Avo_LogDebug("Failed to get expiration model for " + appType + " " + altId, 1);
                return;
            }

            var expStatus = b1ExpModel.getExpStatus();
            Avo_LogDebug("Exp Status(" + expStatus + ")", 2);   //debug

            if (expStatus == "Expired") {
                Avo_LogDebug(AppType + " is expired. Manual intervention required to process", 1);
                return;
            }

            var expiration = b1ExpModel.getExpDate();
            var expDate = new Date(expiration.time);
            Avo_LogDebug("Exp date(" + aa.util.formatDate(expDate, "MM/dd/yyyy") + ")", 2);	//debug

            var expDatePlus120 = new Date(expDate.getTime());
            expDatePlus120.setDate(expDatePlus120.getDate() + 120);

            var today = Avo_GetToday();

            if (today.getTime() < expDate.getTime()) {
                return;
            }
            if (today.getTime > expDatePlus120.getTime()) {
                return;
            }

            Avo_LogDebug(appType + " is within 120 day renewal period", 1);
        }

        Avo_LogDebug("Activation on " + appType + " " + altId + " is unpermitted", 1);

        var feeCode = "ALUUNP";
        var capIdToStore = capId;
        capId = recordId;

        var alreadyAdded = feeExists(feeCode);
        if (alreadyAdded == true) {
            return;
        }

        capId = capIdToStore;

        var feeSched = aa.finance.getFeeScheduleByCapID(recordId).getOutput();
        var feeId = Avo_AddFeeOnDateWithComment(feeCode, feeSched, "FINAL", 1, "Y", alarmDate, eventId, recordId);
        Avo_LogDebug('Added and invoiced fee ' + feeCode + ' from schedule ' + feeSched + ' to ' + appType + ' ' + altId, 1);
    }

    function IsFeePaid(feeCode, recordId, altId, appType) {
        var feeTotal = 0;
        var feeIds = new Object();

        var result = aa.fee.getFeeItems(recordId, feeCode, null);
        if (result.getSuccess() != true) {
            Avo_LogDebug("Failed to find fee " + feeCode + " on " + appType + " " + altId, 1);
            return false;
        }

        var feeItems = result.getOutput();
        if (feeItems.length == 0) {
            return false;
        }

        for (var i in feeItems) {
            var feeAmount = feeItems[i].fee;
            var feeId = feeItems[i].feeSeqNbr;
            Avo_LogDebug(feeId + "($" + feeAmount + ")", 2);    //debug

            feeTotal += feeAmount;
            feeIds[feeId] = feeAmount;
        }

        var paidTotal = 0;
        result = aa.finance.getPaymentFeeItems(recordId, aa.util.newQueryFormat());
        if (result.getSuccess() != true) {
            Avo_LogDebug("Failed to find any payments on " + appType + " " + altId, 1);
            return false;
        }

        var paymentItems = result.getOutput();
        for (var i in paymentItems) {
            var feeIdToCheck = paymentItems[i].feeSeqNbr;
            Avo_LogDebug("Payment Fee ID(" + feeIdToCheck + ")", 2);    //debug

            if (!(feeIdToCheck in feeIds)) {
                continue;
            }

            paidTotal += paymentItems[a].feeAllocation;
        }

        Avo_LogDebug("Paid Total($" + paidTotal + ")", 2);  //debug

        return paidTotal >= feeTotal;
    }

    function RemoveAndReAddFees(totalBillable, allEvents, allRemovedEvents, allFeeArrays, recordId, altId, appType) {
        Avo_LogDebug("Total Removed(" + Object.keys(allRemovedEvents).length + ")", 2); //debug
        if (Object.keys(allRemovedEvents).length == 0) {
            return;
        }

        Avo_LogDebug("Billable(" + totalBillable + ")", 2);	//debug

        var act200 = Object.keys(allFeeArrays[4]).length;
        Avo_LogDebug("Act200(" + act200 + ")", 2);	//debug

        if (totalBillable - 6 == act200) {
            Avo_LogDebug("No excess fees", 2);	//debug
            return;
        }

        var allActsToReAdd = new Object();

        // Remove excess fees
        for (i = 0; i < allFeeArrays.length; i++) {
            var feeArray = allFeeArrays[i];

            for (eventId in allRemovedEvents) {
                var eventFeeObj = feeArray[eventId];
                if (typeof (eventFeeObj) == "undefined") {
                    continue;
                }

                var feeId = eventFeeObj.feeId;
                Avo_LogDebug("Fee Id(" + feeId + ")", 2);	// debug

                var feeGroupCode = allFeeCodes[i];
                Avo_LogDebug("Fee Group Code(" + feeGroupCode + ")", 2);	//debug

                // Get fee details
                var result = aa.fee.getFeeItemByPK(capId, feeId);
                if (result.getSuccess() != true) {
                    Avo_LogDebug("Unable to get fee item " + feeId + " on record " + altId, 1);
                    continue;
                }

                var feeItem = result.getOutput();
                var feeItemModel = feeItem.f4FeeItem

                var feeCodeToCheck = feeItem.feeCod;
                if (feeCodeToCheck != feeGroupCode) {
                    Avo_LogDebug('Error: Fee code "' + feeCodeToCheck + '" associated with event "' + eventId + '" does not match "' + feeGroupCode + '" on record ' + altId, 1);
                    continue;
                }

                var eventIdToCheck = feeItemModel.feeNotes;
                if (eventIdToCheck != eventId) {
                    Avo_LogDebug('Error: Event id "' + eventIdToCheck + '" associated with fee "' + feeGroupCode + '" ' + feeId + ' does not match "' + eventId + '" on record ' + altId, 1);
                    continue;
                }

                // Credit fee
                var isCredited = Avo_CreditFee(feeId);
                if (isCredited == false) {
                    Avo_LogDebug('Failed to credit fee "' + feeId + '" on record ' + altId, 1);
                    return;
                }

                // Remove from fee array
                delete feeArray[eventId];

                // Remove all proceeding fees as well but save event ids
                for (j = i + 1; j < allFeeArrays.length; j++) {
                    var nextFeeArray = allFeeArrays[j];
                    for (a in nextFeeArray) {
                        var nextEventFeeObj = nextFeeArray[a];
                        var nextEventId = nextEventFeeObj.eventId;
                        var nextAlarmDate = nextEventFeeObj.alarmDate;

                        // Check if event is already queued to be removed
                        if (nextEventId in allRemovedEvents) {
                            continue;
                        }

                        allRemovedEvents[nextEventId] = nextEventId;
                        allActsToReAdd[nextEventId] = nextAlarmDate;
                    }
                }

                removedOutput = "";
                for (a in allRemovedEvents) {
                    removedOutput += a + ", ";
                }
                Avo_LogDebug("All Removed Events(" + removedOutput + ")", 2);   //debug

                // Remove current event from array
                delete allRemovedEvents[eventId];
                Avo_LogDebug("Removed event(" + eventId + ")", 2);	//debug

                removedOutput = "";
                for (a in allRemovedEvents) {
                    removedOutput += a + ", ";
                }
                Avo_LogDebug("All Removed Events(" + removedOutput + ")", 2);   //debug
            }
        }

        // Readd fees for events
        readdOutput = "";
        for (var a in allActsToReAdd) {
            readdOutput += a + ", ";
        }
        Avo_LogDebug("All Events to Readd(" + readdOutput + ")", 2);   //debug

        var totalMissingFees = Object.keys(allActsToReAdd).length;
        var nextAct = totalBillable - totalMissingFees + 1;
        Avo_LogDebug("Next(" + nextAct + "), Total(" + totalMissingFees + ")", 2);	//debug

        for (eventId in allActsToReAdd) {
            // Get fee code
            var index = null;

            // 1-2 activations
            if (nextAct < 3) {
                index = 0;
            }

            // 3rd activation
            if (nextAct == 3) {
                index = 1;
            }

            // 4th activation
            if (nextAct == 4) {
                index = 2;
            }

            // 5-6 activations
            if (nextAct > 4 && nextAct < 7) {
                index = 3;
            }

            // 7+ activations
            if (nextAct >= 7) {
                index = 4;
            }

            if (index == null) {
                Avo_LogDebug("Invalid operation", 1);
                continue;
            }

            var feeCode = allFeeCodes[index];
            var feeArray = allFeeArrays[index];
            Avo_LogDebug(feeCode + "(" + Object.keys(feeArray).length + ")", 2);	//debug

            if (index == 0 && totalBillable <= Object.keys(feeArray).length) {
                Avo_LogDebug(feeCode + " has already been added " + String(Object.keys(feeArray).length) + " times to " + appType + " " + altId, 1);
                continue;
            }
            if (index == 1 && Object.keys(feeArray) > 0) {
                Avo_LogDebug(feeCode + " has already been added to " + appType + " " + altId, 1);
                continue;
            }
            if (index == 2 && Object.keys(feeArray) > 0) {
                Avo_LogDebug(feeCode + " has already been added to " + appType + " " + altId, 1);
                continue;
            }
            if (index == 3 && (totalBillable - 4) <= Object.keys(feeArray).length) {
                continue;
            }
            if (index == 4 && (totalBillable - 6) <= Object.keys(feeArray).length) {
                continue;
            }

            // Add fee
            var applyDate = allActsToReAdd[eventId];
            Avo_LogDebug("Date to ReAdd(" + aa.util.formatDate(applyDate, "MM/dd/yyyy") + ")", 2);  //debug

            var feeSched = aa.finance.getFeeScheduleByCapID(recordId).getOutput();
            var feeId = Avo_AddFeeOnDateWithComment(feeCode, feeSched, "FINAL", 1, "Y", applyDate, eventId, recordId);
            Avo_LogDebug('Added and invoiced fee ' + feeCode + ' from schedule ' + feeSched + ' to ' + appType + ' ' + altId, 1);

            var eventFeeObj = { eventId: eventId, feeId: feeId, alarmDate: applyDate };
            feeArray[note] = eventFeeObj;

            nextAct++;
        }
    }
}