/*******************************************************
| Script Title: Batch_ExportSmartstreamData (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 3Apr18
| Usage: For each row in Accela ACCOUNTING_AUDIT_TRAIL table add a line to file expSmartstreamyyyyMMdd_Hmmss.csv
| Modified by: Nic Bunting (8Aug19)
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
//aa.env.setValue("sftpUrl", "172.19.61.35");
//aa.env.setValue("sftpPort", 21);
//aa.env.setValue("sftpFolder", "test");
//aa.env.setValue("sftpFolder", "Production");
//aa.env.setValue("username", "AccelaFTP");
//aa.env.setValue("password", "txfr1234!");
//aa.env.setValue("tranDate", '03/22/2018');
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var url = aa.env.getValue("sftpUrl");
//var port = aa.env.getValue("sftpPort");
var sftpFolder = aa.env.getValue("sftpFolder");
//var username = aa.env.getValue("username");
//var password = aa.env.getValue("password");
var tranDate = aa.env.getValue("tranDate");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");
var currentUserID = "ADMIN";

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

// Print debug using aa.print instead of aa.debug
useLogDebug = false;

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    aa.print("Batch Job " + batchJobName + " Job ID is " + batchJobID);
}
else {
    aa.print("Batch job ID not found " + batchJobResult.getErrorMessage());
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    aa.print("Start of Job");

    if (!timeExpired) {
        try {
            exportSmartstreamData(sftpFolder, tranDate);
        }
        catch (e) {
            aa.print("Error in process " + e.message);
        }
    }
    else {
        aa.print("End of Job: Elapsed Time : " + elapsed() + " Seconds");
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

function exportSmartstreamData(sftpFolder, tranDate) {
    Avo_LogDebug("exportSmartstreamData(" + sftpFolder + ", " + tranDate + ")", 1);

    var exportData = "";

    var today = new Date();
    var year = today.getFullYear().toString();
    var month = String(today.getMonth() + 1).length < 2 ? "0" + String(today.getMonth() + 1) : String(today.getMonth() + 1);
    var day = today.getDate() < 9 ? "0" + today.getDate().toString() : today.getDate().toString();
    var hour = String(today.getHours());
    var mins = (today.getMinutes() < 10 ? "0" + today.getMinutes().toString() : today.getMinutes().toString());
    var secs = (today.getSeconds() < 10 ? "0" + today.getSeconds().toString() : today.getSeconds().toString());

    var filename = "expSmartstream" + year + month + day + "_" + hour + mins + secs + ".csv";
    Avo_LogDebug("Filename(" + filename + ")", 2);    //debug

    var dateStr = year + "/" + month + "/" + day;
    if (String(tranDate).length > 0) {
        var tranDateArr = String(tranDate).split("/");
        dateStr = tranDateArr[2] + "/" + tranDateArr[0] + "/" + tranDateArr[1];
    }

    Avo_LogDebug("Getting all transactions for " + dateStr, 1);

    // Get Accounting Audit data
    var selectString = "SELECT * "
        + "FROM ACCOUNTING_AUDIT_TRAIL "
        + "Where SERV_PROV_CODE = 'SCOTTSDALE' "
		+ "And TRAN_DATE Between TO_DATE('" + dateStr + " 12:00:00', 'YYYY/MM/DD HH24:MI:SS') And TO_DATE('" + dateStr + " 23:59:59', 'YYYY/MM/DD HH24:MI:SS')";

    try {
        var initialContext = aa.proxyInvoker.newInstance("javax.naming.InitialContext", null).getOutput();
        var ds = initialContext.lookup("java:/AA");
        var conn = ds.getConnection();
        var SQLStatement = conn.prepareStatement(selectString);
        var rSet = SQLStatement.executeQuery();

        while (rSet.next()) {
            for (var i = 1; i < 49; i++) {
                if (i != 1) {
                    exportData += ",";
                }

                var value = String(rSet.getString(i));
                //if (value.indexOf(",") != -1 || value.indexOf('"') != -1) {
                value = '"' + value + '"';  // Wrap in quotes to maintain as literal
                //}

                exportData += value;
            }

            exportData += "\n";
        }
    }
    catch (e) {
        Avo_LogDebug("Exception getting data from ACCOUNTING_AUDIT_TRAIL. " + e.message, 1);
    }
    finally {
        initialContext.close();
        SQLStatement.close();
        if (rSet) {
            rSet.close();
        }
        conn.close();
    }

    Avo_LogDebug("Data(" + exportData + ")", 2);    //debug

    if (!exportData || exportData.length == 0) {
        Avo_LogDebug("No data to export", 1);
        return;
    }

    //// Write export data to file
    //var path = "C://Temp/";
    //var file = null;

    //try {
    //    file = aa.util.writeToFile(exportData, path + filename);
    //} catch (ex) {
    //    Avo_LogDebug("Failed to create export file. " + ex.message, 1);
    //    return;
    //}

    //if (!file) {
    //    Avo_LogDebug("Unable to create export file", 1);
    //    return;
    //}

    //// Upload file to FTP
    //var result = aa.util.ftpUpload(ftpSite, port, username, password, sftpFolder, file);
    //if (result.getSuccess()) {
    //    Avo_LogDebug("Uploaded " + filename + " to " + ftpSite + ":" + port.toString() + " in directory " + sftpFolder, 1);
    //} else {
    //    Avo_LogDebug("Failed to upload " + filename + " to " + ftpSite + ":" + port.toString() + ". " + result.errorMessage, 1);
    //}

    // Upload Export data
    include("EMSE:SFTPADAPTERAPI");
    eval(getScriptText("EMSE:SFTPADAPTERAPI"));

    adapterToken = getToken(adapterUsername, adapterPassword);
    Avo_LogDebug("Token(" + adapterToken + ")", 2);	//debug

    if (!adapterToken) {
        Avo_LogDebug("Failed to get token from SFTP Adapter @ " + adapterUrl, 1);
        return;
    }

    var success = saveData(sftpFolder, filename, exportData);
    Avo_LogDebug("Upload Success(" + success + ")", 2); //debug
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}