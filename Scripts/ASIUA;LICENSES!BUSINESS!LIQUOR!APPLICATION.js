//ASIUA: Licenses/Business/Liquor/Application

//call script ** LIQ03 ** script/functions based on requirements
Avo_AddPermIssFees();

/*******************************************************
| Script/Function: Avo_AddPermIssFees() - (LIQ03)
| Created by: Nicolaj Bunting
| Created on: 10Jan18
| Usage: If ASI "State Approval Date" isn't empty Then if ASI "Interim Permit" is checked then use ASI "Interim Permit End Date" to prorate license fee 
| else use ASI "State Approval Date" to prorate license fee, get number prefix from ASI "State Series #" and add and invoice fee prefix + "-I" from schedule "LIC_LIQ_LIC" 
| and add and invoice fee prefix from schedule "LIC_LIQ_LIC" with quantity depending on proration
| Modified by: Nic Bunting (15Mar18)
*********************************************************/
function Avo_AddPermIssFees() {
    logDebug("LIQ03 Avo_AddPermIssFees()");

    var apprDate = getAppSpecific("State Approval Date", capId);
    if (!apprDate) {
        return;
    }

    var quantity = 0;
    var prorationDate = Avo_GetDateFromAccelaDateString(apprDate);

    var interimPermit = getAppSpecific("Interim Permit", capId) == "CHECKED";
    if (interimPermit == true) {
        var interimEnd = getAppSpecific("Interim Permit End Date", capId);
        prorationDate = Avo_GetDateFromAccelaDateString(interimEnd);
        quantity = 0.25;
    }

    var stateSeries = getAppSpecific("State Series #", capId);
    logDebug("#(" + stateSeries + ")"); //debug

    var prefix = String(stateSeries).substring(0, 2);
    if (isNaN(prefix) == true) {
        prefix = "0" + prefix.substring(0, 1);
    }

    var invoiceFee = Avo_ShouldLicFeeInvoice(prefix);
    Avo_LogDebug("Invoice fee(" + invoiceFee + ")", 2); //debug

    var feeCode = prefix;
    logDebug("Code(" + feeCode + ")");  //debug

    // Permit fee
    quantity += Avo_GetQuarterPercentage(aa.util.formatDate(prorationDate, "MM/dd/yyyy"));
    updateFee(feeCode, "LIC_LIQ_LIC", "FINAL", quantity, invoiceFee, "Y");
    logDebug("Added fee " + feeCode + " from schedule LIC_LIQ_LIC with quantity " + quantity.toString());

    // Issuance fee
    updateFee(feeCode + "-I", "LIC_LIQ_LIC", "FINAL", 1, invoiceFee, "Y");
    logDebug("Added fee " + feeCode + "-I from schedule LIC_LIQ_LIC");
}