//ASIUA: Licenses/Business/Alarm User/*

//call script ** ID83, ID84, ID85 ** script/functions based on requirements
UpdateActivationFees();

/*******************************************************
| Script/Function: UpdateActivationFees() - (ID83, ID84, ID85)
| Created by: Nicolaj Bunting
| Created on: 4May18
| Usage: Adjust the number of alarm activation fees invoiced based on the entries in ASIT "Activations"
| Modified by: Nic Bunting (16May18)
*********************************************************/
function UpdateActivationFees() {
    Avo_LogDebug("ID83, ID84, ID85 UpdateActivationFees()", 1);

    var isApp = appMatch("Licenses/Business/Alarm User/Application", capId);
    var isPermit = appMatch("Licenses/Business/Alarm User/Permit", capId);
    if (!isApp && !isPermit) {
        return;
    }

    eval(getScriptText("Avo_UpdateAlarmActFees"));
    Avo_UpdateAlarmActFees(capId);
}