/*******************************************************
| Script Title: Batch_AddActivationFees (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 20Apr18
| Usage: For all records of type "Licenses/Business/Alarm User/Permit" adjust the number of alarm activation fees invoiced based on the entries in ASIT "Activations"
| Modified by: Nic Bunting (24Aug18)
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
//aa.env.setValue("param1","Yes");
//aa.env.setValue("param1","No");
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var param1 = aa.env.getValue("param1");
//var param2 = aa.env.getValue("param2");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

// Print debug using aa.print instead of aa.debug
useLogDebug = false;

// Set the system user
var result = aa.person.getUser("ADMIN");
if (result.getSuccess() != true) {
    Avo_LogDebug("Failed to get sys user ADMIN. " + result.errorMessage, 1);
}

systemUserObj = result.getOutput();

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    aa.print("Batch Job " + batchJobName + " Job ID is " + batchJobID);
}
else {
    aa.print("Batch job ID not found " + batchJobResult.getErrorMessage());
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    aa.print("Start of Job");

    if (!timeExpired) {
        try {
            AddActivationFees();
        }
        catch (e) {
            aa.print("Error in process " + e.message);
        }
    }
    else {
        aa.print("End of Job: Elapsed Time : " + elapsed() + " Seconds");
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

function AddActivationFees() {
    Avo_LogDebug("ALU02 AddActivationFees()", 1);

    // Get all Alarm user permits
    var group = "Licenses";
    var type = "Business";
    var subType = "Alarm User";
    var category = "Permit";
    var appsArray = aa.cap.getByAppType(group, type, subType, category).getOutput();
    if (appsArray.length == 0) {
        Avo_LogDebug('No records of type "' + group + "/" + type + "/" + subType + "/" + category + '" found', 1);
        return;
    }

    for (a in appsArray) {
        capId = appsArray[a].getCapID();
        var altId = aa.cap.getCap(capId).getOutput().getCapModel().altID;
        Avo_LogDebug("CapId(" + altId + ")", 2); //debug

        eval(getScriptText("Avo_UpdateAlarmActFees"));
        Avo_UpdateAlarmActFees(capId);
    }
}

function Avo_UpdateFeeWithComment(fcode, fsched, fperiod, fqty, finvoice, pDuplicate, pFeeSeq, fcomment, fCapId) {
    if (pDuplicate == null || pDuplicate.length == 0) {
        pDuplicate = true;
    } else {
        pDuplicate = pDuplicate.toUpperCase() == "Y";
    }

    var invFeeFound = false;
    var adjustedQty = fqty;
    var feeSeq = null;
    var feeUpdated = false;

    var getFeeResult;
    if (pFeeSeq == null) {
        getFeeResult = aa.finance.getFeeItemByFeeCode(capId, fcode, fperiod);
    } else {
        getFeeResult = aa.finance.getFeeItemByPK(capId, pFeeSeq);
    }

    if (getFeeResult.getSuccess() == false) {
        Avo_LogDebug("**ERROR: getting fee items (" + fcode + "): " + getFeeResult.getErrorMessage(), 1);
    } else {
        if (pFeeSeq == null)
            var feeList = getFeeResult.getOutput();
        else {
            var feeList = new Array();
            feeList[0] = getFeeResult.getOutput();
        }

        for (feeNum in feeList) {
            var feeStatus = feeList[feeNum].getFeeitemStatus();
            if (feeStatus.equals("INVOICED")) {
                if (pDuplicate == true) {
                    Avo_LogDebug("Invoiced fee " + fcode + " found, subtracting invoiced amount from update qty.", 1);
                    adjustedQty = adjustedQty - feeList[feeNum].getFeeUnit();
                } else {
                    Avo_LogDebug("Invoiced fee " + fcode + " found.  Not updating this fee. Not assessing new fee " + fcode, 1);
                }

                invFeeFound = true;
            }

            if (feeStatus.equals("NEW")) {
                adjustedQty = adjustedQty - feeList[feeNum].getFeeUnit();
            }
        }

        for (feeNum in feeList) {
            if (!feeList[feeNum].getFeeitemStatus().equals("NEW")) {
                continue;
            }
            if (feeUpdated == true) {
                continue;
            }

            // update this fee item
            var feeSeq = feeList[feeNum].getFeeSeqNbr();
            var editResult = aa.finance.editFeeItemUnit(capId, adjustedQty + feeList[feeNum].getFeeUnit(), feeSeq);
            feeUpdated = true;
            if (editResult.getSuccess()) {
                Avo_LogDebug("Updated Qty on Existing Fee Item: " + fcode + " to Qty: " + fqty, 1);
                if (finvoice == "Y") {
                    feeSeqList.push(feeSeq);
                    paymentPeriodList.push(fperiod);
                }
            } else {
                Avo_LogDebug("**ERROR: updating qty on fee item (" + fcode + "): " + editResult.getErrorMessage(), 1);
                break
            }
        }
    }

    // Add fee if no fee has been updated OR invoiced fee already exists and duplicates are allowed
    if (!feeUpdated && adjustedQty != 0 && (!invFeeFound || invFeeFound && pDuplicate == "Y")) {
        //feeSeq = addFee(fcode, fsched, fperiod, adjustedQty, finvoice);
        feeSeq = addFeeWithExtraData(fcode, fsche, fperiod, adjustedQty, finvoice, fCapId, fcomment);
    } else {
        feeSeq = null;
    }

    updateFeeItemInvoiceFlag(feeSeq, finvoice);
    return feeSeq;
}

function updateFee(fcode, fsched, fperiod, fqty, finvoice, pDuplicate, pFeeSeq) {
    // Updates an assessed fee with a new Qty.  If not found, adds it; else if invoiced fee found, adds another with adjusted qty.
    // optional param pDuplicate -if "N", won't add another if invoiced fee exists (SR5085)
    // Script will return fee sequence number if new fee is added otherwise it will return null (SR5112)
    // Optional param pSeqNumber, Will attempt to update the specified Fee Sequence Number or Add new (SR5112)
    // 12/22/2008 - DQ - Correct Invoice loop to accumulate instead of reset each iteration
    // If optional argument is blank, use default logic (i.e. allow duplicate fee if invoiced fee is found)
    if (pDuplicate == null || pDuplicate.length == 0)
        pDuplicate = "Y";
    else
        pDuplicate = pDuplicate.toUpperCase();
    var invFeeFound = false;
    var adjustedQty = fqty;
    var feeSeq = null;
    feeUpdated = false;
    if (pFeeSeq == null)
        getFeeResult = aa.finance.getFeeItemByFeeCode(capId, fcode, fperiod);
    else
        getFeeResult = aa.finance.getFeeItemByPK(capId, pFeeSeq);
    if (getFeeResult.getSuccess()) {
        if (pFeeSeq == null)
            var feeList = getFeeResult.getOutput();
        else {
            var feeList = new Array();
            feeList[0] = getFeeResult.getOutput();
        }
        for (feeNum in feeList) {
            if (feeList[feeNum].getFeeitemStatus().equals("INVOICED")) {
                if (pDuplicate == "Y") {
                    Avo_LogDebug("Invoiced fee " + fcode + " found, subtracting invoiced amount from update qty.", 1);
                    adjustedQty = adjustedQty - feeList[feeNum].getFeeUnit();
                    invFeeFound = true;
                } else {
                    invFeeFound = true;
                    Avo_LogDebug("Invoiced fee " + fcode + " found.  Not updating this fee. Not assessing new fee " + fcode, 1);
                }
            }
            if (feeList[feeNum].getFeeitemStatus().equals("NEW")) {
                adjustedQty = adjustedQty - feeList[feeNum].getFeeUnit();
            }
        }
        for (feeNum in feeList)
            if (feeList[feeNum].getFeeitemStatus().equals("NEW") && !feeUpdated) // update this fee item
            {
                var feeSeq = feeList[feeNum].getFeeSeqNbr();
                var editResult = aa.finance.editFeeItemUnit(capId, adjustedQty + feeList[feeNum].getFeeUnit(), feeSeq);
                feeUpdated = true;
                if (editResult.getSuccess()) {
                    Avo_LogDebug("Updated Qty on Existing Fee Item: " + fcode + " to Qty: " + fqty, 1);
                    if (finvoice == "Y") {
                        feeSeqList.push(feeSeq);
                        paymentPeriodList.push(fperiod);
                    }
                } else {
                    Avo_LogDebug("**ERROR: updating qty on fee item (" + fcode + "): " + editResult.getErrorMessage(), 1);
                    break
                }
            }
    } else {
        Avo_LogDebug("**ERROR: getting fee items (" + fcode + "): " + getFeeResult.getErrorMessage(), 1)
    }
    // Add fee if no fee has been updated OR invoiced fee already exists and duplicates are allowed
    if (!feeUpdated && adjustedQty != 0 && (!invFeeFound || invFeeFound && pDuplicate == "Y"))
        feeSeq = addFee(fcode, fsched, fperiod, adjustedQty, finvoice);
    else
        feeSeq = null;
    updateFeeItemInvoiceFlag(feeSeq, finvoice);
    return feeSeq;
}

function addFeeWithExtraData(fcode, fsched, fperiod, fqty, finvoice, feeCap, feeComment, UDF1, UDF2) {
    var feeCapMessage = "";
    var feeSeq_L = new Array(); // invoicing fee for CAP in args
    var paymentPeriod_L = new Array(); // invoicing pay periods for CAP in args
    assessFeeResult = aa.finance.createFeeItem(feeCap, fsched, fcode, fperiod, fqty);
    if (assessFeeResult.getSuccess()) {
        feeSeq = assessFeeResult.getOutput();
        logMessage("Successfully added Fee " + fcode + ", Qty " + fqty + feeCapMessage);
        Avo_LogDebug("The assessed fee Sequence Number " + feeSeq + feeCapMessage, 1);
        fsm = aa.finance.getFeeItemByPK(feeCap, feeSeq).getOutput().getF4FeeItem();
        if (feeComment) fsm.setFeeNotes(feeComment);
        if (UDF1) fsm.setUdf1(UDF1);
        if (UDF2) fsm.setUdf2(UDF2);
        aa.finance.editFeeItem(fsm)
        if (finvoice == "Y" && arguments.length == 5) // use current CAP
        {
            feeSeqList.push(feeSeq);
            paymentPeriodList.push(fperiod);
        }
        if (finvoice == "Y" && arguments.length > 5) // use CAP in args
        {
            feeSeq_L.push(feeSeq);
            paymentPeriod_L.push(fperiod);
            var invoiceResult_L = aa.finance.createInvoice(feeCap, feeSeq_L, paymentPeriod_L);
            if (invoiceResult_L.getSuccess())
                logMessage("Invoicing assessed fee items is successful.");
            else
                Avo_LogDebug("**ERROR: Invoicing the fee items assessed was not successful.  Reason: " + invoiceResult.getErrorMessage(), 1);
        }
    } else {
        Avo_LogDebug("**ERROR: assessing fee (" + fcode + "): " + assessFeeResult.getErrorMessage(), 1);
        return null;
    }
    return feeSeq;
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}