/*
 * DJ 10/30/2017
 * WorkFlow task for Renewal
 *
 */

logDebug("Renew Test Script for Auctioneer");

if (currentUserID == "ADMIN")
	showDebug = true;

if ((wfTask == "Renewal") &&
	((wfStatus == "Pending") || (wfStatus == "Additional Info Required")))

{
	aa.print("We have a status of Pending or Additional Info Required.");
	var renOwnerShipType = getAppSpecific("Ownership Type");
	aa.print(renOwnerShipType);
	editAppSpecific("Ownership Type", "Sole Proprietor",capId);
	renOwnerShipType = getAppSpecific("Ownership Type");
	aa.print(renOwnerShipType);
	
}