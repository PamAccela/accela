//ASA: Licenses/Business/Valet Parking Special Event/Application

//call script ** EVP01 ** script/functions based on requirements
Avo_ValetParkingEventFee();

/*******************************************************
| Script/Function: Avo_ValetParkingEventFee() - (EVP01)
| Created by: Nicolaj Bunting
| Created on: 18Jan18
| Usage: Add and invoice fee "EVPLIC" from schedule "LIC_EVP_LIC" with quanitity of the number of days between ASI "Requested Event Start Date" and "Requested Event End Date"
| Modified by: ()
*********************************************************/
function Avo_ValetParkingEventFee() {
    logDebug("EVP01 Avo_ValetParkingEventFee()");

    var start = getAppSpecific("Requested Event Start Date", capId);
    if (!start) {
        return;
    }

    var startDate = Avo_GetDateFromAccelaDateString(start);
    Avo_LogDebug("Start Date(" + aa.util.formatDate(startDate, "MM/dd/yyyy") + ")", 2);    //debug

    var end = getAppSpecific("Requested Event End Date", capId);
    if (!end) {
        return;
    }

    var endDate = Avo_GetDateFromAccelaDateString(end);
    Avo_LogDebug("End Date(" + aa.util.formatDate(endDate, "MM/dd/yyyy") + ")", 2);    //debug

    var timeDiff = endDate.getTime() - startDate.getTime();
    var totalDays = Math.round(timeDiff / (24 * 60 * 60 * 1000)) + 1;
    Avo_LogDebug("Days(" + totalDays + ")", 2);    //debug

    addFee("EVPLIC", "LIC_EVP_LIC", "FINAL", totalDays, "Y", capId);
    Avo_LogDebug("Added fee EVPLIC from schedule LIC_EVP_LIC with quantity of " + totalDays.toString(), 1);
}