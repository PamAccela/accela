/*******************************************************
| Script Title: Batch_ExportAccelaARData (Batch Script)
| Created by: Nicolaj Bunting
| Created on: 27Jan18
| Usage: For each record get all invoices and add a line to file expARAccelayyyyMMdd_Hmmss.csv including the alt id, 
| organization name, first and last name of Applicant, address line 1, Applicant's business phone, balance owning on invoice, invoice #
| Modified by: Nic Bunting (28Jan19)
| Modified by: Nic Bunting (2May19)
| Modified by: Nic Bunting (8Aug19)
| Modified by: Nic Bunting (20Aug19)
*********************************************************/
/* ***************************************************************************************************************************
 IMPORTANT NOTE: IF USING COMMIT() - To test the script, it must be executed by setting the Script Transaction drop down to "Use User Transaction"
****************************************************************************************************************************/

/*------------------------------------------------------------------------------------------------------/
| START: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 3.0;

var showDebug = true;
//var showMessage = false;
//var message = "";
var debug;
var emailText;
//var maxSeconds = 4.5 * 60;
var br = "<br/>";
var startDate = new Date();
var startTime = startDate.getTime(); // Start timer
var systemUserObj = aa.person.getUser("ADMIN").getOutput();
//Validate workflow parameters
var paramsOK = true;
var timeExpired = false;
var useAppSpecificGroupName = false;
// Set time out to 60 minutes
var timeOutInSeconds = 60 * 60;
/*------------------------------------------------------------------------------------------------------/
| END: USER CONFIGURABLE PARAMETERS
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START: TEST DATA
/------------------------------------------------------------------------------------------------------*/
//aa.env.setValue("sftpUrl", "172.19.61.35");
//aa.env.setValue("sftpPort", 21);
//aa.env.setValue("sftpFolder", "test");  // testing
//aa.env.setValue("username", "AccelaFTP");
//aa.env.setValue("password", "txfr1234!");
/*------------------------------------------------------------------------------------------------------/
| END: TEST DATA
/------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------/
| Start: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
//var url = aa.env.getValue("sftpUrl");
//var port = aa.env.getValue("sftpPort");
var sftpFolder = aa.env.getValue("sftpFolder");
//var username = aa.env.getValue("username");
//var password = aa.env.getValue("password");
/*----------------------------------------------------------------------------------------------------/
| End: BATCH PARAMETERS
/------------------------------------------------------------------------------------------------------*/
sysDate = aa.date.getCurrentDate();
batchJobResult = aa.batchJob.getJobID()
batchJobName = "" + aa.env.getValue("BatchJobName");
var currentUserID = "ADMIN";

eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS"));
eval(getScriptText("INCLUDES_ACCELA_GLOBALS"));
eval(getScriptText("INCLUDES_CUSTOM"));
eval(getScriptText("INCLUDES_CUSTOM_GLOBALS"));
//eval(getScriptText("INCLUDES_BATCH"));

// Print debug using aa.print instead of aa.debug
useLogDebug = false;

function getScriptText(vScriptName) {
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
    return emseScript.getScriptText() + "";
}

batchJobID = 0;
if (batchJobResult.getSuccess()) {
    batchJobID = batchJobResult.getOutput();
    aa.print("Batch Job " + batchJobName + " Job ID is " + batchJobID);
}
else {
    aa.print("Batch job ID not found " + batchJobResult.getErrorMessage());
}

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/

if (paramsOK) {
    aa.print("Start of Job");

    if (!timeExpired) {
        try {
            exportARData(sftpFolder);
        }
        catch (e) {
            aa.print("Error in process " + e.message);
        }
    }
    else {
        aa.print("End of Job: Elapsed Time : " + elapsed() + " Seconds");
    }
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

function exportARData(sftpFolder) {
    Avo_LogDebug("exportARData(" + sftpFolder + ")", 1);

    var exportData = "";

    var today = new Date();
    var year = today.getFullYear().toString();
    var month = String(today.getMonth() + 1).length < 2 ? "0" + String(today.getMonth() + 1) : String(today.getMonth() + 1);
    var day = today.getDate() < 9 ? "0" + today.getDate().toString() : today.getDate().toString();
    var hour = String(today.getHours());
    var mins = (today.getMinutes() < 10 ? "0" + today.getMinutes().toString() : today.getMinutes().toString());
    var secs = (today.getSeconds() < 10 ? "0" + today.getSeconds().toString() : today.getSeconds().toString());

    var filename = "expARAccela" + year + month + day + "_" + hour + mins + secs + ".csv";
    Avo_LogDebug("Filename(" + filename + ")", 2);    //debug

    var group = null;
    var type = null;

    //count = 0;  // testing

    //var allGroups = ["Building", "Enforcement", "Licenses", "AMS", "EnvHealth", "Fire", "Planning", "ServiceRequest"];

    //for (var i in allGroups) {
    //    // Get all records in group
    //    group = allGroups[i];
    //    var appsArray = aa.cap.getByAppType(group, type).getOutput();
    //    if (appsArray.length == 0) {
    //        continue;
    //    }

    //    Avo_LogDebug(group + ":", 2);

    //    for (var a in appsArray) {
    //        capId = appsArray[a].getCapID();
    //        exportData = writeARData(capId, exportData);
    //    }
    //}

    group = "Licenses";
    type = "Business";
    var allSubTypes = ["Bus Reg Merchant", "Bus Reg Service", "Alarm User"];
    for (var i in allSubTypes) {
        var subType = allSubTypes[i];
        var appsArray = aa.cap.getByAppType(group, type, subType, null).getOutput();
        if (appsArray.length == 0) {
            continue;
        }

        Avo_LogDebug(subType + ":", 2);

        for (var a in appsArray) {
            capId = appsArray[a].getCapID();
            exportData = writeARData(capId, exportData);

            //if (exportData.length > 0) {
            //    break;  //testing
            //}
        }
    }

    Avo_LogDebug("Data(" + exportData + ")", 2);    //debug
    //return; //testing

    //// Write export data to file
    //var path = "C://Temp/";
    //var file = null;

    //try {
    //    file = aa.util.writeToFile(exportData, path + filename);
    //} catch (ex) {
    //    Avo_LogDebug("Failed to create export file. " + ex.message);
    //    return;
    //}

    //if (!file) {
    //    Avo_LogDebug("Unable to create export file");
    //    return;
    //}

    //// Upload file to FTP
    //var result = aa.util.ftpUpload(ftpSite, port, username, password, ftpFolder, file);
    //if (result.getSuccess()) {
    //    Avo_LogDebug("Uploaded " + filename + " to " + ftpSite + ":" + port.toString() + " in directory " + ftpFolder);
    //} else {
    //    Avo_LogDebug("Failed to upload " + filename + " to " + ftpSite + ":" + port.toString() + ". " + result.errorType + ": " + result.errorMessage);
    //}

    // Upload Export data
    include("EMSE:SFTPADAPTERAPI");
    eval(getScriptText("EMSE:SFTPADAPTERAPI"));

    adapterToken = getToken(adapterUsername, adapterPassword);
    Avo_LogDebug("Token(" + adapterToken + ")", 2);	//debug

    if (!adapterToken) {
        Avo_LogDebug("Failed to get token from SFTP Adapter @ " + adapterUrl, 1);
        return;
    }

    var success = saveData(sftpFolder, filename, exportData);
    Avo_LogDebug("Upload Success(" + success + ")", 2); //debug

    //.replace(/\//g, '|').replace(/[?]/g, '').replace(/[%]/g, '').replace(/[&]/g, '').replace(/[:]/g, '').replace(/[;]/g, '')
    //    .replace(/[=]/g, '').replace(/[+]/g, '').replace(/[<]/g, '').replace(/[>]/g, '').replace(/["]/g, '').trim();  // Handle special chars
}

function writeARData(capId, exportData) {
    var altId = String(capId.getCustomID());
    //Avo_LogDebug("CapId(" + altId + ")", 2);	//debug

    var result = aa.finance.getInvoiceByCapID(capId, aa.util.newQueryFormat());
    if (result.getSuccess() == false) {
        Avo_LogDebug("Failed to get any invoices for record " + altId + '. ' + result.errorType + ': ' + result.errorMessage, 1);
        return exportData;
    }

    var allInvoices = result.getOutput();
    //Avo_LogDebug("Total Invoices(" + allInvoices.length + ")", 2);    //debug

    if (allInvoices.length == 0) {
        //Avo_LogDebug("No invoices on record " + altId, 1);
        return exportData;
    }

    //count++;    //testing

    var address = null;

    result = aa.address.getAddressByCapId(capId);
    if (result.getSuccess() != true) {
        Avo_LogDebug("Failed to get address for record " + altId + '. ' + result.errorType + ': ' + result.errorMessage, 1);
    } else {
        var addressModel = result.getOutput()[0];
        if (addressModel) {
            address = String(addressModel.displayAddress).trim();
        }
    }

    //Avo_LogDebug("Address(" + address + ")", 2);  //debug

    var name = null;
    var phone = null;

    // Get primary contact
    var allContacts = aa.people.getCapContactByCapID(capId).getOutput();
    for (var i in allContacts) {
        var contact = allContacts[i];
        var capContactModel = contact.capContactModel;
        var flag = capContactModel.primaryFlag;
        logDebug("Flag(" + flag + ")");	//debug

        if (flag != "Y") {
            continue;
        }

        // Get name
        // DBA name
        name = capContactModel.tradeName;

        // Legal business name
        if (!name) {
            name = capContactModel.businessName;
        }

        // First and last name
        if (!name) {
            name = capContactModel.firstName + " " + capContactModel.lastName;
        }

        //if (name.indexOf(",") != -1 || name.indexOf('"') != -1) {
        //    name = '"' + name + '"';
        //}
        name = name.split(',').join('');
        name = name.trim();

        Avo_LogDebug("Name(" + name + ")", 2);    //debug

        // if no record address get mailing address from contact
        if (!address || address.length == 0 || address.toLowerCase() == 'null') {
            var allAddresses = capContactModel.people.contactAddressList.toArray();
            for (var b in allAddresses) {
                var contactAddress = allAddresses[b];

                var addressType = contactAddress.addressType;
                if (addressType != "Mailing") {
                    continue;
                }

                address = String(contactAddress.addressLine1).trim() + ' ' + String(contactAddress.city).trim() + ' ' + String(contactAddress.zip).trim();
            }
        }

        // Get phone number
        if (capContactModel.phone3) {
            phone = capContactModel.phone3;
        }
        if (!phone) {
            phone = capContactModel.phone2;
        }
        if (!phone) {
            phone = capContactModel.phone1;
        }
        if (!phone) {
            phone = "5555555555";
        }

        break;
    }

    address = address.split(',').join('');
    address = address.trim();

    Avo_LogDebug("Address(" + address + ")", 2);  //debug

    // Apply mask to phone number
    phone = String(phone);
    phone.replace("(", "");
    phone.replace(")", "");
    phone.replace("-", "");

    var regex = /\s*?(\d{3})\s*(\d{4})\s*?/g;
    var mask = '$2-$3'
    if (phone.length > 7) {
        regex = /\s*?(\d{3})\s*?(\d{3})\s*(\d{4})\s*?/g;
        mask = '($1) $2-$3';
    }

    phone = phone.replace(regex, mask);
    Avo_LogDebug("Phone(" + phone + ")", 2);  //debug

    for (var i in allInvoices) {
        var invoiceModel = allInvoices[i].invoiceModel;

        var invoiceNum = invoiceModel.invNbr;
        Avo_LogDebug("Invoice(" + invoiceNum + ")", 2);	//debug

        var amount = invoiceModel.invAmount;
        var balance = invoiceModel.balanceDue;
        Avo_LogDebug("Balance($" + balance + ")", 2);	//debug

        exportData += '"' + altId + '",' + name + ',' + address + ',"' + phone + '",' + balance.toString();
        if (invoiceNum) {
            exportData += ',"' + invoiceNum.toString() + '"';
        }

        exportData += "\n";
    }

    return exportData;
}

function getAppSpecificCustom(itemName)  // optional: itemCap
{
    var updated = false;
    var i = 0;

    if (arguments.length == 2) {
        var itemCap = arguments[1]; // use cap ID specified in args
    } else {
        var itemCap = capId;
    }

    if (useAppSpecificGroupName) {
        if (itemName.indexOf(".") < 0) {
            aa.print("**WARNING: editAppSpecific requires group name prefix when useAppSpecificGroupName is true");
            return false;
            //return;
        }

        var itemGroup = itemName.substr(0, itemName.indexOf("."));
        var itemName = itemName.substr(itemName.indexOf(".") + 1);
    }
    var appSpecInfoResult = aa.appSpecificInfo.getByCapID(itemCap);
    if (appSpecInfoResult.getSuccess()) {
        var appspecObj = appSpecInfoResult.getOutput();

        if (itemName != "") {
            for (i in appspecObj)
                if (appspecObj[i].getCheckboxDesc() == itemName && (!useAppSpecificGroupName || appspecObj[i].getCheckboxType() == itemGroup)) {
                    //aa.print(appspecObj[i].getCheckboxDesc());
                    return appspecObj[i].getChecklistComment();
                    break;
                }
        } // item name blank
    }
    else { aa.print("**ERROR: getting app specific info for Cap : " + appSpecInfoResult.getErrorMessage()) }
}

/** ************************************************************************************** 
*  
*/
function startTransaction(timeOutInSec) {
    aa.batchJob.beginTransaction(timeOutInSec);
    logDebug(" *** A new transaction has been initiated");
}

/** ************************************************************************************** 
*  
*/
function commit() {
    aa.batchJob.commitTransaction();
    logDebug(" *** The transaction has been committed (script changes saved)");
    // aa.batchJob.rollbackTransaction();
    // logDebug(" *** The transaction has been rolled back (for testing)");
}

/** ************************************************************************************** 
*  
*/
function rollback() {
    aa.batchJob.rollbackTransaction();
    logDebug(" *** The transaction has been rolled back (script changes are not saved)");
}