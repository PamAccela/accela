/*******************************************************
| Script/Function: CopyAddressToContact() - (ID87)
| Created by: Nicolaj Bunting
| Created on: 22Aug18
| Usage: For all contacts on record copy primary or only contact address line1, line2, city, state, zip, and country to contact compact address
| Modified by: Nic Bunting (5Oct18)
*********************************************************/
(function CopyAddressToContact() {
    Avo_LogDebug("ID87 CopyAddressToContact()", 1);

    var result = aa.people.getCapContactByCapID(capId);
    if (result.getSuccess() != true) {
        Avo_LogDebug("No contacts on record", 1);
        return;
    }

    var allContacts = result.getOutput();
    for (i in allContacts) {
        var contact = allContacts[i];
        var contactModel = contact.capContactModel;

        var contactName = contactModel.contactName;
        Avo_LogDebug("Contact Name(" + contactName + ")", 2);	//debug

        var peopleModel = contactModel.people;

        var compactAddrModel = contactModel.compactAddress;

        var allContactAddresses = peopleModel.contactAddressList.toArray();
        for (j in allContactAddresses) {
            var contactAddressModel = allContactAddresses[j];

            // Default to primary or only address
            if (allContactAddresses.length > 1) {
                var isPrimary = contactAddressModel.primary == "Y";
                if (isPrimary != true) {
                    continue;
                }
            }

            // Copy address to Contact
            compactAddrModel.addressLine1 = contactAddressModel.addressLine1;
            compactAddrModel.addressLine2 = contactAddressModel.addressLine2;
            compactAddrModel.city = contactAddressModel.city;
            compactAddrModel.state = contactAddressModel.state;
            compactAddrModel.zip = contactAddressModel.zip;
            compactAddrModel.countryCode = contactAddressModel.countryCode;

            contactModel.compactAddress = compactAddrModel;

            var result = aa.people.editCapContact(contactModel);
            if (result.getSuccess() != true) {
                Avo_LogDebug("Failed to update address on contact " + contactName + ". " + result.errorMessage, 1);
            } else {
                Avo_LogDebug("Updated address on contact " + contactName, 1);
            }

            break;
        }
    }
})();