/*------------------------------------------------------------------------------------------------------/
| SVN $Id: DocumentReviewAddAfter.js 
| Program : DocumentReviewAddAfter.js
| Event   : DocumentReviewAddAfter
|
| Usage   : Master Script by Accela.  See accompanying documentation and release notes.
|
| Client  : N/A
| Action# : N/A
|
| Notes   : 
|
|
/------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------/
| START User Configurable Parameters
|
|     Only variables in the following section may be changed.  If any other section is modified, this
|     will no longer be considered a "Master" script and will not be supported in future releases.  If
|     changes are made, please add notes above.
/------------------------------------------------------------------------------------------------------*/

var controlString = "DocumentReviewAddAfter";	// Standard choice for control
var preExecute = "PreExecuteForAfterEvents";		// Standard choice to execute first (for globals, etc)
var documentOnly = false;							// Document Only -- displays hierarchy of std choice steps

/*------------------------------------------------------------------------------------------------------/
| BEGIN Event Specific Variables
/------------------------------------------------------------------------------------------------------*/
var capIDModel = null;
var documentID = null;
var userID = null;
var entityAssociationModels = aa.env.getValue("DocumentReviewModels");
var documentReviewModels = new Array();

//if (entityAssociationModels != null) {
//    logDebug("DocumentReviewModels Length is:" + entityAssociationModels.size());
//    var it = entityAssociationModels.iterator();
//    if (it.hasNext()) {
//        var model = it.next();

//        var id1 = model.getID1();
//        var id2 = model.getID2();
//        var id3 = model.getID3();
//        aa.env.setValue("PermitId1", id1);
//        aa.env.setValue("PermitId2", id2);
//        aa.env.setValue("PermitId3", id3);
//    }
//}

/*------------------------------------------------------------------------------------------------------/
| END Event Specific Variables
/------------------------------------------------------------------------------------------------------*/
var SCRIPT_VERSION = 9.0;
var useCustomScriptFile = false;  // if true, use Events->Custom Script and Master Scripts, else use Events->Scripts->INCLUDES_*
var useSA = false;
var SA = null;
var SAScript = null;
var bzr = aa.bizDomain.getBizDomainByValue("MULTI_SERVICE_SETTINGS", "SUPER_AGENCY_FOR_EMSE");
if (bzr.getSuccess() && bzr.getOutput().getAuditStatus() != "I") {
    useSA = true;
    SA = bzr.getOutput().getDescription();
    bzr = aa.bizDomain.getBizDomainByValue("MULTI_SERVICE_SETTINGS", "SUPER_AGENCY_INCLUDE_SCRIPT");
    if (bzr.getSuccess()) {
        SAScript = bzr.getOutput().getDescription();
    }
}

var controlFlagStdChoice = "EMSE_EXECUTE_OPTIONS";
var doStdChoices = true; // compatibility default
var doScripts = false;
var bzr = aa.bizDomain.getBizDomain(controlFlagStdChoice).getOutput().size() > 0;
if (bzr) {
    var bvr1 = aa.bizDomain.getBizDomainByValue(controlFlagStdChoice, "STD_CHOICE");
    doStdChoices = bvr1.getSuccess() && bvr1.getOutput().getAuditStatus() != "I";
    var bvr1 = aa.bizDomain.getBizDomainByValue(controlFlagStdChoice, "SCRIPT");
    doScripts = bvr1.getSuccess() && bvr1.getOutput().getAuditStatus() != "I";
    var bvr3 = aa.bizDomain.getBizDomainByValue(controlFlagStdChoice, "USE_MASTER_INCLUDES");
    if (bvr3.getSuccess()) { if (bvr3.getOutput().getDescription() == "No") useCustomScriptFile = false };
}

if (SA) {
    eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS", SA, useCustomScriptFile));
    eval(getScriptText("INCLUDES_ACCELA_GLOBALS", SA, useCustomScriptFile));
    eval(getScriptText(SAScript, SA));
} else {
    eval(getScriptText("INCLUDES_ACCELA_FUNCTIONS", null, useCustomScriptFile));
    eval(getScriptText("INCLUDES_ACCELA_GLOBALS", null, useCustomScriptFile));
}

eval(getScriptText("INCLUDES_CUSTOM", null, useCustomScriptFile));

//Add Standard Solution Includes 
solutionInc = aa.bizDomain.getBizDomain("STANDARD_SOLUTIONS").getOutput().toArray();
for (sol in solutionInc) {
    if (solutionInc[sol].getAuditStatus() != "I") eval(getScriptText(solutionInc[sol].getBizdomainValue(), null));
}

//Add Configurable RuleSets 
configRules = aa.bizDomain.getBizDomain("CONFIGURABLE_RULESETS").getOutput().toArray();
for (rule in configRules) {
    if (configRules[rule].getAuditStatus() != "I") eval(getScriptText(configRules[rule].getBizdomainValue(), null));
}

if (documentOnly) {
    doStandardChoiceActions(controlString, false, 0);
    aa.env.setValue("ScriptReturnCode", "0");
    aa.env.setValue("ScriptReturnMessage", "Documentation Successful.  No actions executed.");
    aa.abortScript();
}

var prefix = lookup("EMSE_VARIABLE_BRANCH_PREFIX", vEventName);

function getScriptText(vScriptName, servProvCode, useProductScripts) {
    if (!servProvCode) servProvCode = aa.getServiceProviderCode();
    vScriptName = vScriptName.toUpperCase();
    var emseBiz = aa.proxyInvoker.newInstance("com.accela.aa.emse.emse.EMSEBusiness").getOutput();
    try {
        if (useProductScripts) {
            var emseScript = emseBiz.getMasterScript(aa.getServiceProviderCode(), vScriptName);
        } else {
            var emseScript = emseBiz.getScriptByPK(aa.getServiceProviderCode(), vScriptName, "ADMIN");
        }
        return emseScript.getScriptText() + "";
    } catch (err) {
        return "";
    }
}

/*------------------------------------------------------------------------------------------------------/
| BEGIN Event Specific Variables
/------------------------------------------------------------------------------------------------------*/
if (entityAssociationModels != null) {
    logDebug("DocumentReviewModels Length is:" + entityAssociationModels.size());
    var it = entityAssociationModels.iterator();
    while (it.hasNext()) {
        var model = it.next();
        documentReviewModels.push(model);

        logDebug("ID			=:" + model.getResID());
        logDebug("Document ID		=:" + model.getDocumentID());
        logDebug("Entity Type		=:" + model.getEntityType());
        logDebug("Entity ID		=:" + model.getEntityID());
        logDebug("User ID		=:" + model.getEntityID1());
        logDebug("Process ID		=:" + model.getEntityID2());
        logDebug("Step Number		=:" + model.getEntityID3());
        logDebug("Assign Pages		=:" + model.getTaskReviewPages());
        logDebug("Assign Comments	=:" + model.getTaskReviewComments());

        logDebug("id1	=:" + model.getID1());
        logDebug("id2	=:" + model.getID2());
        logDebug("id3	=:" + model.getID3());

        if (capIDModel == null && model.getEntityType() == "TASK" && model.getID1() != null && model.getID2() != null && model.getID3() != null) {
            capIDModel = getCapIDModel(model.getID1(), model.getID2(), model.getID3());

            aa.env.setValue("PermitId1", model.getID1());
            aa.env.setValue("PermitId2", model.getID2());
            aa.env.setValue("PermitId3", model.getID3());
        }
        if (documentID == null && model.getDocumentID() != null) {
            documentID = model.getDocumentID();
        }

        userID = model.getEntityID1();
        //if (externalUserDept == model.getEntityID() && model.getEntityID1() != '' && model.getEntityID1() != null) {
        //    sendEmailByExtUser(model.getEntityID1())
        //}
        //else if (userID && userID != '') {
        //    sendEmailByUserID(userID);
        //}
        //else if (model.getEntityID() && model.getEntityID() != '') {
        //    sendEmailByDeptName(model.getEntityID());
        //}
    }
}
else {
    logDebug("ERROR: Cannot find the parameter 'DocumentReviewModels'.");
}

/*------------------------------------------------------------------------------------------------------/
| END Event Specific Variables
/------------------------------------------------------------------------------------------------------*/

if (preExecute.length) doStandardChoiceActions(preExecute, true, 0);    // run Pre-execution code

logGlobals(AInfo);

/*------------------------------------------------------------------------------------------------------/
| <===========Main=Loop================>
|
/-----------------------------------------------------------------------------------------------------*/
//
//
//  Get the Standard choices entry we'll use for this App type
//  Then, get the action/criteria pairs for this app
//

if (doStdChoices) doStandardChoiceActions(controlString, true, 0);

//
//  Next, execute and scripts that are associated to the record type
//

if (doScripts) doScriptActions();

//
// Check for invoicing of fees
//
if (feeSeqList.length) {
    invoiceResult = aa.finance.createInvoice(capId, feeSeqList, paymentPeriodList);
    if (invoiceResult.getSuccess())
        logMessage("Invoicing assessed fee items is successful.");
    else
        logMessage("**ERROR: Invoicing the fee items assessed to app # " + capIDString + " was not successful.  Reason: " + invoiceResult.getErrorMessage());
}

/*------------------------------------------------------------------------------------------------------/
| <===========END=Main=Loop================>
/-----------------------------------------------------------------------------------------------------*/

if (debug.indexOf("**ERROR") > 0) {
    aa.env.setValue("ScriptReturnCode", "1");
    aa.env.setValue("ScriptReturnMessage", debug);
}
else {
    aa.env.setValue("ScriptReturnCode", "0");
    if (showMessage) aa.env.setValue("ScriptReturnMessage", message);
    if (showDebug) aa.env.setValue("ScriptReturnMessage", debug);
}

function getCapIDModel(id1, id2, id3) {
    var capId = null;
    var capIdModelResult = aa.cap.getCapID(id1, id2, id3);
    if (capIdModelResult.getSuccess()) {
        capId = capIdModelResult.getOutput();
    }

    return capId;
}